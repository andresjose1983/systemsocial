package eu.marcocattaneo.androidinstagramconnector.connection.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import systemsocial.trofiventures.com.instagram.R;

public class AuthenticationDialog extends Dialog {

    private WebView wv;
    private String brandName;

    public static AuthenticationDialog newInstance(Context activity, Runnable runnable,
                                                   String url, String urlCallback, String brandName) {
        AuthenticationDialog f = new AuthenticationDialog(activity, runnable, url, urlCallback, brandName);
        return f;
    }

    public AuthenticationDialog(Context activity, Runnable runnable, String url, String urlCallback,
                                String brandName) {
        super(activity);
        this.mActivity = activity;
        this.mUrl = url;
        this.mUrlCallback = urlCallback;
        this.runnable = runnable;
        this.brandName = brandName;
    }

    private Runnable runnable;

    private String mUrl;

    private String mUrlCallback;

    private Context mActivity;

    private OnHttpCallback mOnHttpCallback;
    private boolean isDismiss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        setContentView(R.layout.dialog_view_instagram);

        /*final View close = findViewById(R.id.iv_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDismiss = false;
                dismiss();
            }
        });*/

        //Web
        wv = (WebView) findViewById(R.id.wv_instagram);
        wv.loadUrl(mUrl);
        wv.setWebChromeClient(new WebChromeClient());

        wv.setWebViewClient(new WebViewClient() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                if (errorResponse.getStatusCode() == 404) {
                    dismiss();
                    Toast.makeText(view.getContext(), R.string.user_password_incorrect, Toast.LENGTH_SHORT).show();
                }
                super.onReceivedHttpError(view, request, errorResponse);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.pg_instragram).setVisibility(View.GONE);
                TextView title = (TextView) findViewById(R.id.text_view_title);
                title.setVisibility(View.VISIBLE);
                title.setText(String.format("Account for %1$s", brandName));
                wv.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (mOnHttpCallback != null && url.contains("code=")) {
                    isDismiss = true;
                    dismiss();
                    mOnHttpCallback.onIntercept(view, url);
                } else if (url.contains("logout")) {
                    isDismiss = true;
                    dismiss();
                    mOnHttpCallback.onIntercept(view, url);
                }

                return false;
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!isDismiss)
                    mOnHttpCallback.onClose(runnable);
                clearCache();
            }
        });

        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.gravity = Gravity.CENTER;

        getWindow().setAttributes(params);
    }

    /**
     * Pass callback interface
     *
     * @param onHttpCallback
     */
    public void addOnHttpCallback(OnHttpCallback onHttpCallback) {
        mOnHttpCallback = onHttpCallback;
    }

    public interface OnHttpCallback {

        void onIntercept(WebView webView, String url);

        void onClose(final Runnable runnable);

    }

    public void clearCache() {
        wv.clearCache(true);
        wv.clearHistory();
        wv.clearFormData();

        //CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(wv.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        cookieManager.removeSessionCookie();
        wv.destroy();

    }

}
