package com.trofiventures.systemsocial.common.alerts;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.dagger.module.AppModule;
import com.trofiventures.systemsocial.interactor.FacebookInteractor;

/**
 * Created by andre on 4/20/2017.
 */

public class FacebookAlbumAlertView extends Dialog {

    private Runnable yesAction, noAction;
    public Context c;
    public Button yes;
    public Button no;

    public FacebookAlbumAlertView(Context a, Runnable yesAction, Runnable noAction) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.facebook_album_modal);
        setCancelable(false);

        yes = (Button) findViewById(R.id.yes_button);
        no = (Button) findViewById(R.id.no_button);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = c.getSharedPreferences(AppModule.class.getCanonicalName(),
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPref.edit();
                edit.putString(FacebookInteractor.ALBUM_NAME,
                        ((EditText) findViewById(R.id.album_name)).getText().toString()).apply();
                edit.commit();
                dismiss();
                yesAction.run();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(noAction != null)
                    noAction.run();
                dismiss();
            }
        });
    }
}
