package com.trofiventures.systemsocial.common.alerts;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by luis_ on 1/21/2017.
 */

public class TwitterAlertView extends Dialog {

    private Callback<TwitterSession> twitterCallback;
    private Runnable yesAction;
    private Runnable noAction;
    public Context c;
    public String title, message, yesString, noString;
    public Button yes;
    public Button no;
    public TextView titleTxt, messasgeTxt;

    public TwitterAlertView(Activity a, Runnable yesAction, Runnable noAction) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    public TwitterAlertView(Context context, Runnable yesAction, Runnable noAction, Callback<TwitterSession> callback) {
        super(context);
        c = context;
        this.twitterCallback = callback;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.twitter_modal);
        setCancelable(false);

        yes = (Button) findViewById(R.id.btn_yes_twitter);
        no = (Button) findViewById(R.id.btn_no);

        yes.setCompoundDrawablesWithIntrinsicBounds(
                getContext().getResources().getDrawable(com.twitter.sdk.android.core.R.drawable.tw__ic_logo_default), null, null, null);
        yes.setCompoundDrawablePadding(
                getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_drawable_padding));
        yes.setText(com.twitter.sdk.android.core.R.string.tw__login_btn_txt);
        yes.setTextColor(getContext().getResources().getColor(com.twitter.sdk.android.core.R.color.tw__solid_white));
        yes.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_text_size));
        yes.setTypeface(Typeface.DEFAULT_BOLD);
        yes.setPadding(getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_left_padding), 0,
                getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_right_padding), 0);
        yes.setBackgroundResource(com.twitter.sdk.android.core.R.drawable.tw__login_btn);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                yesAction.run();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (noAction != null)
                    noAction.run();
            }
        });
    }
}