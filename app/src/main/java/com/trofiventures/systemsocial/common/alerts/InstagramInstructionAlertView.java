package com.trofiventures.systemsocial.common.alerts;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.dagger.module.AppModule;

/**
 * Created by andre on 4/20/2017.
 */

public class InstagramInstructionAlertView extends Dialog {

    private Runnable yesAction;
    public Context c;
    public Button yes;
    public Button no;
    private TextView alertMessageTextView;
    private String instagramUserName;

    public InstagramInstructionAlertView(Context a, Runnable yesAction, String instagramUserName) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
        this.instagramUserName = instagramUserName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.instagram_instructions_modal);
        setCancelable(false);

        alertMessageTextView = (TextView) findViewById(R.id.alert_message);
        alertMessageTextView.setText(String.format(alertMessageTextView.getText().toString(), instagramUserName));

        yes = (Button) findViewById(R.id.yes_button);
        no = (Button) findViewById(R.id.no_button);

        final SwitchCompat aSwitch = (SwitchCompat) findViewById(R.id.instruction_switch);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesAction.run();
                dismiss();
                SharedPreferences sharedPref = c.getSharedPreferences(AppModule.class.getCanonicalName(),
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPref.edit();
                edit.putBoolean(c.getString(R.string.instruction_selected), aSwitch.isChecked()).apply();
                edit.commit();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
