package com.trofiventures.systemsocial.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.alerts.AccountsAlertView;
import com.trofiventures.systemsocial.common.alerts.AlertView;
import com.trofiventures.systemsocial.common.alerts.FacebookAlbumAlertView;
import com.trofiventures.systemsocial.common.alerts.FacebookLikeAlertView;
import com.trofiventures.systemsocial.common.alerts.InstagramAlertView;
import com.trofiventures.systemsocial.common.alerts.InstagramCommentAlertView;
import com.trofiventures.systemsocial.common.alerts.InstagramInstructionAlertView;
import com.trofiventures.systemsocial.common.alerts.NewVersionAlertView;
import com.trofiventures.systemsocial.common.alerts.PostTextInstagramAlertView;
import com.trofiventures.systemsocial.common.alerts.TwitterAlertView;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.presenter.AccountsPresenter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.UUID;

/**
 * Created by luis_ on 1/21/2017.
 */

public class UIUtils {

    private static ProgressDialog dialog;
    private static InstagramAlertView instagramAlert;

    public static void showFadeInText(TextView textView, String message) {
        showFadeInText(textView, message, 0, 0);
    }

    public static void showFadeInText(TextView textView, String message, int duration, int color) {
        duration = duration == 0 ? 1200 : duration;
        AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);

        if (color != 0)
            textView.setTextColor(color);
        else
            textView.setTextColor(BaseApplication.getContext().getResources().getColor(R.color.lightRed));

        textView.setVisibility(android.view.View.VISIBLE);
        textView.setText(message);
        textView.startAnimation(fadeIn);
        textView.startAnimation(fadeOut);

        fadeIn.setDuration(duration);
        fadeIn.setFillAfter(true);
        fadeOut.setDuration(duration);
        fadeOut.setFillAfter(true);
        fadeOut.setStartOffset((duration * 3) + fadeIn.getStartOffset());

        textView.setVisibility(android.view.View.INVISIBLE);
    }

    public static void showProgressDialog(Context context, String message) {
        dialog = new ProgressDialog(context);
        dialog.setMessage(message == null ? BaseApplication.getContext().getString(R.string.loading_data) : message);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void hideProgressDialog() {
        try {
            if (dialog != null)
                dialog.cancel();
        } catch (Exception e) {
            Log.d(UIUtils.class.getCanonicalName(), e.getMessage());
        }
    }

    public static void storeDeviceID() {
        SharedPreferences preferences = BaseApplication.getContext().getSharedPreferences("SystemSocial", android.content.Context.MODE_PRIVATE);
        if (!preferences.contains(BaseApplication.getContext().getString(R.string.token))) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(BaseApplication.getContext().getString(R.string.token), UUID.randomUUID().toString()).apply();
            editor.commit();
        }
    }

    public static void storePlayerID(String playerId) {
        SharedPreferences preferences = BaseApplication.getContext().getSharedPreferences("SystemSocial", android.content.Context.MODE_PRIVATE);
        if (!preferences.contains(BaseApplication.getContext().getString(R.string.token))) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(BaseApplication.getContext().getString(R.string.player_id), playerId).apply();
            editor.commit();
        }
    }

    public static String getPlayerID() {
        SharedPreferences preferences = BaseApplication.getContext().getSharedPreferences("SystemSocial", android.content.Context.MODE_PRIVATE);
        return preferences.getString(BaseApplication.getContext().getString(R.string.player_id), "");
    }

    public static String getDeviceID() {
        SharedPreferences preferences = BaseApplication.getContext().getSharedPreferences("SystemSocial", android.content.Context.MODE_PRIVATE);
        return preferences.getString(BaseApplication.getContext().getString(R.string.token), "");
    }

    public static void showFacebookAlert(Activity context, Runnable yesAction) {
        AccountsAlertView alertView = new AccountsAlertView(context, yesAction, null);
        alertView.show();
    }

    public static void showInstagram(Activity context, Runnable yesAction, AccountsPresenter presenter) {
        instagramAlert = new InstagramAlertView(context, yesAction, null, presenter);
        instagramAlert.show();
    }

    public static void hideInstagram() {
        instagramAlert.dismiss();
    }

    public static void showTwitter(Context context, Runnable yesAction, Callback<TwitterSession> callback) {
        TwitterAlertView alertView = new TwitterAlertView(context, yesAction, null, callback);
        alertView.show();
    }

    public static void showAlertView(Context context, String title, String message, String yesString, String noString, Runnable yesAction, Runnable noAction) {
        AlertView alertView = new AlertView(context, title, message, yesString, noString, yesAction, noAction);
        alertView.show();
    }

    public static void showInstagramInstructionAlertView(Context context, Runnable yesAction, String instagramUserName) {
        Dialog alertView = new InstagramInstructionAlertView(context, yesAction, instagramUserName);
        alertView.show();
    }

    public static void showFacebookLikeAlertView(Context context, Runnable yesAction) {
        Dialog alertView = new FacebookLikeAlertView(context, yesAction);
        alertView.show();
    }

    public static void showPostTextInstagramAlertView(Context context, Runnable yesAction) {
        Dialog alertView = new PostTextInstagramAlertView(context, yesAction);
        alertView.show();
    }

    public static void showFacebookAlbum(Context context, Runnable yesAction, Runnable noAction) {
        Dialog dialog = new FacebookAlbumAlertView(context, yesAction, noAction);
        dialog.show();
    }

    public static void instagramComment(Context context, SocialPost post, Runnable yesAction,
                                        Runnable noAction) {
        Dialog dialog = new InstagramCommentAlertView(context, post, yesAction, noAction);
        dialog.show();
    }

    public static void newVersion(Context context, boolean showNoteContent) {
        Dialog dialog = new NewVersionAlertView(context, showNoteContent);
        dialog.show();
    }
}
