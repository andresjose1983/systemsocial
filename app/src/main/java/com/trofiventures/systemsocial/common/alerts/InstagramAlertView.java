package com.trofiventures.systemsocial.common.alerts;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.presenter.AccountsPresenter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by luis_ on 1/21/2017.
 */

public class InstagramAlertView extends Dialog {

    private AccountsPresenter presenter;
    private Callback<TwitterSession> twitterCallback;
    private Runnable yesAction;
    private Runnable noAction;
    public Context c;
    public String title, message, yesString, noString;
    public Button yes;
    public Button no;
    public TextView titleTxt, messasgeTxt;
    private EditText username, password;

    public InstagramAlertView(Activity a, Runnable yesAction, Runnable noAction, AccountsPresenter presenter) {
        super(a);
        this.c = a;
        this.presenter = presenter;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.instagram_moda);
        setCancelable(false);

        yes = (Button) findViewById(R.id.btn_yes_twitter);
        no = (Button) findViewById(R.id.btn_no);
        username = (EditText) findViewById(R.id.instagram_username);
        password = (EditText) findViewById(R.id.instagram_password);

        yes.setText(R.string.login_to_instagram);
        yes.setTextColor(getContext().getResources().getColor(com.twitter.sdk.android.core.R.color.tw__solid_white));
        yes.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_text_size));
        yes.setTypeface(Typeface.DEFAULT_BOLD);
        yes.setPadding(getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_left_padding), 0,
                getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_right_padding), 0);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesAction.run();
              //  presenter.sendInstagramActivation(username.getText().toString(), password.getText().toString());
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (noAction != null)
                    noAction.run();
            }
        });
    }
}