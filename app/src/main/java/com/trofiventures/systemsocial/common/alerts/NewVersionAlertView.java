package com.trofiventures.systemsocial.common.alerts;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;

/**
 * Created by andre on 4/20/2017.
 */

public class NewVersionAlertView extends Dialog {

    public Context c;
    private boolean showNoteContent;

    public NewVersionAlertView(Context a, boolean showNoteContent) {
        super(a);
        this.c = a;
        this.showNoteContent = showNoteContent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.new_version_available_modal);
        setCancelable(false);

        Button yes = (Button) findViewById(R.id.yes_button);
        Button no = (Button) findViewById(R.id.no_button);

        TextView noteContent = (TextView) findViewById(R.id.note_message);

        if (showNoteContent)
            noteContent.setVisibility(View.VISIBLE);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = c.getPackageName();
                try {
                    c.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    c.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id="
                                    + appPackageName)));
                }finally {
                    dismiss();
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
