package com.trofiventures.systemsocial.common.alerts;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.dagger.module.AppModule;

/**
 * Created by andre on 4/20/2017.
 */

public class FacebookLikeAlertView extends Dialog {

    private Runnable yesAction;
    public Context c;
    public Button yes;
    public Button no;

    public FacebookLikeAlertView(Context a, Runnable yesAction) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.facebook_like_action_modal);
        setCancelable(false);

        yes = (Button) findViewById(R.id.yes_button);
        no = (Button) findViewById(R.id.no_button);

        final SwitchCompat aSwitch = (SwitchCompat) findViewById(R.id.facebook_like_switch);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesAction.run();
                dismiss();
                SharedPreferences sharedPref = c.getSharedPreferences(AppModule.class.getCanonicalName(),
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPref.edit();
                edit.putBoolean(c.getString(R.string.facebook_like_selected), aSwitch.isChecked()).apply();
                edit.commit();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
