package com.trofiventures.systemsocial.common.alerts;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.dagger.module.AppModule;

/**
 * Created by andre on 4/20/2017.
 */

public class PostTextInstagramAlertView extends Dialog {

    private Runnable continueAction;
    public Context c;
    public Button continueButton;

    public PostTextInstagramAlertView(Context a, Runnable continueAction) {
        super(a);
        this.c = a;
        this.continueAction = continueAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.post_text_instagram_modal);
        setCancelable(false);

        continueButton = (Button) findViewById(R.id.continue_button);

        final SwitchCompat aSwitch = (SwitchCompat) findViewById(R.id.post_switch);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (continueAction != null)
                    continueAction.run();
                dismiss();
                SharedPreferences sharedPref = c.getSharedPreferences(AppModule.class.getCanonicalName(),
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = sharedPref.edit();
                edit.putBoolean(c.getString(R.string.post_selected), aSwitch.isChecked()).apply();
                edit.commit();
            }
        });

    }
}
