package com.trofiventures.systemsocial.common;

/**
 * Created by andre on 4/6/2017.
 */

public enum PostType {

    ADVOCATE(0),
    BRAND(1),
    MESSAGE(2),
    RELINK(3);

    private int id;

    PostType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
