package com.trofiventures.systemsocial.common;


import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.network.responses.User;

/**
 * Created by luis_ on 1/15/2017.
 */
public class BaseActivity extends AppCompatActivity {

    /**
     * Make a Custom Toolbar
     *
     * @param title
     * @param home
     */
    public void setCustomToolbar(String title, boolean home) {
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.custom_toolbar);
            if (toolbar != null) {
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayShowTitleEnabled(false);

                if (!home) {
                    getSupportActionBar().setHomeButtonEnabled(true);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    toolbar.setNavigationOnClickListener(new android.view.View.OnClickListener() {
                        @Override
                        public void onClick(android.view.View v) {
                            finish();
                        }
                    });
                }

                TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

                if (mTitle != null) {
                    toolbar.setTitle(null);
                    if (mTitle.getText().toString() != null) {
                        mTitle.setTextSize(18);
                        mTitle.setText(title);
                    } else {
                        mTitle.setVisibility(View.GONE);
                    }
                } else {
                    toolbar.setTitle(title);
                }

                User user = User.getSharedUser();

                if (user != null && !user.isMultibrandAdvocate()) {
                    ImageView logoImage = (ImageView) toolbar.findViewById(R.id.logo_image_view);

                    if (user.getBrandColor() != null) {
                        String color = "#" + user.getBrandColor();
                        toolbar.setBackgroundColor(Color.parseColor(color));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(Color.parseColor(color));
                        }
                    }
                    if (user.getBrandLogo() != null)
                        Picasso.with(this)
                                .load(Uri.parse(user.getBrandLogo()))
                                .resize(48, 48)
                                .centerInside()
                                .into(logoImage);

                }
            }
        } catch (Exception e) {
            Log.e("Custom toolbar", "Error log message.", e);
        }
    }

}
