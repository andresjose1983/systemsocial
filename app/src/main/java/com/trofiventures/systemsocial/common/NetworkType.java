package com.trofiventures.systemsocial.common;

/**
 * Created by ajmp on 4/4/2017.
 */

public enum NetworkType {

    FACEBOOK(0),
    INSTAGRAM(1),
    TWITTER(2);

    private int id;

    NetworkType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
