package com.trofiventures.systemsocial.common.alerts;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.facebook.share.widget.LikeView;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;

public class AlertFacebookLikeView extends Dialog {

    public String title, message;
    public Button no;
    private SocialPost socialPost;

    public AlertFacebookLikeView(Activity a, SocialPost socialPost) {
        super(a);
        this.socialPost = socialPost;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.facebook_like_modal);
        setCancelable(false);

        LikeView likeView = (LikeView) findViewById(R.id.facebook_like_view);
        likeView.setLikeViewStyle(LikeView.Style.STANDARD);
        likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);

        likeView.setObjectIdAndType(BuildConfig.FACEBOOK_URL + socialPost.getNetworkId(),
                LikeView.ObjectType.DEFAULT);

        findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}