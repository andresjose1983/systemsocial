package com.trofiventures.systemsocial.common;

/**
 * Created by andre on 4/6/2017.
 */

public enum Action {
    COMMENT(0),
    EDIT(1),
    LIKE(2),
    POST(3),
    RETWEET(4),
    SHARE(5),
    TWEET(6),
    NONE(-1);

    private int id;

    Action(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Action get(int value) {
        switch (value) {
            case 0:
                return COMMENT;
            case 1:
                return EDIT;
            case 2:
                return LIKE;
            case 3:
                return POST;
            case 4:
                return RETWEET;
            case 5:
                return SHARE;
            case 6:
                return TWEET;
            default:
                return NONE;
        }
    }
}
