package com.trofiventures.systemsocial.common.alerts;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

/**
 * Created by luis_ on 1/21/2017.
 */

public class AccountsAlertView extends Dialog {

    private int layoutId;
    private Runnable yesAction;
    private Runnable noAction;
    public Context c;
    public String title, message, yesString, noString;
    public RelativeLayout yes;
    public Button no;
    public TextView titleTxt, messasgeTxt;

    public AccountsAlertView(Activity a, Runnable yesAction, Runnable noAction) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.facebook_modal);
        setCancelable(false);

        yes = (RelativeLayout) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                yesAction.run();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (noAction != null)
                    noAction.run();
            }
        });
    }
}