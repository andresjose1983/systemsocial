package com.trofiventures.systemsocial.common;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.dagger.module.AppModule;
import com.trofiventures.systemsocial.dagger.module.DaggerNetComponent;
import com.trofiventures.systemsocial.dagger.module.FacebookModule;
import com.trofiventures.systemsocial.dagger.module.InstagramModule;
import com.trofiventures.systemsocial.dagger.module.NetComponent;
import com.trofiventures.systemsocial.dagger.module.NetModule;
import com.trofiventures.systemsocial.dagger.module.TwitterModule;
import com.trofiventures.systemsocial.helper.Util;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by luis_ on 1/16/2017.
 */

public class BaseApplication extends MultiDexApplication {

    private static NetComponent mNetComponent;
    private static android.content.Context context;
    private LruCache picassoCache;

    @Override
    public void onCreate() {
        super.onCreate();

        TwitterAuthConfig authConfig = new TwitterAuthConfig(BuildConfig.TWITTER_KEY,
                BuildConfig.TWITTER_SECRET);
        Fabric.Builder builderFrabic = new Fabric.Builder(this).kits(new Crashlytics(), new Twitter(authConfig));
        Fabric.with(builderFrabic.build());
        /*Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
*/
        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this);

        context = this.getApplicationContext();
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.URL))
                .twitterModule(new TwitterModule())
                .facebookModule(new FacebookModule())
                .instagramModule(new InstagramModule())
                .build();

        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/SystemSocial/");
        if (!file.exists()) {
            file.mkdirs();
        } else {
            Log.d("error", "dir. already exists");
        }

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        RealmController.with(this);

        try {
            Picasso.Builder builder = new Picasso.Builder(this);
            picassoCache = new LruCache(this);
            builder.memoryCache(picassoCache);
            Picasso built = builder.build();
            //if (BuildConfig.DEBUG)
            //    built.setIndicatorsEnabled(true);
            built.setLoggingEnabled(true);
            Picasso.setSingletonInstance(built);
        } catch (IllegalStateException e) {

        }
        if (BuildConfig.DEBUG) {
            try {
                PackageInfo info = getPackageManager().getPackageInfo(
                        "com.systemsocial.systemsocial",
                        PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {

            } catch (NoSuchAlgorithmException e) {

            }
        }

        //OneSignal
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                        OneSignal.clearOneSignalNotifications();
                        Util.showNotification(BaseApplication.this, notification.payload.title,
                                notification.payload.body);
                    }
                })
                .init();

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                Log.d("debug", "User:" + userId);
                UIUtils.storePlayerID(userId);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);

            }
        });

        new Instabug.Builder(this, "7b25638b9e01e2294c341e0a2f4ed4e6")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();
    }

    public static android.content.Context getContext() {
        return context;
    }

    public static NetComponent getNetComponent() {
        return mNetComponent;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        picassoCache.clear();
    }

    // This fires when a notification is opened by tapping on it or one is received while the app is running.
    /*private class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
            try {
                if (additionalData != null) {
                    if (additionalData.has("actionSelected"))
                        Log.d("OneSignalExample", "OneSignal notification button with id " + additionalData.getString("actionSelected") + " pressed");
                    Log.d("OneSignalExample", "Full additionalData:\n" + additionalData.toString());
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }*/
}
