package com.trofiventures.systemsocial.common.alerts;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.dagger.module.AppModule;
import com.trofiventures.systemsocial.helper.CircleTransform;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.model.entities.post.Action;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;

/**
 * Created by andre on 4/20/2017.
 */

public class InstagramCommentAlertView extends Dialog {

    private Runnable yesAction, noAction;
    private Context c;
    private SocialPost post;

    public InstagramCommentAlertView(Context a, SocialPost post, Runnable yesAction,
                                     Runnable noAction) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
        this.post = post;
        this.noAction = noAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.instagram_comment_modal);
        setCancelable(true);

        View sendButton = findViewById(R.id.post_comment_fab);
        ImageView photoImageView = (ImageView) findViewById(R.id.photo_image_view);

        if (post != null) {
            String image;
            if (post.getYoutubeId() != null && post.getYoutubeId().length() > 0)
                image = post.getThumbnail();
            else
                image = post.getImgUrl();

            Picasso.with(c).load(image).into(photoImageView);
        }

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = ((EditText) findViewById(R.id.comment_text_view)).getText().toString();

                if (comment.length() > 0) {
                    SharedPreferences sharedPref = c.getSharedPreferences(AppModule.class.getCanonicalName(),
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPref.edit();
                    edit.putString(InstagramInteractor.COMMENT, comment).apply();
                    edit.commit();
                    dismiss();
                    yesAction.run();
                }
            }
        });

        findViewById(R.id.close_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        this.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (noAction != null)
                    noAction.run();
            }
        });

        if (getWindow() != null)
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
