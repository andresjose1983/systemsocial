package com.trofiventures.systemsocial.common.alerts;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;

/**
 * Created by luis_ on 1/21/2017.
 */

public class AlertView extends Dialog {

    private Runnable yesAction;
    private Runnable noAction;
    public Context c;
    public String title, message, yesString, noString;
    public Button yes, no;
    public TextView titleTxt, messasgeTxt;

    public AlertView(Activity a, Runnable yesAction, Runnable noAction) {
        super(a);
        this.c = a;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    public AlertView(Context context, String title, String message, String yesString, String noString,
                     Runnable yesAction, Runnable noAction) {
        super(context);
        c = context;
        this.title = title;
        this.message = message;
        this.yesString = yesString;
        this.noString = noString;
        this.yesAction = yesAction;
        this.noAction = noAction;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_alert);
        setCancelable(false);

        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);

        if (title != null) {
            titleTxt = (TextView) findViewById(R.id.alert_title);
            titleTxt.setText(title);
        }

        messasgeTxt = (TextView) findViewById(R.id.alert_message);
        messasgeTxt.setText(message);

        if (yesString == null) {
            yes.setVisibility(View.GONE);
        } else {
            yes.setText(yesString);
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    yesAction.run();
                }
            });
        }

        if (noString == null) {
            no.setVisibility(View.GONE);
        } else {
            no.setText(noString);
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    if (noAction != null)
                        noAction.run();
                }
            });
        }
    }
}