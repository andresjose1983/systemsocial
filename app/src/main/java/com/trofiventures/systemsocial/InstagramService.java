package com.trofiventures.systemsocial;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 5/5/2017.
 */

public class InstagramService extends Service {

    private Subscription sUpdate;
    private final int ONGOING_NOTIFICATION_ID = 5001;

    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    InstagramInteractor instagramInteractor;
    private boolean isRequesting = false;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public InstagramService() {
        BaseApplication.getNetComponent().inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            final int count = sharedPreferences.getInt(InstagramInteractor.COUNT, 0);
            final SocialPost post = new SocialPost();
            post.setId(intent.getExtras().getString("ID"));
            post.setBrandId(intent.getExtras().getString("BRAND_ID"));
            sUpdate = Observable.interval(0, 3, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .flatMap(new Func1<Long, Observable<Integer>>() {
                        @Override
                        public Observable<Integer> call(Long integer) {
                            instagramInteractor.refresUserData(InstagramService.this, post, new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("Test call", " " + count + " " + sharedPreferences
                                            .getInt(InstagramInteractor.COUNT, 0));
                                    if (sharedPreferences.getInt(InstagramInteractor.COUNT, 0) > count
                                            && !isRequesting) {
                                        isRequesting = true;
                                        instagramInteractor.getMedia(post, new StopInstagramService() {
                                            @Override
                                            public void onStop() {
                                                if (sUpdate != null && !sUpdate.isUnsubscribed()) {
                                                    sUpdate.unsubscribe();
                                                    stop();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            return Observable.just(sharedPreferences.getInt(
                                    InstagramInteractor.COUNT, 0));
                        }
                    })
                    .take(30)
                    .doOnError(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {

                        }
                    })
                    .doOnSubscribe(new Action0() {
                        @Override
                        public void call() {
                            showNotification();
                        }
                    })
                    .doAfterTerminate(new Action0() {
                        @Override
                        public void call() {
                            stop();
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Integer>() {
                        @Override
                        public void call(Integer integer) {
                            Log.d("Test subscribe", " " + integer);
                        }
                    });
        } else
            stop();
        return START_STICKY;
    }

    private void stop() {
        Log.d("Test subscribe", " FINISH");
        Intent intent1 = new Intent();
        intent1.setAction(HomeNavActivity.HIDE_LOADER_FILTER);
        LocalBroadcastManager.getInstance(InstagramService.this).sendBroadcast(intent1);
        stopForeground(true);
        stopSelf();
    }

    private void showNotification() {
        Intent intent = new Intent();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setTicker(getString(R.string.app_name))
                .setContentText(getString(R.string.waiting_for_instagram))
                .setSmallIcon(R.drawable.ic_cloud_download_24dp)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    public static boolean isMyServiceRunning(final Context context) {

        if (context != null) {
            ActivityManager manager = (ActivityManager) context.getSystemService(
                    Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager
                    .getRunningServices(Integer.MAX_VALUE)) {
                if (InstagramService.class.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public interface StopInstagramService {
        void onStop();
    }
}
