package com.trofiventures.systemsocial.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.network.responses.Brand;

import java.util.List;

/**
 * Created by andre on 5/1/2017.
 */

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.ViewHolder> {

    private List<Brand> brands;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_brand, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Brand brand = brands.get(position);
        holder.brandName.setText(brand.getName());
    }

    @Override
    public int getItemCount() {
        return brands == null ? 0 : brands.size();
    }

    final class ViewHolder extends RecyclerView.ViewHolder {

        TextView brandName;

        public ViewHolder(View itemView) {
            super(itemView);
            brandName = (TextView) itemView.findViewById(R.id.name_brands_text);
        }
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }
}
