package com.trofiventures.systemsocial.adapter;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.systemsocial.systemsocial.InstagramBrandGroupActivity;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.network.responses.Brand;

import java.util.List;

/**
 * Created by andre on 1/29/2018.
 */

public class InstagramBrandGroupAdapter extends RecyclerView.Adapter<InstagramBrandGroupAdapter.ViewHolder> {

    private List<Brand> instagramBrandGroups;
    private InstagramBrandGroupActivity.IIBrandAction action;

    public void setAction(InstagramBrandGroupActivity.IIBrandAction action) {
        this.action = action;
    }

    public void setBrands(List<Brand> instagramBrandGroups) {
        this.instagramBrandGroups = instagramBrandGroups;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand_group, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Brand instagramBrandGroup = instagramBrandGroups.get(position);
        holder.brandName.setText(instagramBrandGroup.getName());
        if (instagramBrandGroup.getInstagramUsername() == null) {
            holder.instagramInfo.setText(R.string.not_instagram_user);
            holder.instagramInfo.setTextColor(ContextCompat.getColor(holder.instagramInfo.getContext(), R.color.red));
            holder.instagramInfo.setTypeface(null, Typeface.NORMAL);
        } else {
            holder.instagramInfo.setText(String.format(holder.instagramInfo.getContext().getString(R.string.instagram_info), instagramBrandGroup.getInstagramUsername()));
            holder.instagramInfo.setTextColor(ContextCompat.getColor(holder.instagramInfo.getContext(), R.color.colorPrimary));
            holder.instagramInfo.setTypeface(null, Typeface.BOLD);
        }
    }

    @Override
    public int getItemCount() {
        return instagramBrandGroups != null ? instagramBrandGroups.size() : 0;
    }

    final class ViewHolder extends RecyclerView.ViewHolder {

        TextView brandName, instagramInfo;

        public ViewHolder(View itemView) {
            super(itemView);
            brandName = (TextView) itemView.findViewById(R.id.name_brand_text);
            instagramInfo = (TextView) itemView.findViewById(R.id.instagram_info_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    action.onClick(instagramBrandGroups.get(getAdapterPosition()));
                }
            });
        }
    }
}
