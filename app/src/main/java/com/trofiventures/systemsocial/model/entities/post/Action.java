
package com.trofiventures.systemsocial.model.entities.post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Action extends RealmObject {

    @SerializedName("points")
    @Expose
    private long points;
    @SerializedName("action")
    @Expose
    private long action;

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public long getAction() {
        return action;
    }

    public void setAction(long action) {
        this.action = action;
    }

}
