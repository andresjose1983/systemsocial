package com.trofiventures.systemsocial.model.network.requests;

import java.io.Serializable;

/**
 * Created by andre on 4/12/2017.
 */

public class TwitterUpload implements Serializable {

    private String message;

    private String post_id;

    public TwitterUpload(String message, String post_id) {
        this.message = message;
        this.post_id = post_id;
    }
}
