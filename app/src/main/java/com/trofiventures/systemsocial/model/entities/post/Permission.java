
package com.trofiventures.systemsocial.model.entities.post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Permission extends RealmObject{

    @SerializedName("employee_type_id")
    @Expose
    private int employeeTypeId;
    @SerializedName("permissions")
    @Expose
    private long permissions;

    public int getEmployeeTypeId() {
        return employeeTypeId;
    }

    public void setEmployeeTypeId(int employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }

    public long getPermissions() {
        return permissions;
    }

    public void setPermissions(long permissions) {
        this.permissions = permissions;
    }

}
