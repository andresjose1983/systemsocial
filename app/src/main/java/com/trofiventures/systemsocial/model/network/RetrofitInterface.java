package com.trofiventures.systemsocial.model.network;


import com.trofiventures.systemsocial.model.network.requests.ActivationRequest;
import com.trofiventures.systemsocial.model.network.requests.ChangePassword;
import com.trofiventures.systemsocial.model.network.requests.ForgotRequest;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;
import com.trofiventures.systemsocial.model.network.requests.RegisterRequest;
import com.trofiventures.systemsocial.model.network.requests.ResetUserAccount;
import com.trofiventures.systemsocial.model.network.requests.TwitterUpload;
import com.trofiventures.systemsocial.model.network.responses.BrandResponse;
import com.trofiventures.systemsocial.model.network.responses.FacebookAlbum;
import com.trofiventures.systemsocial.model.network.responses.LoginResponse;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.model.network.responses.post.PostResponse;
import com.trofiventures.systemsocial.model.network.responses.register.RegisterResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by luis_ on 1/15/2017.
 */
public interface RetrofitInterface {

    @POST("login")
    Observable<LoginResponse> login(@Header("push_token") String playerId,
                                    @Header("os") int os,
                                    @Header("version") String version,
                                    @Header("invalidates") boolean invalidates,
                                    @Body User request);

    @GET("login")
    Observable<StatusRequest> checkSession(@Header("SS-Auth-Token") String token,
                                           @Header("os") int os,
                                           @Header("version") String version,
                                           @Header("invalidates") boolean invalidates,
                                           @Header("push_token") String playerId);

    @POST("reset")
    Observable<Void> reset(@Header("SS-Auth-Token") String token);

    @POST("users/link")
    Observable<ResponseBody> link(@Header("push_token") String playerId, @Body ActivationRequest request);

    @POST("employees")
    Observable<RegisterResponse> register(@Body RegisterRequest request);

    @GET("terms")
    Call<ResponseBody> terms(@Header("Accept") String accept, @Query("user_type") String userType);

    @GET("post")
    Observable<PostResponse> getPostData();

    @PUT("restore")
    Observable<ResponseBody> restorePassword(@Body ForgotRequest request);

    @POST("restore")
    Observable<ResponseBody> sendForgotRequest(@Body ForgotRequest request);

    @PUT("employees/{user_id}")
    Observable<ResponseBody> updatePassword(@Body ChangePassword request,
                                            @Path("user_id") String userId);

    @GET
    @Streaming
    Call<ResponseBody> getVideo(@Url String url);

    @DELETE("login")
    Observable<ResponseBody> logout(@Header("SS-Auth-Token") String token);

    @POST("post/{post_id}/decline")
    Observable<ResponseBody> deletePost(@Header("SS-Auth-Token") String token,
                                        @Path("post_id") String postId);

    @POST("post/{post_id}/done")
    Observable<ResponseBody> donePost(@Header("SS-Auth-Token") String token,
                                      @Path("post_id") String postId);

    @POST("post/action")
    Observable<StatusRequest> actionPost(@Header("SS-Auth-Token") String token, @Body PostBrand postBrand);

    @POST("post/twitter")
    Observable<StatusRequest> postOnTwitter(@Header("SS-Auth-Token") String token,
                                            @Body TwitterUpload twitterUpload);

    @GET("brands")
    Observable<BrandResponse> getBrands(@Header("SS-Auth-Token") String token);

    @POST("users/facebookAlbumId/{facebook_album_id}")
    Observable<Void> setFacebookAlbumId(@Header("SS-Auth-Token") String token,
                                        @Path("facebook_album_id") String albumId);

    @GET("users/facebookAlbumId")
    Observable<FacebookAlbum> getFacebookAlbumId(@Header("SS-Auth-Token") String token);

    @DELETE("users/link/{network}/{brand}")
    Observable<ResponseBody> resetSystemSocialAccount(@Header("SS-Auth-Token") String token,
                                                      @Path("network") String network,
                                                      @Path("brand") String brand);

    @POST("users/link/remove")
    Observable<ResponseBody> resetSystemSocialAccount(@Header("SS-Auth-Token") String token,
                                                      @Body ResetUserAccount resetUserAccount);
}
