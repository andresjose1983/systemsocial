package com.trofiventures.systemsocial.model.network.requests;

import java.io.Serializable;

/**
 * Created by andre on 8/28/2017.
 */

public class ResetUserAccount implements Serializable {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
