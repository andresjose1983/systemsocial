package com.trofiventures.systemsocial.model.network.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andre on 5/1/2017.
 */

public class ChangePassword {

    @SerializedName("old_password")
    private String oldPassword;

    private String password;

    private String confirmPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }


    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
