package com.trofiventures.systemsocial.model.network.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by andre on 5/1/2017.
 */

public class Brand implements Serializable {

    private String id;
    private String name;
    private boolean active;
    @SerializedName("instagramUsername")
    private String instagramUsername;

    public Brand(String id) {
        this.id = id;
    }

    public String getInstagramUsername() {
        return instagramUsername;
    }

    public void setInstagramUsername(String instagramUsername) {
        this.instagramUsername = instagramUsername;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Brand)) return false;

        Brand brand = (Brand) o;
        return id.equals(brand.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
