package com.trofiventures.systemsocial.model.network.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by luis_ on 1/27/2017.
 */

public final class ActivationRequest {
    private int network;
    private String token;
    private String user_network_id;
    @SerializedName("network_data")
    private NetworkData networkData;
    private int os;
    private String username;
    private String brand;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public NetworkData getNetworkData() {
        return networkData;
    }

    public void setNetworkData(NetworkData networkData) {
        this.networkData = networkData;
    }

    public int getNetwork() {
        return network;
    }

    public void setNetwork(int network) {
        this.network = network;
    }

    public String getUser_network_id() {
        return user_network_id;
    }

    public void setUser_network_id(String user_network_id) {
        this.user_network_id = user_network_id;
    }

    public void setOs(int os) {
        this.os = os;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
