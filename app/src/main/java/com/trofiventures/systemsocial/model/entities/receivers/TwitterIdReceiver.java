package com.trofiventures.systemsocial.model.entities.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Debug;

import com.twitter.sdk.android.tweetcomposer.TweetUploadService;

/**
 * Created by luis_ on 2/26/2017.
 */

public class TwitterIdReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Debug.waitForDebugger();
        System.out.println("received twitter callback");
        if (TweetUploadService.UPLOAD_SUCCESS.equals(intent.getAction())) {
            // success
            final Long tweetId = intent.getExtras().getLong(TweetUploadService.EXTRA_TWEET_ID);
            System.out.println(tweetId);
        } else {
            // failure

            System.out.println("twitter post failed");
            final Intent retryIntent = intent.getExtras().getParcelable(TweetUploadService.EXTRA_RETRY_INTENT);
        }
    }
}
