
package com.trofiventures.systemsocial.model.network.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("token")
    @Expose
    private String token;
    private String name;
    @SerializedName("has_links")
    @Expose
    private Boolean hasLinks;
    @SerializedName("should_accept_terms")
    @Expose
    private Boolean shouldAcceptTerms;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user")
    @Expose
    private User user;
    private String id;
    @SerializedName("brand_color")
    @Expose
    private String brandColor;
    @SerializedName("brand_logo")
    @Expose
    private String brandLogo;
    private List<Brand> brands;
    @SerializedName("is_multi_brand_advocate")
    @Expose
    private boolean isMultibrandAdvocate;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasLinks() {
        return hasLinks;
    }

    public void setHasLinks(Boolean hasLinks) {
        this.hasLinks = hasLinks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getShouldAcceptTerms() {
        return shouldAcceptTerms;
    }

    public void setShouldAcceptTerms(Boolean shouldAcceptTerms) {
        this.shouldAcceptTerms = shouldAcceptTerms;
    }

    public String getBrandColor() {
        return brandColor;
    }

    public void setBrandColor(String brandColor) {
        this.brandColor = brandColor;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public boolean isMultibrandAdvocate() {
        return isMultibrandAdvocate;
    }

    public void setMultibrandAdvocate(boolean multibrandAdvocate) {
        isMultibrandAdvocate = multibrandAdvocate;
    }
}
