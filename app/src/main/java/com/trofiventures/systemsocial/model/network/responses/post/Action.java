
package com.trofiventures.systemsocial.model.network.responses.post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Action {

    @SerializedName("points")
    @Expose
    private Long points;
    @SerializedName("action")
    @Expose
    private Long action;

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public Long getAction() {
        return action;
    }

    public void setAction(Long action) {
        this.action = action;
    }

}
