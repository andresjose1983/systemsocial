
package com.trofiventures.systemsocial.model.network.responses.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Brand {

    @SerializedName("brand")
    @Expose
    private Brand_ brand;
    @SerializedName("type")
    @Expose
    private Type type;

    public Brand_ getBrand() {
        return brand;
    }

    public void setBrand(Brand_ brand) {
        this.brand = brand;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}
