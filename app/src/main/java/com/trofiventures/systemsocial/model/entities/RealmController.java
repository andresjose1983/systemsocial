package com.trofiventures.systemsocial.model.entities;

/**
 * Created by luis_ on 1/16/2017.
 */

import android.app.Application;

import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.helper.Util;
import com.trofiventures.systemsocial.model.entities.post.PostId;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.entities.post.TypeData;
import com.trofiventures.systemsocial.model.network.responses.post.Action;
import com.trofiventures.systemsocial.model.network.responses.post.Post;

import java.util.Date;
import java.util.TimeZone;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Application application) {
        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {
        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    //Refresh the realm istance
    public void refresh() {
        realm.refresh();
    }

    //clear all objects from SocialPost.class
    public void clearAll() {
        realm.beginTransaction();
        realm.clear(SocialPost.class);
        realm.clear(PostId.class);
        realm.clear(TypeData.class);
        realm.clear(com.trofiventures.systemsocial.model.entities.post.Permission.class);
        realm.clear(com.trofiventures.systemsocial.model.entities.post.Action.class);
        realm.commitTransaction();
    }

    private void save(final Post post) {
        try {
            final SocialPost socialPost = new SocialPost();
            socialPost.setBrandId(post.getBrandId());
            //socialPost.setDateEnd(post.getDateEnd());
            socialPost.setId(post.getId());
            socialPost.setType(post.getType());
            socialPost.setMediaType(post.getMediaType());
            socialPost.setNetworkData(post.getNetworkData());
            socialPost.setPostDate(post.getPostDate());
            if (post.getType() == 0 &&
                    post.getNetworks().contains(new Long(NetworkType.FACEBOOK.getId()))) {
                socialPost.setPostText(null);
                socialPost.setFacebookText(post.getPostText());
            } else
                socialPost.setPostText(post.getPostText());
            socialPost.setPostUrl(post.getPostUrl());
            socialPost.setPostTitle(post.getPostTitle());
            socialPost.setThumbnail(post.getThumbnail());
            socialPost.setImgUrl(post.getImgUrl());
            socialPost.setTitle(post.getTitle());
            socialPost.setNetworkId(post.getNetworkId());
            socialPost.setYoutubeId(post.getVideoId());
            socialPost.setVideoUrl(post.getVideoUrl());
            socialPost.setEditable(post.isEditable());
            socialPost.setIntagramSelected(false);
            socialPost.setFacebookSelected(false);
            socialPost.setTwitterSelected(false);
            socialPost.setDateEnd(Util.toDate(post.getDateEnd()));
            socialPost.setLink(post.getLink());

            TypeData data = new TypeData();

            if (post.getNetworks() != null) {
                RealmList<PostId> ids = new RealmList<>();
                for (Long id : post.getNetworks()) {
                    PostId postId = new PostId();
                    postId.setId(id.intValue());
                    ids.add(postId);
                }

                data.setNetworks(ids);
            }

            if (post.getActions() != null) {
                RealmList<com.trofiventures.systemsocial.model.entities.post.Action> actions = new RealmList<>();
                for (Action action : post.getActions()) {
                    com.trofiventures.systemsocial.model.entities.post.Action typeAction = new com.trofiventures.systemsocial.model.entities.post.Action();
                    typeAction.setAction(action.getAction());
                    typeAction.setPoints(action.getPoints());
                    actions.add(typeAction);
                }

                data.setActionsRealm(actions);
            }

            socialPost.setTypeData(data);
            //Realm.getDefaultInstance().commitTransaction();
            Realm.getDefaultInstance().beginTransaction();
            Realm.getDefaultInstance().copyToRealmOrUpdate(socialPost);
            Realm.getDefaultInstance().commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createPost(final Post post) {
        /*if (getSocialPost(post.getId()) == null &&
                post.getType() == 0 && post.getNetworks().size() > 1) {
            if (post.getNetworks().contains(new Long(NetworkType.FACEBOOK.getId()))) {
                List<Long> longs = new ArrayList<>();
                for (Long aLong : post.getNetworks())
                    if (NetworkType.FACEBOOK.getId() != aLong)
                        longs.add(aLong);
                post.setNetworks(longs);
                createPost(post);
                //Create facebook post
                post.setId(post.getId() + "-facebook");
                post.setNetworks(Arrays.asList(new Long(NetworkType.FACEBOOK.getId())));
                createPost(post);
            } else
                save(post);
        } else */
        if (getSocialPost(post.getId()) == null) {
            save(post);
        }
    }

    //find all objects in the SocialPost.class
    public RealmResults<SocialPost> getSocialPosts() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        RealmResults<SocialPost> allSorted = realm.where(SocialPost.class)
                .equalTo("dispatched", false)
                .greaterThan("dateEnd", new Date())
                .findAllSorted("id");
        realm.commitTransaction();
        return allSorted;
    }

    //query a single item with the given id
    public SocialPost getSocialPost(String id) {
        SocialPost socialPost = realm.where(SocialPost.class).equalTo("id", id).findFirst();
        realm.commitTransaction();
        return socialPost;
    }

    //check if SocialPost.class is
    public boolean hasSocialPosts() {
        return !realm.allObjects(SocialPost.class).isEmpty();
    }

    //query example
    public RealmResults<SocialPost> queriedSocialPosts() {
        return realm.where(SocialPost.class).findAll();
    }

    public void setSocialPostDispatched(String id) {
        SocialPost socialPost = getSocialPost(id);

        //Realm.getDefaultInstance().commitTransaction();
        Realm.getDefaultInstance().beginTransaction();
        socialPost.setDispatched(true);
        Realm.getDefaultInstance().copyToRealmOrUpdate(socialPost);
        Realm.getDefaultInstance().commitTransaction();
    }

    public void setSocialPostTwitterSelected(String id, boolean b) {

        SocialPost socialPost = getSocialPost(id);
        // Realm.getDefaultInstance().commitTransaction();
        Realm.getDefaultInstance().beginTransaction();
        socialPost.setTwitterSelected(b);
        Realm.getDefaultInstance().copyToRealmOrUpdate(socialPost);
        Realm.getDefaultInstance().commitTransaction();
    }

    public void setSocialPostFacebookSelected(String id, final boolean b) {

        SocialPost socialPost = getSocialPost(id);
        //Realm.getDefaultInstance().commitTransaction();
        Realm.getDefaultInstance().beginTransaction();
        socialPost.setFacebookSelected(b);
        Realm.getDefaultInstance().copyToRealmOrUpdate(socialPost);
        Realm.getDefaultInstance().commitTransaction();
    }

    public void setSocialPostInstagramSelected(String id, boolean b) {

        SocialPost socialPost = getSocialPost(id);
        //Realm.getDefaultInstance().commitTransaction();
        Realm.getDefaultInstance().beginTransaction();
        socialPost.setIntagramSelected(b);
        Realm.getDefaultInstance().copyToRealmOrUpdate(socialPost);
        Realm.getDefaultInstance().commitTransaction();
    }

    public void deletePost(String postId) {
        SocialPost socialPost = getSocialPost(postId);
        if (socialPost != null) {
            //Realm.getDefaultInstance().commitTransaction();
            Realm.getDefaultInstance().beginTransaction();
            socialPost.removeFromRealm();
            Realm.getDefaultInstance().commitTransaction();
        }
    }

    public void updatePost(String postId, String text) {
        //Realm.getDefaultInstance().commitTransaction();
        SocialPost socialPost = getSocialPost(postId);
        Realm.getDefaultInstance().beginTransaction();
        socialPost.setPostText(text);
        Realm.getDefaultInstance().copyToRealmOrUpdate(socialPost);
        Realm.getDefaultInstance().commitTransaction();
    }
}
