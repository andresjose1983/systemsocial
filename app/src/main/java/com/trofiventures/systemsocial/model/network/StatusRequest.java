package com.trofiventures.systemsocial.model.network;

import com.google.gson.annotations.SerializedName;
import com.trofiventures.systemsocial.model.network.responses.Brand;

import java.io.Serializable;
import java.util.List;

/**
 * Created by andre on 4/5/2017.
 */

public class StatusRequest implements Serializable {

    private String code;
    private String statusCode;
    private String msg;
    private boolean valid;
    private boolean invalidates;
    private String brandColor;
    private String brandLogo;
    private List<Brand> brands;
    @SerializedName("is_multi_brand_advocate")
    private boolean isMultibrandAdvocate;

    public StatusRequest(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isValid() {
        return valid;
    }

    public boolean isInvalidates() {
        return invalidates;
    }

    public String getBrandColor() {
        return brandColor;
    }

    public void setBrandColor(String brandColor) {
        this.brandColor = brandColor;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public boolean isMultibrandAdvocate() {
        return isMultibrandAdvocate;
    }

    public void setMultibrandAdvocate(boolean multibrandAdvocate) {
        isMultibrandAdvocate = multibrandAdvocate;
    }
}
