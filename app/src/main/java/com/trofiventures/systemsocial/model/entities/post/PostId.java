package com.trofiventures.systemsocial.model.entities.post;

import io.realm.RealmObject;

/**
 * Created by luis_ on 2/5/2017.
 */

public class PostId extends RealmObject {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
