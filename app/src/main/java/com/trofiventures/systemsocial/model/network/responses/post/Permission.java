
package com.trofiventures.systemsocial.model.network.responses.post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Permission {

    @SerializedName("employee_type_id")
    @Expose
    private Object employeeTypeId;
    @SerializedName("permissions")
    @Expose
    private Long permissions;

    public Object getEmployeeTypeId() {
        return employeeTypeId;
    }

    public void setEmployeeTypeId(Object employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }

    public Long getPermissions() {
        return permissions;
    }

    public void setPermissions(Long permissions) {
        this.permissions = permissions;
    }

}
