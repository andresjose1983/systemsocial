package com.trofiventures.systemsocial.model.network.responses;

import java.util.List;

/**
 * Created by andre on 5/1/2017.
 */

public class BrandResponse {

    private int code;
    private String msg;
    private String statusCode;
    private List<Brand> brands;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }
}
