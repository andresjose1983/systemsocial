
package com.trofiventures.systemsocial.model.network.responses.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Brand_ {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("suspended")
    @Expose
    private Boolean suspended;
    @SerializedName("allowed_users")
    @Expose
    private Long allowedUsers;
    @SerializedName("current_users")
    @Expose
    private Long currentUsers;
    @SerializedName("billing_level")
    @Expose
    private Long billingLevel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Long getAllowedUsers() {
        return allowedUsers;
    }

    public void setAllowedUsers(Long allowedUsers) {
        this.allowedUsers = allowedUsers;
    }

    public Long getCurrentUsers() {
        return currentUsers;
    }

    public void setCurrentUsers(Long currentUsers) {
        this.currentUsers = currentUsers;
    }

    public Long getBillingLevel() {
        return billingLevel;
    }

    public void setBillingLevel(Long billingLevel) {
        this.billingLevel = billingLevel;
    }

}
