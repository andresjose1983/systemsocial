
package com.trofiventures.systemsocial.model.entities.post;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class TypeData extends RealmObject {

    @SerializedName("actions")
    @Expose
    @Ignore
    private transient List<Action> actions = null;

    @Expose
    private transient RealmList<Action> actionsRealm = null;

    @SerializedName("editable")
    @Expose
    private boolean editable;

    private transient RealmList<PostId> networks;

    @SerializedName("points")
    @Expose
    private long points;

    public boolean getEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }


    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public RealmList<Action> getActionsRealm() {
        return actionsRealm;
    }

    public void setActionsRealm(RealmList<Action> actionsRealm) {
        this.actionsRealm = actionsRealm;
    }

    public RealmList<PostId> getNetworks() {
        return networks;
    }

    public void setNetworks(RealmList<PostId> networks) {
        this.networks = networks;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}
