
package com.trofiventures.systemsocial.model.entities.post;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class SocialPost extends RealmObject {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private String id;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("permissions")
    @Expose
    private RealmList<Permission> permissions = null;
    @SerializedName("network_data")
    @Expose
    private String networkData;
    @SerializedName("post_url")
    @Expose
    private String postUrl;
    @SerializedName("post_text")
    @Expose
    private String postText;
    @SerializedName("type")
    @Expose
    private long type;
    @SerializedName("video_id")
    @Expose
    private String youtubeId;
    @SerializedName("date_start")
    @Expose
    private String dateStart;
    @SerializedName("date_end")
    @Expose
    private Date dateEnd;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("type_data")
    @Expose
    private TypeData typeData;
    private boolean editable;
    private String imgUrl;

    private String facebookText;

    private String videoUrl;

    private String networkId;

    private RealmList<PostId> mediaIds;

    private String link;


    private boolean twitterSelected = false, facebookSelected = false, intagramSelected = false, hasSelectedNetworks;

    @Ignore
    private String requestMediaIds;

    private boolean dispatched;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public RealmList<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(RealmList<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getNetworkData() {
        return networkData;
    }

    public void setNetworkData(String networkData) {
        this.networkData = networkData;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public TypeData getTypeData() {
        return typeData;
    }

    public void setTypeData(TypeData typeData) {
        this.typeData = typeData;
    }

    public boolean isDispatched() {
        return dispatched;
    }

    public void setDispatched(boolean dispatched) {
        this.dispatched = dispatched;
    }

    public RealmList<PostId> getMediaIds() {
        return mediaIds;
    }

    public void setMediaIds(RealmList<PostId> mediaIds) {
        this.mediaIds = mediaIds;
    }

    public String getRequestMediaIds() {
        StringBuilder mediasIds = new StringBuilder();

        for (int i = 0; i < getMediaIds().size(); i++) {
            PostId id = getMediaIds().get(i);

            mediasIds.append(id.getId());
            if (i < getMediaIds().size())
                mediasIds.append(",");
        }
        return mediasIds.toString();
    }


    public void setRequestMediaIds(String requestMediaIds) {

        this.requestMediaIds = requestMediaIds;
    }

    public boolean isTwitterSelected() {
        return twitterSelected;
    }

    public void setTwitterSelected(boolean twitterSelected) {
        this.twitterSelected = twitterSelected;
    }

    public boolean isFacebookSelected() {
        return facebookSelected;
    }

    public void setFacebookSelected(boolean facebookSelected) {
        this.facebookSelected = !this.facebookSelected;
    }

    public boolean isIntagramSelected() {
        return intagramSelected;
    }

    public void setIntagramSelected(boolean intagramSelected) {
        this.intagramSelected = !this.intagramSelected;
    }

    public boolean isHasSelectedNetworks() {
        hasSelectedNetworks = false;

        if (twitterSelected || intagramSelected || facebookSelected)
            hasSelectedNetworks = true;

        return hasSelectedNetworks;
    }

    public void setHasSelectedNetworks(boolean hasSelectedNetworks) {
        this.hasSelectedNetworks = hasSelectedNetworks;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getFacebookText() {
        return facebookText;
    }

    public void setFacebookText(String facebookText) {
        this.facebookText = facebookText;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
