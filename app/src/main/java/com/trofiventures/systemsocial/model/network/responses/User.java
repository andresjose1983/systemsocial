
package com.trofiventures.systemsocial.model.network.responses;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.facebook.AccessToken;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.twitter.sdk.android.core.TwitterAuthToken;

import java.io.Serializable;
import java.util.List;

public class User extends BaseObservable implements Serializable {

    static User sharedUser;
    private String token;
    private AccessToken facebookToken;
    private TwitterAuthToken twitterToken;
    private String instagramToken;
    @SerializedName("push_token")
    private String pushToken;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("terms_accepted")
    @Expose
    private Long termsAccepted;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("has_links")
    @Expose
    private Boolean hasLinks;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("suspended")
    @Expose
    private Boolean suspended;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("should_accept_terms")
    @Expose
    private Boolean shouldAcceptTerms;
    @SerializedName("has_posted")
    @Expose
    private Boolean hasPosted;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    private String brandColor;
    private String brandLogo;
    private List<Brand> brands;
    @SerializedName("is_multibrand_advocate")
    @Expose
    private boolean isMultibrandAdvocate;

    public static User getSharedUser() {
        return sharedUser;
    }

    public static void setUser(User user) {
        sharedUser = user;
    }

    public Long getTermsAccepted() {
        return termsAccepted;
    }

    public void setTermsAccepted(Long termsAccepted) {
        this.termsAccepted = termsAccepted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getHasLinks() {
        return hasLinks;
    }

    public void setHasLinks(Boolean hasLinks) {
        this.hasLinks = hasLinks;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Boolean getShouldAcceptTerms() {
        return shouldAcceptTerms;
    }

    public void setShouldAcceptTerms(Boolean shouldAcceptTerms) {
        this.shouldAcceptTerms = shouldAcceptTerms;
    }

    public Boolean getHasPosted() {
        return hasPosted;
    }

    public void setHasPosted(Boolean hasPosted) {
        this.hasPosted = hasPosted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AccessToken getFacebookToken() {
        return facebookToken;
    }

    public void setFacebookToken(AccessToken facebookToken) {
        setHasLinks(true);
        this.facebookToken = facebookToken;
    }

    public TwitterAuthToken getTwitterToken() {
        return twitterToken;
    }

    public void setTwitterToken(TwitterAuthToken twitterToken) {
        setHasLinks(true);
        this.twitterToken = twitterToken;
    }

    public String getInstagramToken() {
        return instagramToken;
    }

    public void setInstagramToken(String instagramToken) {
        this.instagramToken = instagramToken;
    }

    public String getBrandColor() {
        return brandColor;
    }

    public void setBrandColor(String brandColor) {
        this.brandColor = brandColor;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public boolean isMultibrandAdvocate() {
        return isMultibrandAdvocate;
    }

    public void setMultibrandAdvocate(boolean multibrandAdvocate) {
        isMultibrandAdvocate = multibrandAdvocate;
    }
}
