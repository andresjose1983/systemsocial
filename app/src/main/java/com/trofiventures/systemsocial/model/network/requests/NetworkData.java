package com.trofiventures.systemsocial.model.network.requests;

import android.support.annotation.Nullable;

import com.facebook.AccessTokenSource;

import java.util.Collection;
import java.util.Date;

/**
 * Created by luis_ on 1/28/2017.
 */

public class NetworkData {

    private String accessToken;
    private String applicationId;
    private String userId, authTokenSecret, authToken, userName, password, username;
    private String media;
    private String user;
    private String image;
    private String follows;
    private String followed_by;
    private String id;
    private String bio;
    @Nullable
    private Collection<String> permissions;
    @Nullable
    private Collection<String> declinedPermissions;
    @Nullable
    private AccessTokenSource accessTokenSource;
    @Nullable
    private Date expirationTime;
    @Nullable
    private Date lastRefreshTime;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Nullable
    public Collection<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(@Nullable Collection<String> permissions) {
        this.permissions = permissions;
    }

    @Nullable
    public Collection<String> getDeclinedPermissions() {
        return declinedPermissions;
    }

    public void setDeclinedPermissions(@Nullable Collection<String> declinedPermissions) {
        this.declinedPermissions = declinedPermissions;
    }

    @Nullable
    public AccessTokenSource getAccessTokenSource() {
        return accessTokenSource;
    }

    public void setAccessTokenSource(@Nullable AccessTokenSource accessTokenSource) {
        this.accessTokenSource = accessTokenSource;
    }

    @Nullable
    public Date getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(@Nullable Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    @Nullable
    public Date getLastRefreshTime() {
        return lastRefreshTime;
    }

    public void setLastRefreshTime(@Nullable Date lastRefreshTime) {
        this.lastRefreshTime = lastRefreshTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthTokenSecret() {
        return authTokenSecret;
    }

    public void setAuthTokenSecret(String authTokenSecret) {
        this.authTokenSecret = authTokenSecret;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFollows() {
        return follows;
    }

    public void setFollows(String follows) {
        this.follows = follows;
    }

    public String getFollowed_by() {
        return followed_by;
    }

    public void setFollowed_by(String followed_by) {
        this.followed_by = followed_by;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
