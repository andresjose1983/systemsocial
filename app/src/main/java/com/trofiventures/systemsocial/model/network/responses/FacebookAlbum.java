package com.trofiventures.systemsocial.model.network.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by andre on 5/26/2017.
 */

public class FacebookAlbum implements Serializable {

    private String statusCode;
    private int code;
    private String msg;
    @SerializedName("facebook_album_id")
    private String albumId;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }
}
