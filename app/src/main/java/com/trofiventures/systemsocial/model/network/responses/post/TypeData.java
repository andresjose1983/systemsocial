
package com.trofiventures.systemsocial.model.network.responses.post;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TypeData {

    @SerializedName("actions")
    @Expose
    private List<Action> actions = null;
    @SerializedName("editable")
    @Expose
    private Boolean editable;
    @SerializedName("networks")
    @Expose
    private List<Long> networks = null;
    @SerializedName("points")
    @Expose
    private Long points;

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public List<Long> getNetworks() {
        return networks;
    }

    public void setNetworks(List<Long> networks) {
        this.networks = networks;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

}
