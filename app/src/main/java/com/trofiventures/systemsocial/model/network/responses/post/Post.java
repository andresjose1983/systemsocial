
package com.trofiventures.systemsocial.model.network.responses.post;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("post_id")
    @Expose
    private String id;
    @SerializedName("campaigns")
    @Expose
    private List<String> campaigns = null;
    @SerializedName("stagger")
    @Expose
    private String stagger;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("permissions")
    @Expose
    private List<Permission> permissions = null;
    @SerializedName("network_data")
    @Expose
    private String networkData;
    @SerializedName("url")
    @Expose
    private String postUrl;
    @SerializedName("post_text")
    @Expose
    private String postText;
    @SerializedName("type")
    @Expose
    private Long type;
    @SerializedName("from_date")
    @Expose
    private String dateStart;
    @SerializedName("to_date")
    @Expose
    private String dateEnd;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("locked")
    @Expose
    private Boolean locked;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("type_data")
    @Expose
    private TypeData typeData;

    @SerializedName("networks")
    @Expose
    private List<Long> networks = null;

    @SerializedName("actions")
    @Expose
    private List<Action> actions = null;

    @SerializedName("network_id")
    @Expose
    private String networkId;

    @SerializedName("video_id")
    @Expose
    private String videoId;

    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("editable")
    @Expose
    private boolean editable;
    @SerializedName("link")
    @Expose
    private String link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<String> campaigns) {
        this.campaigns = campaigns;
    }

    public String getStagger() {
        return stagger;
    }

    public void setStagger(String stagger) {
        this.stagger = stagger;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getNetworkData() {
        return networkData;
    }

    public void setNetworkData(String networkData) {
        this.networkData = networkData;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public TypeData getTypeData() {
        return typeData;
    }

    public void setTypeData(TypeData typeData) {
        this.typeData = typeData;
    }

    public List<Long> getNetworks() {
        return networks;
    }

    public void setNetworks(List<Long> networks) {
        this.networks = networks;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
