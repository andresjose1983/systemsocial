
package com.trofiventures.systemsocial.model.network.responses.register;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("suspended")
    @Expose
    private Boolean suspended;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("terms_accepted")
    @Expose
    private Long termsAccepted;
    @SerializedName("has_links")
    @Expose
    private Boolean hasLinks;
    @SerializedName("should_accept_terms")
    @Expose
    private Boolean shouldAcceptTerms;
    @SerializedName("has_posted")
    @Expose
    private Boolean hasPosted;
    @SerializedName("brands")
    @Expose
    private List<Brand> brands = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Long getTermsAccepted() {
        return termsAccepted;
    }

    public void setTermsAccepted(Long termsAccepted) {
        this.termsAccepted = termsAccepted;
    }

    public Boolean getHasLinks() {
        return hasLinks;
    }

    public void setHasLinks(Boolean hasLinks) {
        this.hasLinks = hasLinks;
    }

    public Boolean getShouldAcceptTerms() {
        return shouldAcceptTerms;
    }

    public void setShouldAcceptTerms(Boolean shouldAcceptTerms) {
        this.shouldAcceptTerms = shouldAcceptTerms;
    }

    public Boolean getHasPosted() {
        return hasPosted;
    }

    public void setHasPosted(Boolean hasPosted) {
        this.hasPosted = hasPosted;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

}
