package com.trofiventures.systemsocial.interactor;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.presenter.LoginPresenter;

/**
 * Created by ajmp on 4/4/2017.
 */

public class UserInteractor {

    private static final String USER = "user";

    public static void save(SharedPreferences sharedPreferences, Gson gson, User user) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(USER, gson.toJson(user)).apply();
        edit.commit();
    }

    public static User getUser(SharedPreferences sharedPreferences, Gson gson) {
        if (sharedPreferences.contains(USER)) {
            String userSP = sharedPreferences.getString(USER, null);
            if (userSP == null)
                return null;
            User user = gson.fromJson(userSP, User.class);
            User.setUser(user);
            return user;
        }
        return null;
    }

    public static void logOut(SharedPreferences sharedPreferences) {
        String user = sharedPreferences.getString(LoginPresenter.USER, null);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        edit.putString(LoginPresenter.USER, user).apply();
        edit.commit();
        try {
            RealmController.getInstance().clearAll();
        }catch (Exception e){
            Log.e(UserInteractor.class.getCanonicalName(), e.getMessage());
        }
    }
}
