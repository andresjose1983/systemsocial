package com.trofiventures.systemsocial.interactor;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 4/6/2017.
 */

public class PostInteractor {

    private static final String FACEBOOK = "-facebook";
    private static final String FACEBOOK_PREFIX = "f";
    private static final String INSTAGRAM_PREFIX = "i";
    private static List<String> listVideosDownloading = new ArrayList<>();

    public static void declinePost(Retrofit retrofit, String token, final String postId, final Runnable runnable) {
        retrofit.create(RetrofitInterface.class).deletePost(token,
                postId.indexOf(FACEBOOK) > 0 ? postId.substring(0, postId.indexOf(FACEBOOK)) : postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        deletePost(postId);
                        if (runnable != null)
                            runnable.run();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (runnable != null)
                            runnable.run();
                    }
                });
    }

    public static void donePost(Retrofit retrofit, String token, final String postId, final Runnable runnable) {
        retrofit.create(RetrofitInterface.class).donePost(token,
                postId.indexOf(FACEBOOK) > 0 ? postId.substring(0, postId.indexOf(FACEBOOK)) : postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        dispatched(postId);
                        if (runnable != null)
                            runnable.run();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (runnable != null)
                            runnable.run();
                    }
                });
    }

    private static void dispatched(String postId) {
        int index = postId.indexOf(FACEBOOK);
        RealmController.getInstance().setSocialPostDispatched(postId);
        if (index > 0)
            RealmController.getInstance().setSocialPostDispatched(postId.substring(0, index));
        else
            RealmController.getInstance().setSocialPostDispatched(postId + FACEBOOK);
    }

    public static SocialPost getPostById(String postId) {
        return RealmController.getInstance().getSocialPost(postId);
    }

    private static void deletePost(String postId) {
        int index = postId.indexOf(FACEBOOK);
        RealmController.getInstance().deletePost(postId);
        if (index > 0)
            RealmController.getInstance().deletePost(postId.substring(0, index));
        else
            RealmController.getInstance().deletePost(postId + FACEBOOK);
    }

    public static void markTweet(SharedPreferences sharedPreferences, final String postId, Action action) {
        SharedPreferences.Editor edit = sharedPreferences.edit();

        String saved = sharedPreferences.getString(postId, null);
        if (saved == null)
            edit.putString(postId, String.valueOf(action.getId())).apply();
        else {
            edit.putString(postId, saved + "," + String.valueOf(action.getId())).apply();
        }
        edit.commit();
    }

    public static void markFacebook(SharedPreferences sharedPreferences, final String postId, Action action) {
        SharedPreferences.Editor edit = sharedPreferences.edit();

        String saved = sharedPreferences.getString(FACEBOOK_PREFIX + postId, null);
        if (saved == null)
            edit.putString(FACEBOOK_PREFIX + postId, String.valueOf(action.getId())).apply();
        else {
            edit.putString(FACEBOOK_PREFIX + postId, saved + "," + String.valueOf(action.getId())).apply();
        }
        edit.commit();
    }

    public static void markInstagram(SharedPreferences sharedPreferences, final String postId, Action action) {
        SharedPreferences.Editor edit = sharedPreferences.edit();

        String saved = sharedPreferences.getString(INSTAGRAM_PREFIX + postId, null);
        if (saved == null)
            edit.putString(INSTAGRAM_PREFIX + postId, String.valueOf(action.getId())).apply();
        else {
            edit.putString(INSTAGRAM_PREFIX + postId, saved + "," + String.valueOf(action.getId())).apply();
        }
        edit.commit();
    }

    public static String getMarkTweet(SharedPreferences sharedPreferences, String postId) {
        return sharedPreferences.getString(postId, null);
    }

    public static String getMarkFacebook(SharedPreferences sharedPreferences, String postId) {
        return sharedPreferences.getString(FACEBOOK_PREFIX + postId, null);
    }

    public static String getMarkInstagram(SharedPreferences sharedPreferences, String postId) {
        return sharedPreferences.getString(INSTAGRAM_PREFIX + postId, null);
    }

    public static void actionPost(Retrofit retrofit, PostBrand postBrand, String token,
                                  final Runnable runnable, final onFailure onFailure) {
        retrofit.create(RetrofitInterface.class)
                .actionPost(token, postBrand)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<StatusRequest>() {
                    @Override
                    public void call(StatusRequest statusRequest) {
                        if (runnable != null && statusRequest.getCode().equals("200"))
                            runnable.run();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        try {
                            JSONObject error = new JSONObject(((HttpException) throwable).response()
                                    .errorBody().string());
                            onFailure.onFailure(error.getString("msg"));
                        } catch (Exception e) {
                            onFailure.onFailure("Try it again.");
                        }
                    }
                });
    }

    public static void downloadVideo(Retrofit retrofit, final SocialPost post) {


        final String youTubeId = post.getYoutubeId();
        final String urlVideo = post.getVideoUrl();

        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);
        File file = new File(path, youTubeId + urlVideo.substring(urlVideo.lastIndexOf('.')));

        if (!file.exists() && !listVideosDownloading.contains(urlVideo)) {

            listVideosDownloading.add(urlVideo);

            Call<ResponseBody> call = retrofit.create(RetrofitInterface.class).getVideo(post.getVideoUrl());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                if (urlVideo != null && urlVideo.length() > 0)
                                    writeResponseBodyToDisk(response.body(), youTubeId, urlVideo);
                                return null;
                            }
                        }.execute();
                    } else {
                        Log.d(PostInteractor.class.getCanonicalName(), "server contact failed");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    public static void clearDownlodVideo(){
        listVideosDownloading.clear();
    }

    private static boolean writeResponseBodyToDisk(ResponseBody body, String videoId, String urlVideo) {
        try {

            File path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MOVIES);
            File file = new File(path, videoId + urlVideo.substring(urlVideo.lastIndexOf('.')));

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                if (!file.getParentFile().exists())
                    file.getParentFile().mkdirs();
                if (!file.exists())
                    file.createNewFile();

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;

                    Log.d(PostInteractor.class.getCanonicalName(), "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public interface onFailure {

        void onFailure(String message);

    }
}
