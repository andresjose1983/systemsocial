package com.trofiventures.systemsocial.interactor;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.InstagramService;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.requests.ActivationRequest;
import com.trofiventures.systemsocial.model.network.requests.NetworkData;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import eu.marcocattaneo.androidinstagramconnector.connection.Instagram;
import eu.marcocattaneo.androidinstagramconnector.connection.InstagramSession;
import eu.marcocattaneo.androidinstagramconnector.connection.implementation.InstagramListener;
import eu.marcocattaneo.androidinstagramconnector.connection.implementation.RequestCallback;
import eu.marcocattaneo.androidinstagramconnector.connection.models.ConnectionError;
import eu.marcocattaneo.androidinstagramconnector.connection.models.Scope;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 4/19/2017.
 */

public class InstagramInteractor implements Serializable {

    private Retrofit retrofit;
    private OnInstagramRequest onInstagramRequest;
    private Instagram instagram;
    private SharedPreferences sharedPreferences;
    private Gson gson;
    public static final String COUNT = "COUNT";
    public static final String COMMENT = "comment";
    public static final String ID = "ID";
    private final String COUNTS = "counts";
    private static final String MEDIA = "media";

    public InstagramInteractor(Retrofit retrofit, SharedPreferences sharedPreferences, Gson gson) {
        this.retrofit = retrofit;
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }

    /**
     * Login with instagram
     */
    public void login(final Context activity, final Brand brand,
                      final Runnable runnable, final Runnable closeListener) {
        init(activity);
        permmissions();
        instagram.getSession(new InstagramListener() {
            @Override
            public void onConnect(InstagramSession session) {
                session.execute("/users/self", new RequestCallback() {
                    @Override
                    public void onResponse(int resultCode, @Nullable String body) {
                        try {
                            JsonParser jsonParser = new JsonParser();
                            JsonObject data = jsonParser.parse(body).getAsJsonObject()
                                    .get("data").getAsJsonObject();
                            sendInstagramActivation(brand, data, runnable);
                        } catch (Exception e) {
                            if (e.getMessage().contains("Attempt to invoke virtual method 'int java.lang.String.length()' on a null object reference")) {
                                onInstagramRequest.onFailure(activity.getString(R.string.instgram_token_invalid));
                                logout(activity);
                            } else
                                onInstagramRequest.onFailure(e.getMessage());
                        }
                    }
                }, brand.getId());
            }

            @Override
            public void onError(ConnectionError error) {
                //onInstagramRequest.onFailure(error.getMessage());
            }
        }, closeListener, brand.getId(), brand.getName());
    }

    /**
     * Get instagram count
     *
     * @param activity
     * @param runnable
     */
    public void refresUserData(Context activity, final SocialPost socialPost, final Runnable runnable) {
        init(activity);
        permmissions();
        instagram.getSession(new InstagramListener() {
            @Override
            public void onConnect(InstagramSession session) {
                session.execute("/users/self", new RequestCallback() {
                    @Override
                    public void onResponse(int resultCode, @Nullable String body) {
                        try {
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonObject = jsonParser.parse(body)
                                    .getAsJsonObject().get("data").getAsJsonObject();
                            refreshCount(jsonObject);
                            if (runnable != null)
                                runnable.run();
                        } catch (Exception e) {

                        }
                    }
                }, socialPost.getBrandId());
            }

            @Override
            public void onError(ConnectionError error) {

            }
        }, null, socialPost.getBrandId(), null);
    }

    public void like(final Context activity, final SocialPost post) {
        sendPostInstagram(post, String.valueOf(NetworkType.INSTAGRAM.getId())
                , Action.LIKE, null, null);
        /*init(activity);
        permmissions();
        instagram.getSession(new InstagramListener() {
            @Override
            public void onConnect(InstagramSession session) {
                session.execute("/media/" + post.getNetworkId() + "/likes", HttpMethod.POST,
                        new RequestCallback() {
                            @Override
                            public void onResponse(int resultCode, @Nullable String body) {
                                if (resultCode == 200)
                                    sendPostInstagram(post, String.valueOf(NetworkType.INSTAGRAM.getId())
                                            , Action.LIKE, null);
                                else if (resultCode == 500)
                                    onInstagramRequest.onFailure(activity.getString(R.string.error_private_instagram_account));
                                else
                                    onInstagramRequest.onFailure(String.valueOf(resultCode));

                            }
                        }, post.getBrandId());
            }

            @Override
            public void onError(ConnectionError error) {
                onInstagramRequest.onFailure(error.getMessage());
            }
        }, null, post.getBrandId(), null);*/
    }

    public void comment(final Context activity, final SocialPost post, final Runnable runnable) {

        sendPostInstagram(post, String.valueOf(NetworkType.INSTAGRAM.getId())
                , Action.COMMENT, null, null);
        if (runnable != null)
            runnable.run();
        /*init(activity);
        permmissions();
        instagram.getSession(new InstagramListener() {
            @Override
            public void onConnect(InstagramSession session) {

                session.execute("/media/" + post.getNetworkId() + "/comments", HttpMethod.POST,
                        sharedPreferences.getString(COMMENT, null),
                        new RequestCallback() {
                            @Override
                            public void onResponse(int resultCode, @Nullable String body) {
                                if (resultCode == 200) {
                                    sendPostInstagram(post, String.valueOf(NetworkType.INSTAGRAM.getId())
                                            , Action.COMMENT, null);
                                    runnable.run();
                                } else if (resultCode == 500)
                                    onInstagramRequest.onFailure(activity.getString(R.string.error_private_instagram_account));
                                else
                                    onInstagramRequest.onFailure(String.valueOf(resultCode));

                            }
                        }, post.getBrandId());
            }

            @Override
            public void onError(ConnectionError error) {
                onInstagramRequest.onFailure(error.getMessage());
            }
        }, null, post.getBrandId(), null);*/
    }

    /**
     * Get media id instagram
     */
    public void getMedia(final SocialPost post,
                         final InstagramService.StopInstagramService stopService) {
        permmissions();
        instagram.getSession(new InstagramListener() {
            @Override
            public void onConnect(InstagramSession session) {

                session.execute("/users/" + sharedPreferences.getString(ID, null)
                        + "/media/recent", new RequestCallback() {
                    @Override
                    public void onResponse(int resultCode, @Nullable String body) {
                        try {
                            if (body != null) {
                                JSONObject postInstagram = new JSONObject(new JSONArray(new JSONObject(body).get("data").toString()).get(0).toString());
                                String mediaId = postInstagram.getString("id");
                                String link = postInstagram.getString("link");
                                Log.d("Media id", mediaId + " " + link);
                                sendPostInstagram(post, mediaId, Action.POST, stopService, link);
                            }
                        } catch (Exception e) {
                            onInstagramRequest.onFailure(e.getMessage());
                            stopService.onStop();
                        }
                    }
                }, post.getBrandId());
            }

            @Override
            public void onError(ConnectionError error) {
                onInstagramRequest.onFailure(error.getMessage());
                stopService.onStop();
            }
        }, null, post.getBrandId(), null);
    }

    private void permmissions() {
        instagram.addScope(Scope.BASIC);
        instagram.addScope(Scope.LIKES);
        instagram.addScope(Scope.PUBLIC);
        instagram.addScope(Scope.COMMENTS);
    }

    /**
     * Send link to server
     *
     * @param jsonObject
     * @param runnable
     */
    public void sendInstagramActivation(final Brand brand, final JsonObject jsonObject, final Runnable runnable) {
        ActivationRequest request = new ActivationRequest();
        request.setNetwork(NetworkType.INSTAGRAM.getId());

        String userName = jsonObject.get("username").toString().replaceAll("\"", "");
        request.setBrand(brand.getId());
        request.setUsername(userName);

        NetworkData networkData = new NetworkData();
        networkData.setId(jsonObject.get("id").toString().replaceAll("\"", ""));
        networkData.setUserId(jsonObject.get("id").toString().replaceAll("\"", ""));
        networkData.setUsername(userName);
        networkData.setBio(jsonObject.get("bio").toString().replaceAll("\"", ""));
        networkData.setImage(jsonObject.get("profile_picture").toString().replaceAll("\"", ""));
        request.setOs(com.trofiventures.systemsocial.BuildConfig.OS);
        request.setNetworkData(networkData);
        request.setUser_network_id(String.valueOf(networkData.getId()));
        request.setToken(instagram.getToken(brand.getId()));

        User user = User.getSharedUser();
        if (user.getBrands() != null && user.getBrands().size() > 0) {
            int index = user.getBrands().indexOf(brand);
            Brand brand1 = user.getBrands().get(index);
            brand1.setInstagramUsername(userName);
            user.getBrands().remove(index);
            user.getBrands().add(index, brand1);
            User.setUser(user);
            UserInteractor.save(sharedPreferences, gson, user);
        }

        Observable<ResponseBody> response = retrofit.create(RetrofitInterface.class).link(
                UIUtils.getPlayerID(), request);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        refreshCount(jsonObject);
                        if (responseBody != null)
                            User.getSharedUser().setHasLinks(true);
                        onInstagramRequest.onSuccess();
                        if (runnable != null)
                            runnable.run();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        JSONObject error = null;
                        try {
                            error = new JSONObject(((HttpException) throwable).response()
                                    .errorBody().string());
                            onInstagramRequest.onFailure(error.getString("msg"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * Save instagram count
     *
     * @param jsonObject
     */
    private void refreshCount(JsonObject jsonObject) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        try {
            Log.d("instagram json", jsonObject.toString());

            edit.putInt(COUNT, Integer.parseInt(new JSONObject(jsonObject.get(COUNTS).toString())
                    .get(MEDIA).toString())).apply();
            edit.putString(ID, jsonObject.get("id").toString().replaceAll("\"", "")).apply();
            edit.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getCount() {
        return sharedPreferences.getInt(COUNT, 0);
    }

    public boolean isLogged(final Context context, String brandId) {
        init(context);
        String instagramToken = instagram.getToken(brandId);
        if (instagramToken != null && !instagramToken.isEmpty())
            return true;
        return false;
    }

    /**
     * Validate instagram login
     *
     * @param activity
     * @param runnable
     */
    public void verifyInstagramSession(final Context activity, final Brand brand,
                                       final Runnable runnable, final Runnable closeListener) {
        login(activity, brand, runnable, closeListener);
    }

    private void sendPostInstagram(final SocialPost post, final String mediaId, final Action action,
                                   final InstagramService.StopInstagramService stopInstagramService, String link) {
        final PostBrand postBrand = new PostBrand();
        postBrand.setMetadata(mediaId.replaceAll("\"", ""));
        postBrand.setAction(action.getId());
        postBrand.setNetwork_id(mediaId.replaceAll("\"", ""));
        postBrand.setPost_id(post.getId());
        postBrand.setNetwork(NetworkType.INSTAGRAM.getId());
        postBrand.setLink(link);
        new Thread(new Runnable() {
            @Override
            public void run() {
                onInstagramRequest.onSuccess(post, postBrand, action, stopInstagramService);
            }
        }).start();
    }

    private void init(Context activity) {
        instagram = Instagram.newInstance(activity,
                BuildConfig.INSTAGRAM_CLIENT_ID, BuildConfig.INSTAGRAM_CLIENT_SECRET,
                BuildConfig.INSTAGRAM_REDIRECT_URI);
    }

    public void setOnInstagramRequest(OnInstagramRequest onInstagramRequest) {
        this.onInstagramRequest = onInstagramRequest;
    }

    public interface OnInstagramRequest {

        void onSuccess();

        void onFailure(String message);

        void onSuccess(SocialPost post, PostBrand postBrand, Action action,
                       InstagramService.StopInstagramService stopInstagramService);
    }

    /**
     * instagrm logout
     */
    public void logout(final Context context) {
        clear(context);
        instagram.logout();
    }

    public void logout(final Context context, final Runnable runnable) {
        clear(context);
        instagram.logout(runnable);
    }

    public void logOutByBrand(final Activity context, Brand brand) {
        instagram.removeTokenByBrand(context, brand.getId());
    }

    private void clear(final Context context) {
        init(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(COUNT, 0).apply();
        edit.putString(ID, null).apply();
        edit.commit();
    }
}
