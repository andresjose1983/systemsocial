package com.trofiventures.systemsocial.interactor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.DefaultAudience;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.google.gson.Gson;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.requests.ActivationRequest;
import com.trofiventures.systemsocial.model.network.requests.NetworkData;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;
import com.trofiventures.systemsocial.model.network.responses.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 4/14/2017.
 */

public class FacebookInteractor {

    private CallbackManager callbackManager;
    private Gson gson;
    private OnFacebookRequest onFacebookRequest;
    private ShareLinkContent.Builder shareLinkContent;
    private Retrofit retrofit;
    private SharedPreferences sharedPreferences;
    public static final String ALBUM_ID = "album_id";
    public static final String ALBUM_NAME = "album_name";

    public FacebookInteractor(CallbackManager callbackManager,
                              ShareLinkContent.Builder shareLinkContent,
                              Gson gson,
                              Retrofit retrofit, SharedPreferences sharedPreferences) {
        this.callbackManager = callbackManager;
        this.gson = gson;
        this.shareLinkContent = shareLinkContent;
        this.retrofit = retrofit;
        this.sharedPreferences = sharedPreferences;
    }

    /**
     * it is logged in facebook
     *
     * @return
     */
    public boolean hasAccessToken() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    /**
     * activity result facebook
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onActivityResult(SocialPost post, Action action) {
        PostBrand postBrand = new PostBrand();
        postBrand.setMetadata(null);
        postBrand.setAction(action.getId());
        postBrand.setNetwork_id(post.getNetworkId().isEmpty() ? String.valueOf(NetworkType.FACEBOOK.getId()) :
                post.getNetworkId());
        postBrand.setPost_id(post.getId());
        postBrand.setNetwork(NetworkType.FACEBOOK.getId());
        onFacebookRequest.onSuccess(post, postBrand, NetworkType.FACEBOOK);
    }


    public byte[] readBytes(String dataPath) throws IOException {

        InputStream inputStream = new FileInputStream(dataPath);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];

        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        return byteBuffer.toByteArray();
    }

    public void createAlbum(final SocialPost post) {

        Bundle createAlbumParams = new Bundle();
        createAlbumParams.putString("name", "MyPhotos");
        createAlbumParams.putString("message", " ");
        JSONObject privacy = new JSONObject();
        try {
            privacy.put("value", "EVERYONE");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        createAlbumParams.putString("privacy", privacy.toString());

        new GraphRequest(AccessToken.getCurrentAccessToken(),
                "me/albums",
                createAlbumParams,
                HttpMethod.POST,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            if (response != null && response.getConnection() != null &&
                                    response.getConnection().getResponseCode() == 200) {
                                final String idAlbum = response.getJSONObject().getString("id");
                                SharedPreferences.Editor edit = sharedPreferences.edit();
                                edit.putString(ALBUM_ID, idAlbum).apply();
                                edit.commit();
                                retrofit.create(RetrofitInterface.class)
                                        .setFacebookAlbumId(User.getSharedUser().getToken(), idAlbum)
                                        .asObservable()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Action1<Void>() {
                                            @Override
                                            public void call(Void aVoid) {
                                                addPhotoToAlbum(post, idAlbum);
                                            }
                                        }, new Action1<Throwable>() {
                                            @Override
                                            public void call(Throwable throwable) {
                                                onFacebookRequest.onFailure(throwable.getMessage());
                                            }
                                        });
                            } else {
                                if (response.getConnection() != null)
                                    onFacebookRequest.onFailure(response.getConnection().getResponseMessage());
                                else
                                    onFacebookRequest.onFailure(response.getError().getErrorMessage());
                            }
                        } catch (IOException e) {
                            onFacebookRequest.onFailure(e.getMessage());
                        } catch (JSONException e) {
                            onFacebookRequest.onFailure(e.getMessage());
                        }
                    }
                }
        ).executeAsync();
    }

   /* public void getAlbumIdByPhoto(final String photoId) {
        Bundle params = new Bundle();
        params.putString("fields", "album");
        new GraphRequest(AccessToken.getCurrentAccessToken(),
                photoId,
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            if (response != null && response.getConnection().getResponseCode() == 200) {
                                final String idAlbum = response.getJSONObject().getJSONObject("album").getString("id");
                                onFacebookRequest.onAlbumId(idAlbum, photoId);
                            } else {
                                onFacebookRequest.onFailure(response.getConnection().getResponseMessage());
                            }
                        } catch (IOException e) {
                            onFacebookRequest.onFailure(e.getMessage());
                        } catch (JSONException e) {
                            onFacebookRequest.onFailure(e.getMessage());
                        }
                    }
                }
        ).executeAsync();
    }*/

    private void uploadVideo(SocialPost post) {

        Bundle params = new Bundle();
        final String youTubeId = post.getYoutubeId();
        final String urlVideo = post.getVideoUrl();
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES);
        File file = new File(path, youTubeId + urlVideo.substring(urlVideo.lastIndexOf('.')));
        byte[] data = null;
        try {
            data = readBytes(file.getAbsoluteFile().getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        params.putByteArray("video.mp4", data);
        String description = "";
        if (post.getPostText() != null && !post.getPostText().isEmpty())
            description = post.getPostText();
        if (post.getPostUrl() != null && post.getPostUrl().length() > 0)
            description = description + " " + post.getPostUrl();

        params.putString("description", description);
            /* make the API call */
        sendFacebookPost(post, "/me/videos", params);
    }

    private void addPhotoToAlbum(SocialPost post, String idAlbums) {
        Bundle params = new Bundle();
        String message;
        // message
        if (post.getPostText() != null && !post.getPostText().isEmpty())
            message = post.getPostText();
        else
            message = post.getFacebookText();
        //Imagen
        if (post.getImgUrl() != null && post.getImgUrl().length() > 0) {
            params.putString("url", post.getImgUrl());
            if (post.getPostUrl() != null && post.getPostUrl().length() > 0)
                message = message + " " + post.getPostUrl();
            params.putString("message", message);
            /* make the API call */
            sendFacebookPost(post, "/" + idAlbums + "/photos", params);
        } else {
            if (post.getPostUrl() != null && post.getPostUrl().length() > 0)
                params.putString("link", post.getPostUrl());
            params.putString("message", message);
            sendFacebookPost(post, "me/feed", params);
        }
    }

    public void isAlbumExist(final SocialPost post, final String idAlbum, final Runnable noExist) {
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + idAlbum,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            if (response != null && response.getConnection().getResponseCode() == 200)
                                addPhotoToAlbum(post, idAlbum);
                            else {
                                if (noExist != null)
                                    noExist.run();
                                //createAlbum(post);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    /**
     * Send post facebook
     *
     * @param post
     */
    public void sendPostFacebook(final SocialPost post, Runnable runnable) {
        //Video
        if (post.getYoutubeId() != null && post.getYoutubeId().length() > 0)
            uploadVideo(post);
        else {
            String idAlbum = sharedPreferences.getString(ALBUM_ID, null);
            if (idAlbum == null) {
                //Create album
                createAlbum(post);
            } else
                isAlbumExist(post, idAlbum, runnable);
        }
    }

    /**
     * Send facebook graph post
     *
     * @param post
     * @param url
     * @param params
     */
    private void sendFacebookPost(final SocialPost post, String url, Bundle params) {
        new GraphRequest(AccessToken.getCurrentAccessToken(), url, params, HttpMethod.POST,
                facebookCallBack(post)
        ).executeAsync();
    }

    /**
     * Callback facebook Graph
     *
     * @param post
     * @return
     */
    private GraphRequest.Callback facebookCallBack(final SocialPost post) {
        return new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                try {
                    if (response != null && response.getConnection().getResponseCode() == 200) {
                        sendPostBrand(post, gson.toJson(response.getRawResponse()),
                                Action.POST, response.getJSONObject().getString("id"));
                    } else {
                        onFacebookRequest.onFailure(response.getConnection().getResponseMessage());
                    }
                } catch (IOException e) {
                    onFacebookRequest.onFailure(e.getMessage());
                } catch (JSONException e) {
                    onFacebookRequest.onFailure(e.getMessage());
                }
            }
        };
    }

    /**
     * Send post brand to server
     *
     * @param post
     * @param metaData
     * @param action
     * @param networdId
     */
    private void sendPostBrand(SocialPost post, String metaData, Action action, String networdId) {
        PostBrand postBrand = new PostBrand();
        postBrand.setMetadata(gson.toJson(metaData));
        postBrand.setAction(action.getId());
        postBrand.setNetwork_id(networdId);
        postBrand.setPost_id(post.getId().indexOf("-") > 0 ? post.getId().split("-")[0] : post.getId());
        postBrand.setNetwork(NetworkType.FACEBOOK.getId());
        onFacebookRequest.onSuccess(post, postBrand, NetworkType.FACEBOOK);
    }

    /**
     * Share in facebook
     */
    /*public void sharePostFacebook(final SocialPost post, final Activity current) {

        ShareDialog shareDialog = new ShareDialog(current);

        shareLinkContent.setContentUrl(Uri.parse(BuildConfig.FACEBOOK_URL + post.getNetworkId()));

        shareDialog.show(shareLinkContent.build(), ShareDialog.Mode.AUTOMATIC);

        shareDialog.registerCallback(callbackManager, registerCallBack(post, Action.SHARE));

    }*/
    /*private FacebookCallback<Sharer.Result> registerCallBack(final SocialPost post, final Action action) {
        return new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                sendPostBrand(post, gson.toJson(result), action, result.getPostId());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                onFacebookRequest.onFailure(error.getMessage());
            }
        };
    }*/
    public void setOnFacebookRequest(OnFacebookRequest onFacebookRequest) {
        this.onFacebookRequest = onFacebookRequest;
    }

    public interface OnFacebookRequest {
        void onFailure(String message);

        void onSuccess(SocialPost socialPost, PostBrand postBrand, NetworkType networkType);

        void onLogin(Runnable runnable);

        //void onAlbumId(String albumId, String photoId);

    }

    public void loginWithFacebook(Activity context, Runnable successBlock) {
        LoginManager.getInstance().logInWithReadPermissions(context,
                Arrays.asList("public_profile,publish_actions,user_photos,user_friends"));
        if (successBlock != null)
            initFacebook(context, successBlock);
    }

    /**
     * Login facebook
     *
     * @param context
     * @param runnable
     */
    public void initFacebook(final Context context, final Runnable runnable) {
        //FacebookSdk.sdkInitialize(context);
        final LoginManager instance = LoginManager.getInstance();
        instance.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (User.getSharedUser() != null) {
                            User.getSharedUser().setFacebookToken(loginResult.getAccessToken());
                            sendFacebookActivation(runnable);
                        }
                    }

                    @Override
                    public void onCancel() {
                        onFacebookRequest.onFailure(context.getString(R.string.error_login_facebook));
                        instance.logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        onFacebookRequest.onFailure(exception.getMessage());
                    }
                });
        instance.setDefaultAudience(DefaultAudience.EVERYONE);
        instance.setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
    }

    /**
     * Send login activation
     *
     * @param runnable
     */
    private void sendFacebookActivation(final Runnable runnable) {
        ActivationRequest request = new ActivationRequest();
        request.setToken(User.getSharedUser().getFacebookToken().getToken());
        request.setNetwork(NetworkType.FACEBOOK.getId());

        NetworkData networkData = new NetworkData();
        networkData.setAccessToken(User.getSharedUser().getFacebookToken().getToken());
        networkData.setAccessTokenSource(User.getSharedUser().getFacebookToken().getSource());
        networkData.setApplicationId(User.getSharedUser().getFacebookToken().getApplicationId());
        networkData.setUserId(User.getSharedUser().getFacebookToken().getUserId());
        networkData.setDeclinedPermissions(User.getSharedUser().getFacebookToken().getDeclinedPermissions());
        networkData.setPermissions(User.getSharedUser().getFacebookToken().getPermissions());
        networkData.setLastRefreshTime(User.getSharedUser().getFacebookToken().getLastRefresh());

        request.setUser_network_id(User.getSharedUser().getFacebookToken().getUserId());
        request.setOs(com.trofiventures.systemsocial.BuildConfig.OS);
        request.setNetworkData(networkData);
        Observable<ResponseBody> response = retrofit.create(RetrofitInterface.class).link(UIUtils.getPlayerID(), request);
        response.subscribeOn(Schedulers.io())
                .onErrorReturn(new Func1<Throwable, ResponseBody>() {
                    @Override
                    public ResponseBody call(Throwable throwable) {
                        try {
                            JSONObject error = new JSONObject(((HttpException) throwable).response().errorBody().string());
                            onFacebookRequest.onFailure(error.getString("msg"));
                        } catch (IOException e) {
                            onFacebookRequest.onFailure(e.getMessage());
                        } catch (JSONException e) {
                            onFacebookRequest.onFailure(e.getMessage());
                        }
                        return null;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        //got the data
                        if (responseBody != null)
                            onFacebookRequest.onLogin(runnable);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        
                    }
                });
    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }
}
