package com.trofiventures.systemsocial.interactor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.requests.ActivationRequest;
import com.trofiventures.systemsocial.model.network.requests.NetworkData;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;
import com.trofiventures.systemsocial.model.network.requests.TwitterUpload;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.Tweet;

import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 4/10/2017.
 */

public class TwitterInteractor {

    private TwitterApiClient twitterApiClient;
    private TwitterAuthClient twitterAuthClient;
    private OkHttpClient.Builder client;

    private OnTwitterRequest onTwitterRequest;
    private onTwitterLogin onTwitterLogin;

    private Gson gson;
    private Retrofit retrofit;

    public TwitterInteractor(Retrofit retrofit, Gson gson, OkHttpClient.Builder client,
                             TwitterAuthClient twitterAuthClient) {
        this.gson = gson;
        this.retrofit = retrofit;
        this.client = client;
        this.twitterAuthClient = twitterAuthClient;
    }

    /**
     * Validate twitter's session
     *
     * @return boolean
     */
    public boolean isValidatedSession() {
        TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        boolean result = twitterSession != null && !twitterSession.getAuthToken().isExpired();
        if (result)
            twitterApiClient = new TwitterApiClient(TwitterCore.getInstance().getSessionManager()
                    .getActiveSession(), client.build());

        return result;
    }

    /**
     * ReTweet twitter
     *
     * @param post
     */
    public void reTweet(final SocialPost post) {

        sendRequestToTwitter(twitterApiClient.getStatusesService()
                .retweet(Long.valueOf(post.getNetworkId().isEmpty() ? "" + NetworkType.TWITTER.getId() :
                        post.getNetworkId()), true), post, Action.SHARE);

    }

    /**
     * Like twitter
     *
     * @param post
     */
    public void like(final SocialPost post) {
        sendRequestToTwitter(twitterApiClient.getFavoriteService()
                .create(Long.valueOf(post.getNetworkId().isEmpty() ? "" + NetworkType.TWITTER.getId() :
                        post.getNetworkId()), true), post, Action.LIKE);
    }

    /**
     * Listener request twitter
     *
     * @param onTwitterRequest
     */
    public void setOnTwitterRequest(OnTwitterRequest onTwitterRequest) {
        this.onTwitterRequest = onTwitterRequest;
    }

    /**
     * Listener login
     *
     * @param onTwitterLogin
     */
    public void setOnTwitterLogin(TwitterInteractor.onTwitterLogin onTwitterLogin) {
        this.onTwitterLogin = onTwitterLogin;
    }

    /**
     * Upload video
     *
     * @param post
     * @param token
     * @param tweet
     */
    public void uploadTwitter(final SocialPost post, String token, String tweet) {

        retrofit.create(RetrofitInterface.class).postOnTwitter(token, new TwitterUpload(tweet, post.getId()))
                .asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<StatusRequest>() {
                    @Override
                    public void call(StatusRequest statusRequest) {
                        onTwitterRequest.onSuccess(post, Action.POST, NetworkType.TWITTER);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        try {
                            JSONObject error = new JSONObject(((HttpException) throwable).response()
                                    .errorBody().string());
                            onTwitterRequest.onFailure(error.getString("msg"));
                        } catch (Exception e) {
                            onTwitterRequest.onFailure("Try it again.");
                        }
                    }
                });
    }

    /**
     * Send only text
     *
     * @param post
     * @param tweet
     */
    public void sendTweet(final SocialPost post, final String tweet) {
        sendRequestToTwitter(twitterApiClient.getStatusesService().update(tweet, null,
                null, null, null, null, null, null, null), post, Action.POST);
    }

    /**
     * Send request to twitter
     *
     * @param response
     * @param socialPost
     * @param action
     */
    private void sendRequestToTwitter(final Call<Tweet> response, final SocialPost socialPost,
                                      final Action action) {
        response.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> responseTweeter) {
                //send tweet to server
                if (responseTweeter != null) {
                    Tweet tweet = responseTweeter.body();
                    if (tweet != null) {
                        if (action != null) {
                            PostBrand postBrand = new PostBrand();
                            postBrand.setMetadata(gson.toJson(tweet));
                            postBrand.setAction(action.getId());
                            postBrand.setNetwork_id(String.valueOf(tweet.getId()));
                            postBrand.setPost_id(socialPost.getId());
                            postBrand.setNetwork(NetworkType.TWITTER.getId());
                            PostInteractor.actionPost(retrofit, postBrand,
                                    User.getSharedUser().getToken(), new Runnable() {
                                        @Override
                                        public void run() {
                                            onTwitterRequest.onSuccess(socialPost, action, NetworkType.TWITTER);
                                        }
                                    }, new PostInteractor.onFailure() {
                                        @Override
                                        public void onFailure(String message) {
                                            onTwitterRequest.onFailure(message);
                                        }
                                    });
                        }
                    } else {
                        try {
                            JSONObject errors = new JSONObject(responseTweeter.errorBody().string());
                            JSONObject error = (JSONObject) errors.getJSONArray("errors").get(0);
                            onTwitterRequest.onFailure(error.getString("message"));
                        } catch (Exception e) {

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {

            }
        });
    }

    /**
     * Send twitter login to server
     *
     * @param userName
     * @param userId
     */
    private void sendTwitterActivation(String userName, long userId) {
        ActivationRequest request = new ActivationRequest();
        request.setToken(User.getSharedUser().getTwitterToken().token);
        request.setNetwork(NetworkType.TWITTER.getId());

        NetworkData networkData = new NetworkData();
        networkData.setAuthToken(User.getSharedUser().getTwitterToken().token);
        networkData.setAuthTokenSecret(User.getSharedUser().getTwitterToken().secret);
        networkData.setUserName(userName);
        networkData.setUserId(String.valueOf(userId));

        request.setNetworkData(networkData);
        request.setUser_network_id(String.valueOf(userId));
        request.setOs(com.trofiventures.systemsocial.BuildConfig.OS);
        final Observable<ResponseBody> response = retrofit.create(RetrofitInterface.class)
                .link(UIUtils.getPlayerID(), request);
        response.subscribeOn(Schedulers.io())
                .onErrorReturn(new Func1<Throwable, ResponseBody>() {
                    @Override
                    public ResponseBody call(Throwable throwable) {
                        StatusRequest structure = Functions.structure(throwable);
                        if (structure != null)
                            onTwitterLogin.onFailure(structure.getMsg());
                        return null;
                    }
                }).subscribe(new Action1<ResponseBody>() {
            @Override
            public void call(ResponseBody responseBody) {
                //got the data
                User.getSharedUser().setTwitterToken(User.getSharedUser().getTwitterToken());
                if (responseBody != null)
                    onTwitterLogin.onLogged();
            }
        });
    }

    /**
     * Login with twitter
     *
     * @param context
     * @param successBlock
     */
    public void loginWithTwitter(final Activity context, final Runnable successBlock) {
        twitterAuthClient.authorize(context, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> result) {
                final TwitterSession session = result.data;
                User.getSharedUser().setTwitterToken(session.getAuthToken());
                TwitterCore.getInstance().addApiClient(session, new TwitterApiClient());

                sendTwitterActivation(session.getUserName(), session.getUserId());
                if (successBlock != null)
                    successBlock.run();

            }

            @Override
            public void failure(final TwitterException e) {
                onTwitterLogin.onFailure(e.getMessage());
            }
        });
    }

    public void handleTwitterResult(int requestCode, int resultCode, Intent data) {
        twitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }

    public interface onTwitterLogin {
        void onFailure(String message);

        void onLogged();
    }

    public interface OnTwitterRequest {

        void onSuccess(SocialPost post, Action action, NetworkType networkType);

        void onFailure(String message);

    }

    public void logout(final Context context) {
        TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (twitterSession != null) {
            ClearCookies(context);
            Twitter.getSessionManager().clearActiveSession();
            Twitter.logOut();
        }
    }

    private void ClearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }
}
