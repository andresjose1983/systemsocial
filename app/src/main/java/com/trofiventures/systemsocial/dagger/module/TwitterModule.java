package com.trofiventures.systemsocial.dagger.module;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.interactor.TwitterInteractor;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by andre on 4/11/2017.
 */
@Module
public class TwitterModule {

    public TwitterModule() {
    }

    @Provides
    TwitterAuthClient provideTwitterAuthClient(){
        return new TwitterAuthClient();
    }

    @Provides
    @Singleton
    public TwitterInteractor provideTwitterInteractor(Retrofit retrofit, Gson gson,
                                                      OkHttpClient.Builder client,
                                                      TwitterAuthClient twitterAuthClient) {
        return new TwitterInteractor(retrofit, gson, client, twitterAuthClient);
    }
}
