package com.trofiventures.systemsocial.dagger.module;

import com.trofiventures.systemsocial.InstagramService;
import com.trofiventures.systemsocial.presenter.AccountsPresenter;
import com.trofiventures.systemsocial.presenter.BrandsPresenter;
import com.trofiventures.systemsocial.presenter.ChangePasswordPresenter;
import com.trofiventures.systemsocial.presenter.ForgotPresenter;
import com.trofiventures.systemsocial.presenter.InstagramBrandGroupPresenter;
import com.trofiventures.systemsocial.presenter.LoginPresenter;
import com.trofiventures.systemsocial.presenter.PostPresenter;
import com.trofiventures.systemsocial.presenter.RegisterPresenter;
import com.trofiventures.systemsocial.presenter.SettingsPresenter;
import com.trofiventures.systemsocial.ui.register.TermsAndConditionsActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by luis_ on 1/16/2017.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class, TwitterModule.class, FacebookModule.class,
        InstagramModule.class})
public interface NetComponent {
    void inject(LoginPresenter presenter);

    void inject(RegisterPresenter presenter);

    void inject(AccountsPresenter presenter);

    void inject(ForgotPresenter presenter);

    void inject(PostPresenter presenter);

    void inject(SettingsPresenter settingsPresenter);

    void inject(ChangePasswordPresenter changePasswordPresenter);

    void inject(BrandsPresenter brandsPresenter);

    void inject(InstagramService instagramService);

    void inject(TermsAndConditionsActivity termsAndConditionsActivity);

    void inject(InstagramBrandGroupPresenter instagramBrandGroupPresenter);
}