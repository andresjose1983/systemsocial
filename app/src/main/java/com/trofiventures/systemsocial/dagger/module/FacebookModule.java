package com.trofiventures.systemsocial.dagger.module;

import android.content.SharedPreferences;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.google.gson.Gson;
import com.trofiventures.systemsocial.interactor.FacebookInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by andre on 4/14/2017.
 */

@Module
public class FacebookModule {

    @Provides
    public ShareLinkContent.Builder provideShareLinkContect() {
        return new ShareLinkContent.Builder();
    }

    @Provides
    public ShareVideoContent.Builder provideShareVideoContent() {
        return new ShareVideoContent.Builder();
    }

    @Provides
    public ShareVideo.Builder provideShareVideo() {
        return new ShareVideo.Builder();
    }

    @Singleton
    @Provides
    public CallbackManager provideCallManager() {
        return CallbackManager.Factory.create();
    }

    @Provides
    public FacebookInteractor provideFacebookInteractor(CallbackManager callbackManager,
                                                        ShareLinkContent.Builder shareLinkContent,
                                                        Gson gson, Retrofit retrofit,
                                                        SharedPreferences sharedPreferences) {
        return new FacebookInteractor(callbackManager, shareLinkContent, gson, retrofit,
                sharedPreferences);
    }
}
