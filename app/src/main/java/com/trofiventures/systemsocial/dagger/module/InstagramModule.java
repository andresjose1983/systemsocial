package com.trofiventures.systemsocial.dagger.module;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by andre on 4/20/2017.
 */
@Module
public class InstagramModule {

    @Singleton
    @Provides
    InstagramInteractor provideIntegramInteractor(Retrofit retrofit, SharedPreferences sharedPreferences, Gson gson){
        return new InstagramInteractor(retrofit, sharedPreferences, gson);
    }
}
