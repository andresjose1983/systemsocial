package com.trofiventures.systemsocial.ui.register;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.widget.CompoundButton;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.databinding.ActivityRegisterBinding;
import com.trofiventures.systemsocial.model.network.requests.RegisterRequest;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.presenter.RegisterPresenter;
import com.trofiventures.systemsocial.ui.login.LoginActivity;

public class RegisterActivity extends BaseActivity implements RegisterView {

    RegisterPresenter presenter;
    ActivityRegisterBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        binding.termsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                binding.getViewModel().setTermsAgreed(isChecked);
            }
        });
        setCustomToolbar(null, false);
        presenter = new RegisterPresenter();
        presenter.attachView(this);
        presenter.onCreate();
        binding.setViewModel(new RegisterRequest());
        binding.setPresenter(presenter);
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.sending_data_dot));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    private void showMessage(String title, String message, String yes, String no,
                             Runnable yesAction, Runnable noAction) {
        UIUtils.showAlertView(this, title,
                message,
                yes,
                no,
                yesAction,
                noAction);
    }

    @Override
    public void showError(String message) {
        this.showMessage(getString(R.string.error), message, null, getString(R.string.ok).toUpperCase(),
                null, null);
    }

    @Override
    public void showErrorTerms() {
        this.showMessage(getString(R.string.missing_terms), getString(R.string.content_error_terms),
                null, getString(R.string.ok).toUpperCase(), null, null);
    }

    @Override
    public void showErrorPassword() {
        this.showMessage(getString(R.string.error), getString(R.string.password_not_match), null,
                getString(R.string.ok).toUpperCase(), null, null);
    }

    @Override
    public void showMissingActivation() {
        this.showMessage(getString(R.string.missing_activation), getString(R.string.content_missing_activation),
                null, getString(R.string.continua), null, new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
    }

    @Override
    public void showRegisterSuccess(final RegisterRequest request) {
        UIUtils.showAlertView(this, getString(R.string.success),
                getString(R.string.content_link_social_media),
                getString(R.string.si).toUpperCase(),
                getString(R.string.no).toUpperCase(),
                new Runnable() {
                    @Override
                    public void run() {
                        //go to accounts
                        // LoginPresenter loginPresenter = new LoginPresenter();
                        User user = new User();
                        user.setEmail(request.getEmail());
                        user.setPassword(request.getPassword());
                        user.setShouldAcceptTerms(false);
                        Intent intent = new Intent();
                        intent.putExtra(LoginActivity.USER_INTENT_DATA, user);
                        setResult(LoginActivity.RESULT_CODE, intent);
                        finish();
                        /*loginPresenter.makeLoginWithCallback(user, RegisterActivity.this, new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(RegisterActivity.this, HomeNavActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(intent);

                                finish();
                            }
                        });*/
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        showMissingActivation();
                    }
                });
    }

    @Override
    public void showTermsAndConditions(String termsHtml) {
        Intent intent = new Intent(RegisterActivity.this, TermsAndConditionsActivity.class);
        intent.putExtra("terms_text", termsHtml);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0) {
            binding.getViewModel().setTermsAgreed(true);
            binding.termsSwitch.setChecked(true);
        }
    }

    @Override
    public void showPrivacyPolicy() {
        Intent intent = new Intent(RegisterActivity.this, PrivacyPolicyActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void showEmailError() {
        this.showError(getString(R.string.email_not_valid));
    }

    @Override
    public void showPasswordErrorPolitics() {
        this.showMessage(getString(R.string.error), getString(R.string.error_password_politics), null,
                getString(R.string.ok).toUpperCase(), null, null);
    }
}
