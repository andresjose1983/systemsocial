package com.trofiventures.systemsocial.ui.posts;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.PostType;
import com.trofiventures.systemsocial.helper.PostBrandHelper;
import com.trofiventures.systemsocial.helper.RelinkHelper;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;

import java.util.List;

/**
 * Created by luis_ on 2/4/2017.
 */

public class SwipeDeckAdapter extends BaseAdapter {

    public List<SocialPost> data;
    private HomeNavActivity context;
    private PostInteraction listener;

    public SwipeDeckAdapter(List<SocialPost> data, HomeNavActivity context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View v = convertView;
        final SocialPost current = data.get(position);
        if (v == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.post_card, parent, false);
        }

        if (current.getType() == PostType.BRAND.getId() ||
                current.getType() == PostType.ADVOCATE.getId()) {
            v.findViewById(R.id.view_post_brand).setVisibility(View.VISIBLE);
            v.findViewById(R.id.view_message).setVisibility(View.GONE);
            v.findViewById(R.id.view_relink).setVisibility(View.GONE);
            PostBrandHelper postBrandHelper = new PostBrandHelper();
            postBrandHelper.showItem(current, context, listener, v);
            //titlePostText.setText(current.getType() == PostType.BRAND.getId()?R.string.brand_post:
            //       R.string.my_post);
        } else if (current.getType() == PostType.MESSAGE.getId()) {
            v.findViewById(R.id.view_post_brand).setVisibility(View.GONE);
            v.findViewById(R.id.view_relink).setVisibility(View.GONE);
            v.findViewById(R.id.view_message).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.title_message_text)).setText(current.getTitle());
            ((TextView) v.findViewById(R.id.content_message_text)).setText(current.getPostText());
            //titlePostText.setText(context.getString(R.string.message).toUpperCase());
            v.findViewById(R.id.read_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.read(current);
                }
            });

            User user = User.getSharedUser();
            if (user != null && user.isMultibrandAdvocate()) {
                TextView textView = (TextView) v.findViewById(R.id.name_brands_message);
                textView.setVisibility(View.VISIBLE);
                int index = user.getBrands().indexOf(new Brand(current.getBrandId()));
                if (index >= 0) {
                    Brand brand = user.getBrands().get(index);
                    if (brand != null) {
                        String brandName;
                        brandName = v.getContext().getString(R.string.brand).concat("<br/>");
                        brandName = brandName + "<b>".concat(brand.getName()).concat("</b");
                        textView.setText(Html.fromHtml(brandName));
                    }
                }
            }

        } else {
            v.findViewById(R.id.view_post_brand).setVisibility(View.GONE);
            v.findViewById(R.id.view_relink).setVisibility(View.VISIBLE);
            v.findViewById(R.id.view_message).setVisibility(View.GONE);
            RelinkHelper relinkHelper = new RelinkHelper();
            relinkHelper.action(v.findViewById(R.id.view_relink), current, listener);
        }

        return v;
    }

    public PostInteraction getListener() {
        return listener;
    }

    public void setListener(PostInteraction listener) {
        this.listener = listener;
    }
}







