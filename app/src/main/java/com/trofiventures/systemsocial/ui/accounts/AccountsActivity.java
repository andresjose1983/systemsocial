package com.trofiventures.systemsocial.ui.accounts;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import com.systemsocial.systemsocial.InstagramBrandGroupActivity;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.databinding.ActivityAccountsBinding;
import com.trofiventures.systemsocial.presenter.AccountsPresenter;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;

public class AccountsActivity extends BaseActivity implements AccountsView {

    ActivityAccountsBinding binding;
    AccountsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_accounts);
        setCustomToolbar(null, true);
        presenter = new AccountsPresenter();
        binding.setPresenter(presenter);
        presenter.attachView(this);
        presenter.onCreate();
        presenter.initFacebook(this);
        //presenter.validateLinks();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.validateLinks();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 64206) {
            presenter.handleFacebookResult(requestCode, resultCode, data);
        } else {
            presenter.handleTwitterResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.please_wait));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    @Override
    public void showSuccess(String title, String message) {
        UIUtils.showAlertView(this, title,
                message,
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showError(String message) {
        hideLoader();
        if (message != null)
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void registerWithFacebook() {
        UIUtils.showFacebookAlert(this, new Runnable() {
            @Override
            public void run() {
                presenter.loginWithFacebook(AccountsActivity.this);
            }
        });
    }

    @Override
    public void successWithFacebook() {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                binding.facebookCheck.setVisibility(View.VISIBLE);
                binding.facebookUncheck.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void registerWithTwitter() {
        UIUtils.showTwitter(this, new Runnable() {
            @Override
            public void run() {
                presenter.loginWithTwitter(AccountsActivity.this);
            }
        }, presenter.twitterCallbackManager);
    }

    @Override
    public void successWithTwitter() {
        System.out.println("Success with twitter");
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                binding.twitterCheck.setVisibility(View.VISIBLE);
                binding.twitterUncheck.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void registerWithInstagram() {
        UIUtils.showInstagram(this, new Runnable() {
            @Override
            public void run() {

            }
        }, presenter);
    }

    @Override
    public void showMissingLink() {
        UIUtils.showAlertView(this, getString(R.string.missing_activation),
                getString(R.string.message_active_account),
                getString(R.string.link_accounts),
                getString(R.string.go_to_login),
                new Runnable() {
                    @Override
                    public void run() {

                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        presenter.logOut();
                    }
                });
    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(AccountsActivity.this, HomeNavActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public void facebookLogOut() {
        UIUtils.showAlertView(this, getString(R.string.reset_account),
                getString(R.string.facebook_logout_content),
                getString(R.string.reset_account),
                getString(R.string.cancel),
                new Runnable() {
                    @Override
                    public void run() {
                        presenter.logOutFacebook();
                        binding.facebookCheck.setVisibility(View.GONE);
                        binding.facebookUncheck.setVisibility(View.VISIBLE);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        //presenter.logOut();
                    }
                });
    }

    @Override
    public void instagramLogOut() {
        UIUtils.showAlertView(this, getString(R.string.reset_account),
                getString(R.string.instagram_logout_content),
                getString(R.string.reset_account),
                getString(R.string.cancel),
                new Runnable() {
                    @Override
                    public void run() {
                        presenter.logOutInstagram();
                        binding.instagramCheck.setVisibility(View.GONE);
                        binding.instagramUncheck.setVisibility(View.VISIBLE);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        //presenter.logOut();
                    }
                });
    }

    @Override
    public void twitterLogOut() {
        UIUtils.showAlertView(this, getString(R.string.reset_account),
                getString(R.string.twitter_logout_content),
                getString(R.string.reset_account),
                getString(R.string.cancel),
                new Runnable() {
                    @Override
                    public void run() {
                        presenter.logOutTwitter();
                        binding.twitterCheck.setVisibility(View.GONE);
                        binding.twitterUncheck.setVisibility(View.VISIBLE);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                        //presenter.logOut();
                    }
                });
    }

    @Override
    public void refreshView() {
        findViewById(R.id.start_button).setVisibility(View.GONE);
        findViewById(R.id.reset_account_text_view).setVisibility(View.GONE);
        setCustomToolbar(null, false);
    }

    @Override
    public void successWithInstagram() {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                //UIUtils.hideInstagram();
                binding.instagramCheck.setVisibility(View.VISIBLE);
                binding.instagramUncheck.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void close() {
        setResult(HomeNavActivity.RESULT_HOME);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.start_button).getVisibility() != View.VISIBLE)
            finish();
    }

    public void resetAllAccounts(View view) {
        UIUtils.showAlertView(this,
                getString(R.string.accounts_removal).toUpperCase(),
                getString(R.string.message_accounts_removal),
                getString(R.string.si),
                getString(R.string.no), new Runnable() {
                    @Override
                    public void run() {
                        presenter.logOutsSocialMedia(AccountsActivity.this);
                        binding.facebookCheck.setVisibility(View.GONE);
                        binding.twitterCheck.setVisibility(View.GONE);
                        binding.instagramCheck.setVisibility(View.GONE);
                        binding.facebookUncheck.setVisibility(View.VISIBLE);
                        binding.twitterUncheck.setVisibility(View.VISIBLE);
                        binding.instagramUncheck.setVisibility(View.VISIBLE);
                    }
                }, null);
    }

    @Override
    public void gotoInstagramBrandGroup() {
        startActivity(new Intent(this, InstagramBrandGroupActivity.class));
    }

    @Override
    public void viewRefreshInstagramView() {
        binding.instagramCheck.setVisibility(View.GONE);
        binding.instagramUncheck.setVisibility(View.GONE);
        binding.instagramGroup.setVisibility(View.VISIBLE);
    }
}
