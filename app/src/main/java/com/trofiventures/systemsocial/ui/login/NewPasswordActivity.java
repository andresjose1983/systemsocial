package com.trofiventures.systemsocial.ui.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.databinding.ActivityNewPasswordBinding;
import com.trofiventures.systemsocial.model.network.requests.ForgotRequest;
import com.trofiventures.systemsocial.presenter.ForgotPresenter;

public class NewPasswordActivity extends BaseActivity implements ForgotView {
    ForgotPresenter presenter;
    ActivityNewPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(NewPasswordActivity.this, R.layout.activity_new_password);
        setCustomToolbar(null, false);
        ForgotRequest request = new ForgotRequest();
        presenter = new ForgotPresenter();
        binding.setPresenter(presenter);
        presenter.onCreate();
        presenter.attachView(this);

        if (getIntent().hasExtra("email")) {
            request.setEmail(getIntent().getExtras().get("email").toString());
            binding.newEmail.setText(request.getEmail());
        }

        binding.setViewModel(request);
    }


    @Override
    protected void onResume() {
        super.onResume();
        System.out.println(binding);
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.sending_data_dot));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    @Override
    public void showError(String message) {
        UIUtils.showAlertView(this, getString(R.string.error),
                message,
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showMessage(String message) {
        UIUtils.showAlertView(this, getString(R.string.success),
                message,
                getString(R.string.continua).toUpperCase(),
                null,
                new Runnable() {
                    @Override
                    public void run() {
                        setResult(2);
                        finish();
                    }
                },
                null);
    }

    @Override
    public void showNewPassword() {

    }

    @Override
    public void showInvalidEmail() {
        hideLoader();
        UIUtils.showAlertView(this, getString(R.string.error),
                getString(R.string.email_not_valid),
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showInvalidPin() {
        UIUtils.showAlertView(this, getString(R.string.error),
                getString(R.string.invalid_pin),
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showInvalidPasswords() {
        UIUtils.showAlertView(this, getString(R.string.error),
                getString(R.string.password_do_not_match),
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showInvalidPassword() {
        UIUtils.showAlertView(this, getString(R.string.error),
                getString(R.string.weak_password),
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showPasswordReset() {
        UIUtils.showAlertView(this, getString(R.string.success),
                getString(R.string.password_reset),
                getString(R.string.continua).toUpperCase(),
                null,
                new Runnable() {
                    @Override
                    public void run() {
                        setResult(2);
                        finish();
                    }
                },
                null);
    }
}
