package com.trofiventures.systemsocial.ui.login;


import android.app.Activity;

import com.trofiventures.systemsocial.common.View;
import com.trofiventures.systemsocial.presenter.LoginPresenter;

/**
 * Created by luis_ on 1/15/2017.
 */
public interface LoginView  extends View {
    void showLoader();
    void hideLoader();
    void showLoginError(String message);
    void showLoginSuccess(String message);
    void showRegister();
    void loginSuccessfull();
    void showReset();
    void showForgot();
    Activity getActivity();
    void requestCallPermission();

    void showUpdateVersion(boolean showNoteContent);
}
