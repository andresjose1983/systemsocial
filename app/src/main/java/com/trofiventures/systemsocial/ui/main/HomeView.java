package com.trofiventures.systemsocial.ui.main;

import android.net.Uri;

import com.trofiventures.systemsocial.common.View;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.StatusRequest;

import java.util.List;

/**
 * Created by luis_ on 2/5/2017.
 */

public interface HomeView extends View {

    void showSuccessPost();

    void showPassed();

    void showLoader();

    void hideLoader();

    void showError(String title, String message);

    void showMessage(String message);

    void showNewPosts(List<SocialPost> response);

    void goToLogin(StatusRequest statusRequest);

    void showMessageSessionExpirated();

    void showInstagram(SocialPost socialPost, Uri uri, String type, Runnable runnable);

    void showFileError();

    void showFacebookAlbum(final SocialPost post);

    void updateBadget(int totalPost);

    void updateToolBar(StatusRequest statusRequest);

    //void facebookAlbumId(String albumId, String photoId);
}
