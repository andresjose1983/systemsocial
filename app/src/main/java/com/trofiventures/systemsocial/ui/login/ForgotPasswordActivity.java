package com.trofiventures.systemsocial.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.network.requests.ForgotRequest;
import com.trofiventures.systemsocial.presenter.ForgotPresenter;

public class ForgotPasswordActivity extends BaseActivity implements ForgotView {
    ForgotPresenter presenter;
    private TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        email = (TextView) findViewById(R.id.forgot_email);
        setCustomToolbar(null, false);

        findViewById(R.id.forgot_reset_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgotPasswordActivity.this, NewPasswordActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        findViewById(R.id.send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotRequest request = new ForgotRequest();
                request.setEmail(email.getText().toString());
                presenter.sendForgotRequest(request);
            }
        });

        presenter = new ForgotPresenter();
        presenter.onCreate();
        presenter.attachView(this);
    }

    @Override
    public void showInvalidEmail() {
        hideLoader();
        UIUtils.showAlertView(this, getString(R.string.error),
                getString(R.string.email_not_valid),
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.sending_data_dot));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    @Override
    public void showError(String message) {
        UIUtils.showAlertView(this, getString(R.string.error),
                message,
                null,
                getString(R.string.ok).toUpperCase(),
                null,
                null);
    }

    @Override
    public void showMessage(String message) {
        UIUtils.showAlertView(this, getString(R.string.success),
                message,
                getString(R.string.continua).toUpperCase(),
                null,
                new Runnable() {
                    @Override
                    public void run() {
                        showNewPassword();
                    }
                },
                null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 2) {
            finish();
        }
    }

    public void showNewPassword() {
        Intent intent = new Intent(ForgotPasswordActivity.this, NewPasswordActivity.class);
        intent.putExtra("email", email.getText().toString());
        startActivityForResult(intent, 1);
    }

    @Override
    public void showInvalidPin() {

    }

    @Override
    public void showInvalidPasswords() {

    }

    @Override
    public void showInvalidPassword() {

    }

    @Override
    public void showPasswordReset() {

    }
}
