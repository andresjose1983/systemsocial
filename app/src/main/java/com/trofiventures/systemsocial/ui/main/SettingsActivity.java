package com.trofiventures.systemsocial.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.systemsocial.systemsocial.InstagramBrandGroupActivity;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.presenter.SettingsPresenter;
import com.trofiventures.systemsocial.ui.accounts.AccountsActivity;
import com.trofiventures.systemsocial.ui.brand.BrandActivity;
import com.trofiventures.systemsocial.ui.changePassword.ChangePasswordActivity;
import com.trofiventures.systemsocial.ui.register.PrivacyPolicyActivity;
import com.trofiventures.systemsocial.ui.register.TermsAndConditionsActivity;
import com.trofiventures.systemsocial.ui.settings.SettingsView;

public class SettingsActivity extends BaseActivity implements SettingsView {

    SettingsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setCustomToolbar(null, false);

        presenter = new SettingsPresenter();
        presenter.onCreate();
        presenter.attachView(this);

        if (User.getSharedUser().getUserType().equals("MANAGER")) {
            findViewById(R.id.instagram_accounts_text_view).setVisibility(View.VISIBLE);
            findViewById(R.id.social_media_text_view).setVisibility(View.GONE);
            findViewById(R.id.brands_text_view).setVisibility(View.GONE);
            findViewById(R.id.brands_view).setVisibility(View.GONE);
        } else {
            findViewById(R.id.instagram_accounts_text_view).setVisibility(View.GONE);
            findViewById(R.id.social_media_text_view).setVisibility(View.VISIBLE);
            findViewById(R.id.brands_text_view).setVisibility(View.VISIBLE);
            findViewById(R.id.brands_view).setVisibility(View.VISIBLE);
        }
    }

    public void showTermsAndConditions(View view) {
        presenter.loadTermsAndConditions();
    }

    public void showPrivacyPolicy(View view) {
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void showTermsAndConditions(String termsHtml) {
        Intent intent = new Intent(this, TermsAndConditionsActivity.class);
        intent.putExtra("terms_text", termsHtml);
        startActivityForResult(intent, 1);
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.please_wait));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    public void changePassword(View view) {
        startActivity(new Intent(this, ChangePasswordActivity.class));
    }

    private void showMessage(String title, String message, String yes, String no,
                             Runnable yesAction, Runnable noAction) {
        UIUtils.showAlertView(this, title,
                message,
                yes,
                no,
                yesAction,
                noAction);
    }

    @Override
    public void showError(String message) {
        this.showMessage(getString(R.string.error), message, null, getString(R.string.ok).toUpperCase(),
                null, null);
    }

    public void accounts(View view) {
        startActivity(new Intent(this, AccountsActivity.class));
    }

    public void Instagramaccounts(View view) {
        startActivity(new Intent(this, InstagramBrandGroupActivity.class));
    }

    public void brands(View view) {
        startActivity(new Intent(this, BrandActivity.class));
    }
}
