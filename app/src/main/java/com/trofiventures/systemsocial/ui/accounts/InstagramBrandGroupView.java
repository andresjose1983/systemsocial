package com.trofiventures.systemsocial.ui.accounts;

import com.trofiventures.systemsocial.common.View;
import com.trofiventures.systemsocial.model.network.responses.Brand;

import java.util.List;

/**
 * Created by luis_ on 1/25/2017.
 */
public interface InstagramBrandGroupView extends View {

    void brandGroups(List<Brand> instagramBrandGroups);

    void resetInstagramAccount(Brand brand);

}
