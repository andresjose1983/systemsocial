package com.trofiventures.systemsocial.ui.register;

/**
 * Created by luis_ on 3/6/2017.
 */
public class TermsEntity {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
