package com.trofiventures.systemsocial.ui.register;

import android.os.Bundle;
import android.webkit.WebView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;

public class PrivacyPolicyActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        setCustomToolbar(null, false);
        WebView webView = (WebView) findViewById(R.id.terms_web_view);
        webView.loadUrl("file:///android_asset/privacy.html");
    }
}
