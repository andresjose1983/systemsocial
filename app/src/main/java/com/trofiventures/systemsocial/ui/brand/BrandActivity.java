package com.trofiventures.systemsocial.ui.brand;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.adapter.BrandsAdapter;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.presenter.BrandsPresenter;

import java.util.List;

public class BrandActivity extends BaseActivity implements BrandView {

    private BrandsPresenter brandsPresenter;

    RecyclerView brandsRecycler;

    private BrandsAdapter brandsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);

        setCustomToolbar(null, false);

        brandsPresenter = new BrandsPresenter();
        brandsPresenter.onCreate();
        brandsPresenter.attachView(this);
        brandsRecycler = (RecyclerView) findViewById(R.id.brands_recycler);
        brandsAdapter = new BrandsAdapter();
        brandsRecycler.setHasFixedSize(true);
        brandsRecycler.setAdapter(brandsAdapter);
        brandsRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected void onResume() {
        super.onResume();
        brandsPresenter.getBrands();
    }

    @Override
    public void showBrands(List<Brand> brands) {
        brandsAdapter.setBrands(brands);
        brandsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.sending_data_dot));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }
}
