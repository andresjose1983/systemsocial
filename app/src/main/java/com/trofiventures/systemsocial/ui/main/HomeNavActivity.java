package com.trofiventures.systemsocial.ui.main;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.helper.Util;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.presenter.PostPresenter;
import com.trofiventures.systemsocial.ui.accounts.AccountsActivity;
import com.trofiventures.systemsocial.ui.login.LoginActivity;
import com.trofiventures.systemsocial.ui.posts.PostsDeckFragment;
import com.trofiventures.systemsocial.ui.register.TermsAndConditionsActivity;

import java.util.List;

public class HomeNavActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeView, PostsDeckFragment.DeckCallback {

    PostPresenter presenter;
    private Toolbar toolbar;
    private ImageView logoImage;
    private PostsDeckFragment mPostsFragment;
    private ProgressDialog progressDialogPost;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    public static final int RESULT_HOME = 1501;
    private TextView totalPostTextView;
    private View viewTotalPost;
    public static final String HIDE_LOADER_FILTER = "com.trofiventures.systemsocial.ui.main.HIDE_LOADER_FILTER";
    public static final String HIDE_NOTIFICATIONS = "com.trofiventures.systemsocial.ui.main.HIDE_NOTIFICATIONS";
    private boolean isActive;
    public static boolean canRefreshPosts;

    public final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(HIDE_LOADER_FILTER))
                hideLoader();
            else {
                if (isActive) {
                    Util.removeNotifications(HomeNavActivity.this);
                    presenter.refreshPost();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_nav);
        if (User.getSharedUser() == null) {
            Intent loginIntent = new Intent(HomeNavActivity.this, LoginActivity.class);
            startActivityForResult(loginIntent, RESULT_HOME);
        }
        setTitle(null);
        presenter = new PostPresenter();
        presenter.onCreate();
        presenter.attachView(this);

        getWindow().setBackgroundDrawable(null);

        toolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(null);
        setSupportActionBar(toolbar);

        logoImage = (ImageView) findViewById(R.id.logo_image_view);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        //toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        progressDialogPost = new ProgressDialog(this);

        progressDialogPost.setCancelable(false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    protected void onResume() {
        super.onResume();
        User user = User.getSharedUser();
        if (user != null) {
            if (!user.getHasLinks()) {
                Intent intent = new Intent(HomeNavActivity.this, AccountsActivity.class);
                startActivityForResult(intent, RESULT_HOME);
            } else {
                if (!canRefreshPosts) {
                    presenter.refreshPost();
                }
                canRefreshPosts = false;
                if (user.getShouldAcceptTerms() != null && user.getShouldAcceptTerms()) {
                    startActivity(new Intent(this, TermsAndConditionsActivity.class));
                    user.setShouldAcceptTerms(false);
                    User.setUser(user);
                }

            }

            if (!user.isMultibrandAdvocate())
                updateToolBar(user.getBrandColor(), user.getBrandLogo());
            else
                updateToolBar();
            //Refresh token
            presenter.refresh();
        }
        drawer.openDrawer(GravityCompat.START);  // OPEN DRAWER
        drawer.closeDrawer(GravityCompat.START);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(HIDE_NOTIFICATIONS);
        intentFilter.addAction(HIDE_LOADER_FILTER);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
        isActive = true;
        Util.removeNotifications(HomeNavActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_nav, menu);
        MenuItem item = menu.findItem(R.id.see_posts);
        android.view.View view = item.getActionView();
        totalPostTextView = (TextView) view.findViewById(R.id.total_post_text_view);
        viewTotalPost = view.findViewById(R.id.view_total_post);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.see_posts) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_post:
                presenter.getPosts();
                break;
            case R.id.nav_settings:
                Intent intent = new Intent(HomeNavActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            /*case R.id.nav_logouts:
                UIUtils.showAlertView(this,
                        getString(R.string.accounts_removal).toUpperCase(),
                        getString(R.string.message_accounts_removal),
                        getString(R.string.si),
                        getString(R.string.no), new Runnable() {
                            @Override
                            public void run() {
                                presenter.logOutsSocialMedia(HomeNavActivity.this);
                            }
                        }, null);
                break;*/
            case R.id.nav_logout:
                deckEmpty();
                totalPostTextView.setText(String.valueOf(0));
                viewTotalPost.setVisibility(View.INVISIBLE);
                presenter.logOutsSocialMedia(HomeNavActivity.this);
                presenter.logOut();
                break;
            case R.id.nav_help:
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(this, Uri.parse(BuildConfig.HELP_URL));
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showSuccessPost() {
        Toast.makeText(this, R.string.posted, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPassed() {
        Toast.makeText(this, R.string.passed, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoader() {
        progressDialogPost.setMessage(getString(R.string.please_wait));
        progressDialogPost.show();
    }

    @Override
    public void hideLoader() {
        progressDialogPost.cancel();
    }

    @Override
    public void showError(String title, String message) {
        hideLoader();
    }

    @Override
    public void showMessage(String message) {
        hideLoader();
        this.showMessage(getString(R.string.error), message, null, getString(R.string.ok).toUpperCase(),
                null, null);
    }

    private void showMessage(String title, String message, String yes, String no,
                             Runnable yesAction, Runnable noAction) {
        hideLoader();
        UIUtils.showAlertView(this, title,
                message,
                yes,
                no,
                yesAction,
                noAction);
    }

    @Override
    public void showNewPosts(final List<SocialPost> response) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (response != null && response.size() > 0) {
                        viewTotalPost.setVisibility(android.view.View.VISIBLE);
                        totalPostTextView.setText(String.valueOf(response.size()));
                        mPostsFragment = new PostsDeckFragment();
                        mPostsFragment.setCallback(HomeNavActivity.this);
                        mPostsFragment.setHomeView(HomeNavActivity.this);

                        mPostsFragment.setSocialPosts(response);
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_post_layout, mPostsFragment).commit();
                    } else {
                        deckEmpty();
                        viewTotalPost.setVisibility(android.view.View.INVISIBLE);
                    }
                } catch (Exception e) {
                    Log.d(HomeNavActivity.class.getCanonicalName(), e.getMessage());
                }
            }
        }, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        canRefreshPosts = true;

        if (resultCode == RESULT_HOME) {
            if (User.getSharedUser() != null && User.getSharedUser().getHasLinks())
                presenter.getPosts();
        } else {
            if (mPostsFragment != null)
                mPostsFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void deckEmpty() {
        if (mPostsFragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(mPostsFragment);
            ft.commit();
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void goToLogin(StatusRequest statusRequest) {
        Intent intent = new Intent(HomeNavActivity.this, LoginActivity.class);
        if (statusRequest != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(LoginActivity.STATUS_REQUEST_INTENT_DATA, statusRequest);
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    public void showProgressDialogWait() {
        progressDialogPost.setTitle(R.string.please_wait);
        progressDialogPost.show();
    }

    public void showProgressDialog() {
        progressDialogPost.setTitle(R.string.sending_data_dot);
        progressDialogPost.show();
    }

    @Override
    public void showMessageSessionExpirated() {
        Toast.makeText(this, R.string.session_expired, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInstagram(SocialPost socialPost, Uri uri, String type, Runnable runnable) {
        mPostsFragment.showInstagram(socialPost, uri, type, runnable);
        canRefreshPosts = true;
    }

    @Override
    public void showFileError() {
        Toast.makeText(this, R.string.error_file_downloading, Toast.LENGTH_SHORT).show();
    }

    public TextView getTotalPostTextView() {
        return totalPostTextView;
    }

    public View getViewTotalPost() {
        return viewTotalPost;
    }

    @Override
    public void showFacebookAlbum(final SocialPost post) {
        presenter.createFacebookAlbum(HomeNavActivity.this, post, new Runnable() {
            @Override
            public void run() {
                presenter.sendPostFacebook(post, HomeNavActivity.this);
            }
        });
    }

    /*@Override
    public void facebookAlbumId(String albumId, String photoId) {
        UIUtils.hideProgressDialog();
        Uri uri = Uri.parse("fb://post/" + albumId + "_" + photoId);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        startActivity(intent);
    }*/

    @Override
    public void updateBadget(int totalPosts) {
        if (totalPosts == 0)
            viewTotalPost.setVisibility(android.view.View.INVISIBLE);
        else {
            viewTotalPost.setVisibility(View.VISIBLE);
            totalPostTextView.setText(String.valueOf(totalPosts));
        }
    }

    @Override
    public void updateToolBar(StatusRequest statusRequest) {
        if (statusRequest != null)
            updateToolBar(statusRequest.getBrandColor(), statusRequest.getBrandLogo());
    }

    private void updateToolBar() {
        int color = ContextCompat.getColor(this, R.color.primary);
        toolbar.setBackgroundColor(color);
        Picasso.with(this)
                .load(R.drawable.bar_logo)
                .into(logoImage);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    private void updateToolBar(String brandColor, String brandLogo) {
        int color;
        if (brandColor != null)
            color = Color.parseColor("#" + brandColor);
        else
            color = ContextCompat.getColor(this, R.color.primary);

        toolbar.setBackgroundColor(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }

        if (brandLogo != null)
            Picasso.with(this)
                    .load(Uri.parse(brandLogo))
                    .resize(48, 48)
                    .centerInside()
                    .into(logoImage);
        else
            Picasso.with(this)
                    .load(R.drawable.bar_logo)
                    .into(logoImage);

    }
}