package com.trofiventures.systemsocial.ui.changePassword;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.widget.Toast;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.databinding.ActivityChangePasswordBinding;
import com.trofiventures.systemsocial.model.network.requests.ChangePassword;
import com.trofiventures.systemsocial.presenter.ChangePasswordPresenter;

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordView {

    private ChangePasswordPresenter changePasswordPresenter;
    ActivityChangePasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);

        setCustomToolbar(null, false);

        changePasswordPresenter = new ChangePasswordPresenter();
        changePasswordPresenter.onCreate();
        changePasswordPresenter.attachView(this);
        binding.setPresenter(changePasswordPresenter);
        binding.setViewModel(new ChangePassword());

    }

    @Override
    public void showError(String error) {
        this.showMessage(getString(R.string.error), error, null,
                getString(R.string.ok).toUpperCase(), null, null);
    }

    @Override
    public void showErrorPassword() {
        this.showMessage(getString(R.string.error), getString(R.string.password_not_match), null,
                getString(R.string.ok).toUpperCase(), null, null);
    }

    private void showMessage(String title, String message, String yes, String no,
                             Runnable yesAction, Runnable noAction) {
        UIUtils.showAlertView(this, title,
                message,
                yes,
                no,
                yesAction,
                noAction);
    }

    @Override
    public void showPasswordEmpty() {
        this.showMessage(getString(R.string.error), getString(R.string.password_empty), null,
                getString(R.string.ok).toUpperCase(), null, null);
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.sending_data_dot));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    @Override
    public void showPasswordChanged() {
        Toast.makeText(this, R.string.password_changed, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void showPasswordErrorPolitics() {
        this.showMessage(getString(R.string.error), getString(R.string.error_password_politics), null,
                getString(R.string.ok).toUpperCase(), null, null);
    }
}
