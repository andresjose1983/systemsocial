package com.trofiventures.systemsocial.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.databinding.ActivityLoginBinding;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.presenter.LoginPresenter;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;
import com.trofiventures.systemsocial.ui.register.RegisterActivity;
import com.trofiventures.systemsocial.ui.register.UserAgreementActivity;

public class LoginActivity extends BaseActivity implements LoginView {

    LoginPresenter mPresenter;
    ActivityLoginBinding mBinder;
    private static int STORAGE_PERMISSION_CODE = 102;
    public final static int RESULT_CODE = 500;
    public final static String USER_INTENT_DATA = "USER";
    public final static String STATUS_REQUEST_INTENT_DATA = "STATUS_REQUEST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinder = DataBindingUtil.setContentView(this, R.layout.activity_login);

        mPresenter = new LoginPresenter();
        mPresenter.attachView(this);
        mPresenter.onCreate();

        mBinder.setPresenter(mPresenter);

        User user = new User();
        user.setEmail(mPresenter.getUserEmail());
        mBinder.setUser(user);

        if (getIntent().hasExtra(STATUS_REQUEST_INTENT_DATA)) {
            StatusRequest statusRequest = (StatusRequest) getIntent().getExtras()
                    .get(STATUS_REQUEST_INTENT_DATA);
            if (statusRequest != null)
                mPresenter.validateStatusRequest(statusRequest);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    public void showLoader() {
        UIUtils.showProgressDialog(this, getString(R.string.login_dot));
    }

    @Override
    public void hideLoader() {
        UIUtils.hideProgressDialog();
    }

    @Override
    public void showLoginError(String message) {
        UIUtils.showFadeInText(mBinder.loginErrorLabel, message);
    }

    @Override
    public void showLoginSuccess(String message) {
        UIUtils.showFadeInText(mBinder.loginErrorLabel, message, 0,
                ContextCompat.getColor(this, R.color.primary));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CODE && data != null) {
            User user = (User) data.getExtras().get(USER_INTENT_DATA);
            mPresenter.makeLogin(user);
        }
    }

    @Override
    public void showRegister() {
        Intent intent = new Intent(LoginActivity.this, UserAgreementActivity.class);
        startActivityForResult(intent, RESULT_CODE);
       // Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        //startActivityForResult(intent, RESULT_CODE);
    }

    @Override
    public void loginSuccessfull() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setResult(HomeNavActivity.RESULT_HOME);
        finish();
    }

    @Override
    public void showForgot() {
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void showReset() {
        Toast.makeText(this, R.string.reset_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUpdateVersion(boolean showNoteContent) {
        UIUtils.newVersion(this, showNoteContent);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {
            View loginButton = findViewById(R.id.login_button);
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loginButton.setEnabled(true);
            } else {
                loginButton.setEnabled(false);
                //Displaying another toast if permission is not granted
                Toast.makeText(this, R.string.error_storage_permission, Toast.LENGTH_LONG).show();
            }
        }
    }

    //Requesting permission
    @Override
    public void requestCallPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            Toast.makeText(getActivity(), R.string.error_denied_storage_permission,
                    Toast.LENGTH_LONG).show();
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                STORAGE_PERMISSION_CODE);
    }
}
