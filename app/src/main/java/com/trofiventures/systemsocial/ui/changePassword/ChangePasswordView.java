package com.trofiventures.systemsocial.ui.changePassword;

import com.trofiventures.systemsocial.common.View;

/**
 * Created by andre on 5/1/2017.
 */

public interface ChangePasswordView extends View {

    void showError(String error);

    void showErrorPassword();

    void showPasswordEmpty();

    void showLoader();

    void hideLoader();

    void showPasswordChanged();

    void showPasswordErrorPolitics();
}
