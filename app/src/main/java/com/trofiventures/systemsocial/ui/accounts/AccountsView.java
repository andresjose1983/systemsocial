package com.trofiventures.systemsocial.ui.accounts;

import android.app.Activity;

import com.trofiventures.systemsocial.common.View;

/**
 * Created by luis_ on 1/25/2017.
 */
public interface AccountsView extends View {
    void showLoader();
    void hideLoader();
    void showSuccess(String title, String message);
    void showError(String message);
    void registerWithFacebook();
    void successWithFacebook();
    void registerWithTwitter();
    void successWithTwitter();
    void registerWithInstagram();
    void successWithInstagram();
    void close();
    void showMissingLink();
    void goToLogin();
    Activity getActivity();
    void facebookLogOut();
    void instagramLogOut();
    void twitterLogOut();
    void refreshView();
    void gotoInstagramBrandGroup();
    void viewRefreshInstagramView();
}
