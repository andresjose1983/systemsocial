package com.trofiventures.systemsocial.ui.posts;

import android.net.Uri;

import com.trofiventures.systemsocial.model.entities.post.SocialPost;

public interface PostInteraction {

    void loginWithFacebook(SocialPost post);

    void loginWithTwitter(SocialPost post);

    void loginWithInstagram(SocialPost post);

    void loadInstagramMedia(SocialPost post, Runnable runnable);

    void showHasPosted();

    void openFacebookBrowser(SocialPost post);

    //void likeFacebook(SocialPost post, Runnable runnable);

    void downloadVideo(SocialPost post);

    void sendPostTwitter(SocialPost post);

    void sendPostFacebook(SocialPost post);

    void reTweet(SocialPost post);

    void likeTweet(SocialPost post);

    void likeInstagram(SocialPost post);

    void commentInstagram(SocialPost post, Runnable runnable);

    String getMarkTweet(String postId);

    String getMarkFacebook(String postId);

    String getMarkInstagram(String postId);

    void pass(SocialPost post);

    void done(SocialPost post);

    void read(SocialPost post);

    void showInstagram(SocialPost socialPost, Uri uri, String type, Runnable runnable);
}
