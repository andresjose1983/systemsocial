package com.trofiventures.systemsocial.ui.register;

import com.trofiventures.systemsocial.common.View;
import com.trofiventures.systemsocial.model.network.requests.RegisterRequest;

/**
 * Created by luis_ on 1/21/2017.
 */
public interface RegisterView extends View {
    void showLoader();
    void hideLoader();
    void showError(String message);
    void showErrorTerms();
    void showErrorPassword();
    void showRegisterSuccess(RegisterRequest request);
    void showTermsAndConditions(String termsHtml);
    void showPrivacyPolicy();
    void showMissingActivation();
    void showEmailError();
    void showPasswordErrorPolitics();
}
