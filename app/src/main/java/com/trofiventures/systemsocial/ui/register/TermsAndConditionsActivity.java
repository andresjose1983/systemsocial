package com.trofiventures.systemsocial.ui.register;

import android.os.Bundle;
import android.text.Html;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TermsAndConditionsActivity extends BaseActivity {

    @Inject
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        setCustomToolbar(null, false);
        final WebView webView = (WebView) findViewById(R.id.terms_web_view);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        BaseApplication.getNetComponent().inject(this);

        UIUtils.showProgressDialog(this, getString(R.string.please_wait));

        retrofit2.Call<ResponseBody> response = retrofit.create(RetrofitInterface.class)
                .terms("text/html,application/json", "employee");
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String text = new Gson().fromJson(response.body().string(), TermsEntity.class).getContent();
                    webView.loadDataWithBaseURL(null, text.replace("\r\n", "<br/>").replace("\t", " "), "text/html", "UTF-8", null);
                } catch (Exception e) {
                    System.out.println(e);
                }
                UIUtils.hideProgressDialog();
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                UIUtils.hideProgressDialog();
                Toast.makeText(TermsAndConditionsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String striptHtml(String html) {
        return Html.fromHtml(html).toString();
    }
}
