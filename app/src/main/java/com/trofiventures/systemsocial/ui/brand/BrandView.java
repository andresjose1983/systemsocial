package com.trofiventures.systemsocial.ui.brand;

import com.trofiventures.systemsocial.common.View;
import com.trofiventures.systemsocial.model.network.responses.Brand;

import java.util.List;

/**
 * Created by andre on 5/1/2017.
 */

public interface BrandView extends View {

    void showBrands(List<Brand> brands);

    void showLoader();

    void hideLoader();
}
