package com.trofiventures.systemsocial.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.login.LoginActivity;

public class UserAgreementActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_agreement);
        setCustomToolbar(null, false);

        final WebView webView = (WebView) findViewById(R.id.terms_web_view);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webView.loadUrl("file:///android_asset/SS2EULA.html");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LoginActivity.RESULT_CODE && data != null) {
            User user = (User) data.getExtras().get(LoginActivity.USER_INTENT_DATA);
            user.setShouldAcceptTerms(false);
            Intent intent = new Intent();

            intent.putExtra(LoginActivity.USER_INTENT_DATA, user);
            setResult(LoginActivity.RESULT_CODE, intent);
            finish();
        }

        finish();
    }

    public void accept(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, LoginActivity.RESULT_CODE);
    }
}
