package com.trofiventures.systemsocial.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;

public class FacebookBrowserActivity extends AppCompatActivity {

    private static final String FACEBOOK_URL = "com.trofiventures.systemsocial.ui.dataIntent.FACEBOOK_URL";
    public static final String SOCIAL_POST_ID = "com.trofiventures.systemsocial.ui.dataIntent.SOCIAL_POST_ID";
    public static final int FACEBOOK_DONE_RESULT = 13251;

    public static void showForResult(Activity activity, SocialPost post, String facebookUrl) {
        Intent intent = new Intent(activity, FacebookBrowserActivity.class);
        intent.putExtra(FACEBOOK_URL, facebookUrl);
        intent.putExtra(SOCIAL_POST_ID, post.getId());
        activity.startActivityForResult(intent, FACEBOOK_DONE_RESULT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_browser);

        WebView facebookWebView = (WebView) findViewById(R.id.wv_facebook);
        facebookWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.pg_facebook).setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return false;
            }
        });

        findViewById(R.id.done_text_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(SOCIAL_POST_ID, getIntent().getExtras()
                        .getString(SOCIAL_POST_ID, null));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        findViewById(R.id.close_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        facebookWebView.loadUrl((String) getIntent().getExtras().get(FACEBOOK_URL));
        facebookWebView.getSettings().setLoadWithOverviewMode(true);
        facebookWebView.getSettings().setUseWideViewPort(true);
        facebookWebView.getSettings().setAllowFileAccess(true);
        facebookWebView.getSettings().setJavaScriptEnabled(true);
        facebookWebView.getSettings().setBuiltInZoomControls(true);
    }

    @Override
    public void onBackPressed() {

    }
}
