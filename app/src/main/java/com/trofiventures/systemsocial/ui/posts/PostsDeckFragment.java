package com.trofiventures.systemsocial.ui.posts;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.daprlabs.cardstack.SwipeDeck;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.InstagramService;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.common.PostType;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.presenter.PostPresenter;
import com.trofiventures.systemsocial.ui.FacebookBrowserActivity;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;
import com.trofiventures.systemsocial.ui.main.HomeView;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostsDeckFragment extends Fragment implements PostInteraction {

    List<SocialPost> socialPosts;
    private PostPresenter presenter;
    private SwipeDeck cardStack;
    private SwipeDeckAdapter adapter;
    private DeckCallback callback;
    private final int MOVE = 3000;
    private TextSwitcher titlePostText;
    private TextSwitcher instagramBrandName;
    private View instagramNameView;
    private Button rightButton;
    private int position;

    enum ACTION_TYPE {
        DONE,
        PASS,
        NONE
    }

    private HomeView homeView;

    private ACTION_TYPE actionType;

    public PostsDeckFragment() {
        // Required empty public constructor
    }

    public void setHomeView(HomeView homeView) {
        this.homeView = homeView;

    }

    public void setCallback(DeckCallback callback) {
        this.callback = callback;
    }

    public void setSocialPosts(List<SocialPost> socialPosts) {
        this.socialPosts = socialPosts;
        setPresenter(new PostPresenter());
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_posts_deck, container, false);
        instagramNameView = v.findViewById(R.id.instagram_name_view);

        titlePostText = (TextSwitcher) v.findViewById(R.id.title_post_text);
        instagramBrandName = (TextSwitcher) instagramNameView.findViewById(R.id.brand_name_text_view);

        setUpViewSwitcher(instagramBrandName, 14);
        setUpViewSwitcher(titlePostText, 18);

        cardStack = (com.daprlabs.cardstack.SwipeDeck) v.findViewById(R.id.swipe_deck);
        cardStack.setHardwareAccelerationEnabled(true);
        cardStack.setDrawingCacheEnabled(true);
        cardStack.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        adapter = new SwipeDeckAdapter(socialPosts, (HomeNavActivity) getActivity());
        try {
            if (socialPosts != null || !socialPosts.isEmpty())
                updateTitle(-1);
            adapter.setListener(this);
            cardStack.setAdapter(adapter);
        } catch (Exception e) {
            Log.d(PostsDeckFragment.class.getCanonicalName(), e.getMessage());
        }

        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                if (position < socialPosts.size()) {
                    final SocialPost post = socialPosts.get(position);
                    if (actionType == ACTION_TYPE.PASS) {
                        presenter.declinePost(post.getId(), new Runnable() {
                            @Override
                            public void run() {
                                cardsDepleted();
                            }
                        });
                        adapter.notifyDataSetChanged();
                    } else
                        updateTitle(++position);
                    actionType = ACTION_TYPE.NONE;
                }
            }

            @Override
            public void cardSwipedRight(int position) {
                if (position < socialPosts.size()) {
                    final SocialPost post = socialPosts.get(position);
                    if (actionType == ACTION_TYPE.DONE) {
                        final HomeNavActivity activity = (HomeNavActivity) getActivity();
                        //activity.showLoader();
                        presenter.donePost(post.getId(), new Runnable() {
                            @Override
                            public void run() {
                                //activity.hideLoader();
                                activity.showSuccessPost();
                                cardsDepleted();
                            }
                        });
                    } else
                        updateTitle(++position);
                    actionType = ACTION_TYPE.NONE;
                }
            }

            @Override
            public void cardsDepleted() {
                HomeNavActivity homeNavActivity = (HomeNavActivity) getActivity();
                if (homeNavActivity != null) {
                    TextView totalPostTextView = homeNavActivity.getTotalPostTextView();
                    if (socialPosts.size() > 0) {
                        adapter.data = socialPosts;
                        updateTitle(-1);
                        cardStack.removeAllViews();
                        cardStack.removeAllViewsInLayout();
                        adapter.notifyDataSetChanged();
                        cardStack.setAdapter(adapter);
                        totalPostTextView.setText(String.valueOf(socialPosts.size()));
                    } else {
                        cardStack.removeAllViews();
                        cardStack.removeAllViewsInLayout();
                        callback.deckEmpty();
                        totalPostTextView.setText(String.valueOf(0));
                        homeNavActivity.getViewTotalPost().setVisibility(View.INVISIBLE);
                    }
                }
                actionType = ACTION_TYPE.NONE;
            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });

        v.findViewById(R.id.left_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionType = ACTION_TYPE.NONE;
                cardStack.swipeTopCardLeft(MOVE);
            }
        });

        rightButton = (Button) v.findViewById(R.id.right_button);

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionType = ACTION_TYPE.NONE;
                cardStack.swipeTopCardRight(MOVE);
            }
        });

        return v;
    }

    private void setUpViewSwitcher(TextSwitcher view, final int size) {
        view.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                // create new textView and set the properties like clolr, size etc
                TextView myText = new TextView(getActivity());
                myText.setPadding(0, 3, 0, 0);
                myText.setGravity(Gravity.CENTER);
                myText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                myText.setTextSize(size);
                myText.setTextColor(Color.WHITE);
                return myText;
            }
        });
        animationTitle(android.R.anim.slide_in_left, android.R.anim.slide_out_right, view);
    }

    private void animationTitle(int left, int right, TextSwitcher textView) {
        // Declare the in and out animations and initialize them
        Animation in = AnimationUtils.loadAnimation(getActivity(), left);
        Animation out = AnimationUtils.loadAnimation(getActivity(), right);

        // set the animation type of textSwitcher
        textView.setInAnimation(in);
        textView.setOutAnimation(out);

    }

    private void updateTitle(int position) {
        this.position = position;
        if (!socialPosts.isEmpty()) {
            SocialPost post = null;
            if (position == -1 || position == socialPosts.size())
                post = socialPosts.get(0);
            else {
                if (socialPosts.size() > position)
                    post = socialPosts.get(position);
            }
            if (post != null) {
                if (post.getType() == PostType.ADVOCATE.getId()) {
                    User user = User.getSharedUser();
                    if (user != null && user.getUserType().equals("MANAGER")) {
                        updateInstagramManagerView(post);

                        titlePostText.setVisibility(View.GONE);
                        instagramNameView.setVisibility(View.VISIBLE);
                    } else {
                        titlePostText.setVisibility(View.VISIBLE);
                        instagramNameView.setVisibility(View.GONE);
                        titlePostText.setText(getActivity().getString(R.string.my_post).toUpperCase());
                    }
                } else if (post.getType() == PostType.BRAND.getId()) {
                    titlePostText.setVisibility(View.VISIBLE);
                    instagramNameView.setVisibility(View.GONE);
                    titlePostText.setText(getActivity().getString(R.string.brand_post).toUpperCase());
                } else if (post.getType() == PostType.MESSAGE.getId()) {
                    titlePostText.setVisibility(View.VISIBLE);
                    instagramNameView.setVisibility(View.GONE);
                    titlePostText.setText(getActivity().getString(R.string.message).toUpperCase());
                } else if (post.getType() == PostType.RELINK.getId()) {
                    titlePostText.setVisibility(View.VISIBLE);
                    instagramNameView.setVisibility(View.GONE);
                    titlePostText.setText(getActivity().getString(R.string.relink_needed).toUpperCase());
                }
            }
        }
    }

    @Override
    public void loginWithFacebook(final SocialPost post) {
        presenter.loginWithFacebookRunnable(getActivity(), deleteRelinkPost(post));
    }

    @Override
    public void loginWithTwitter(SocialPost post) {
        presenter.loginWithTwitterRunnable(getActivity(), deleteRelinkPost(post));
    }

    @Override
    public void loginWithInstagram(SocialPost post) {
        presenter.verifyInstagramSession(getActivity(), new Brand(post.getBrandId()),
                deleteRelinkPost(post), new Runnable() {
                    @Override
                    public void run() {
                        ((HomeNavActivity) getActivity()).hideLoader();
                    }
                });
    }

    private Runnable deleteRelinkPost(final SocialPost post) {
        return new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rightButton.callOnClick();
                        RealmController.getInstance().deletePost(post.getId());
                    }
                });
            }
        };
    }

    @Override
    public void loadInstagramMedia(final SocialPost post, final Runnable runnable) {
        ((HomeNavActivity) getActivity()).showProgressDialogWait();
        presenter.verifyInstagramSession(getActivity(), new Brand(post.getBrandId()), new Runnable() {
            @Override
            public void run() {
                ((HomeNavActivity) getActivity()).hideLoader();
                updateInstagramManagerView(post);
                if (presenter.shouldShowInstragram(getString(R.string.instruction_selected)))
                    try {
                        String instagramUserName = "";
                        int index = User.getSharedUser().getBrands().indexOf(new Brand(post.getBrandId()));
                        if (index >= 0) {
                            Brand brand = User.getSharedUser().getBrands().get(index);
                            if (User.getSharedUser().getUserType().equals("MANAGER"))
                                instagramUserName = new StringBuilder().append(" ")
                                        .append(getString(R.string.as))
                                        .append(" ")
                                        .append(brand.getInstagramUsername()).toString();
                        }
                        UIUtils.showInstagramInstructionAlertView(getActivity(), new Runnable() {
                            @Override
                            public void run() {
                                shouldShowInstragramPost(post, runnable);
                            }
                        }, instagramUserName);
                    } catch (Exception e) {
                        shouldShowInstragramPost(post, runnable);
                    }
                else
                    shouldShowInstragramPost(post, runnable);
            }
        }, new Runnable() {
            @Override
            public void run() {
                HomeNavActivity activity = ((HomeNavActivity) getActivity());
                if (activity != null)
                    activity.hideLoader();
            }
        });
    }

    @Override
    public void showHasPosted() {
        Toast.makeText(getActivity(), R.string.posted, Toast.LENGTH_SHORT).show();
    }

    private void shouldShowInstragramPost(final SocialPost post, final Runnable runnable) {
        try {
            if (presenter.shouldShowInstragram(getString(R.string.post_selected)))
                UIUtils.showPostTextInstagramAlertView(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        presenter.sendPostInstagram((HomeNavActivity) getActivity(), post, runnable);
                    }
                });
            else
                presenter.sendPostInstagram((HomeNavActivity) getActivity(), post, runnable);
        } catch (Exception e) {
            Toast.makeText(getActivity(), R.string.error_login_facebook, Toast.LENGTH_SHORT).show();
        }
    }

    private void updateInstagramManagerView(SocialPost post) {
        User user = User.getSharedUser();
        if (user != null && user.getUserType().equals("MANAGER")) {
            int index = user.getBrands().indexOf(new Brand(post.getBrandId()));
            if (index >= 0) {
                Brand brand = user.getBrands().get(index);
                if (brand != null) {
                    String instagramAccount;
                    if (brand.getInstagramUsername() == null)
                        instagramAccount = "<font color='#EE0000'>" + getString(R.string.not_linked) + "</font>";
                    else
                        instagramAccount = brand.getInstagramUsername();
                    instagramBrandName.setText(Html.fromHtml(brand.getName().concat(" / ") + instagramAccount));
                }
            }
        }
    }

    @Override
    public void showInstagram(SocialPost socialPost, Uri uri, String type, Runnable runnable) {
        String marketInstagram = "com.instagram.android";
        Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage(marketInstagram);
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setPackage(marketInstagram);

            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.setType(type);
            ((HomeNavActivity) getActivity()).hideLoader();
            presenter.startService(getActivity(), socialPost);
            if (runnable != null)
                runnable.run();
            startActivity(shareIntent);
        } else
            openGooglePlayInstagram();
    }

    private void openGooglePlayInstagram() {
        Toast.makeText(getActivity(), R.string.instagram_not_installed, Toast.LENGTH_SHORT).show();
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=com.instagram.android"));
            startActivity(intent);
        } catch (Exception e) {
            Log.d(PostsDeckFragment.class.getCanonicalName(), e.getMessage());
        }
    }

    @Override
    public void openFacebookBrowser(SocialPost post) {
        FacebookBrowserActivity.showForResult(getActivity(), post,
                BuildConfig.FACEBOOK_URL + post.getNetworkId());
        presenter.handleFacebookPostResult(post, Action.SHARE);
    }

    @Override
    public void downloadVideo(SocialPost post) {
        presenter.downloadVideo(post);
    }

    @Override
    public void sendPostTwitter(SocialPost post) {
        ((HomeNavActivity) getActivity()).showProgressDialog();
        if (post.isTwitterSelected()) {
            getPresenter().sendTwitterPost(post, getActivity());
        }
    }

    @Override
    public void sendPostFacebook(SocialPost post) {
        if (post.isFacebookSelected())
            getPresenter().sendPostFacebook(post, getActivity());
    }

    @Override
    public void reTweet(SocialPost post) {
        ((HomeNavActivity) getActivity()).showProgressDialog();
        getPresenter().reTweet(post, getActivity());
    }

    @Override
    public void likeTweet(SocialPost post) {
        ((HomeNavActivity) getActivity()).showProgressDialog();
        getPresenter().likeTweet(post, getActivity());
    }

    @Override
    public String getMarkTweet(String postId) {
        return getPresenter().getMarkTweet(postId);
    }

    @Override
    public String getMarkFacebook(String postId) {
        return getPresenter().getMarkFacebook(postId);
    }

    @Override
    public String getMarkInstagram(String postId) {
        return getPresenter().getMarkInstagram(postId);
    }

    @Override
    public void likeInstagram(final SocialPost post) {
        getPresenter().likeInstagram(getActivity(), post);
        instagramBrandAction(post);
    }

    private void instagramBrandAction(final SocialPost socialPost) {
        Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.instagram.android");
        if (intent != null) {
            intent.setData(Uri.parse(socialPost.getLink()));
            startActivity(intent);
        } else
            openGooglePlayInstagram();

    }

    @Override
    public void commentInstagram(final SocialPost post, final Runnable runnable) {
        getPresenter().commentInstagram(getActivity(), post, runnable);
        instagramBrandAction(post);
    }

    @Override
    public void pass(SocialPost post) {
        try {
            if (presenter.isDone(post.getId()))
                return;
            actionType = ACTION_TYPE.PASS;
            cardStack.swipeTopCardLeft(MOVE);
        } catch (Exception e) {
            Log.d(PostsDeckFragment.class.getCanonicalName(), e.getMessage());
        }
    }

    @Override
    public void done(SocialPost post) {
        if (InstagramService.isMyServiceRunning(getActivity())) {
            Toast.makeText(getActivity(), R.string.please_wait, Toast.LENGTH_SHORT).show();
            return;
        }
        if (presenter.isDone(post.getId()))
            read(post);
    }

    @Override
    public void read(SocialPost post) {
        actionType = ACTION_TYPE.DONE;
        cardStack.swipeTopCardRight(MOVE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 64206) {
            presenter.handleFacebookResult(requestCode, resultCode, data);
        } else if (requestCode == 64207) {
            presenter.handleFacebookPostResult(requestCode, resultCode, data);
        } else if (requestCode == FacebookBrowserActivity.FACEBOOK_DONE_RESULT
                && resultCode == Activity.RESULT_OK) {
            String socialPostId = data.getExtras()
                    .getString(FacebookBrowserActivity.SOCIAL_POST_ID, null);
            if (socialPostId != null)
                done(RealmController.getInstance().getSocialPost(socialPostId));
        } else {
            presenter.handleTwitterResult(requestCode, resultCode, data);
        }
    }

    public void emptyPosts() {
        callback.deckEmpty();
    }

    public PostPresenter getPresenter() {
        return presenter;
    }

    public void setPresenter(PostPresenter presenter) {
        this.presenter = presenter;
        this.presenter.onCreate();
        this.presenter.attachView(homeView);
    }

    public interface DeckCallback {
        void deckEmpty();
    }
}
