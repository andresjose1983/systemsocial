package com.trofiventures.systemsocial.ui.settings;

/**
 * Created by andre on 5/1/2017.
 */

public interface SettingsView extends com.trofiventures.systemsocial.common.View {

    void showTermsAndConditions(String page);

    void showLoader();

    void hideLoader();

    void showError(String message);
}
