package com.trofiventures.systemsocial.ui.login;

import com.trofiventures.systemsocial.common.View;

/**
 * Created by luis_ on 1/28/2017.
 */

public interface ForgotView extends View {

    void showPasswordReset();

    void showInvalidEmail();

    void showInvalidPassword();

    void showLoader();

    void hideLoader();

    void showError(String message);

    void showMessage(String message);

    void showNewPassword();

    void showInvalidPin();

    void showInvalidPasswords();
}

