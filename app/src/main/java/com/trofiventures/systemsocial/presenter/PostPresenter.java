package com.trofiventures.systemsocial.presenter;

import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.InstagramService;
import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.helper.Util;
import com.trofiventures.systemsocial.interactor.FacebookInteractor;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.interactor.PostInteractor;
import com.trofiventures.systemsocial.interactor.TwitterInteractor;
import com.trofiventures.systemsocial.interactor.UserInteractor;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.model.network.responses.post.Post;
import com.trofiventures.systemsocial.model.network.responses.post.PostResponse;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;
import com.trofiventures.systemsocial.ui.main.HomeView;

import java.io.File;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okhttp3.Dispatcher;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by luis_ on 1/21/2017.
 */

public class PostPresenter implements Presenter<HomeView>, TwitterInteractor.OnTwitterRequest,
        FacebookInteractor.OnFacebookRequest, InstagramInteractor.OnInstagramRequest {

    HomeView view;

    @Inject
    Retrofit retrofit;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Gson gson;

    @Inject
    TwitterInteractor twitterInteractor;
    @Inject
    FacebookInteractor facebookInteractor;
    @Inject
    InstagramInteractor instagramInteractor;
    @Inject
    Application application;
    @Inject
    Dispatcher dispatcher;

    private Subscription sLogOut;

    private AccountsPresenter accountsPresenter;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
        twitterInteractor.setOnTwitterRequest(this);
        facebookInteractor.setOnFacebookRequest(this);
        instagramInteractor.setOnInstagramRequest(this);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (sLogOut != null && sLogOut.isUnsubscribed())
            sLogOut.unsubscribe();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(HomeView view) {
        this.view = view;
    }

    /**
     * GEt user's posts
     */
    public void getPosts() {
        view.showLoader();
        retrofit.create(RetrofitInterface.class)
                .getPostData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.hideLoader();
                    }
                })
                .doOnNext(new Action1<PostResponse>() {
                    @Override
                    public void call(PostResponse postResponse) {
                        if (postResponse != null)
                            for (Post post : postResponse.getPosts())
                                RealmController.getInstance().createPost(post);
                    }
                })
                .subscribe(new Action1<PostResponse>() {
                    @Override
                    public void call(PostResponse postResponse) {
                        view.showNewPosts(RealmController.getInstance().getSocialPosts());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d(PostPresenter.class.getCanonicalName(), throwable.getMessage());
                    }
                });
    }

    /**
     * Update posts to refresh post view
     */
    public void refreshPost() {
        final boolean empty = RealmController.getInstance().getSocialPosts().isEmpty();
        retrofit.create(RetrofitInterface.class).getPostData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Action1<PostResponse>() {
                    @Override
                    public void call(PostResponse postResponse) {

                        final RealmController realmController = RealmController.getInstance();
                        RealmQuery<SocialPost> where = realmController.getSocialPosts().where();
                        if (postResponse != null) {
                            for (Post post : postResponse.getPosts())
                                where = where.notEqualTo("id", post.getId());

                            final RealmResults<SocialPost> deletePosts = where.findAll();
                            realmController.getRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    deletePosts.clear();
                                }
                            });
                        }

                        if (postResponse != null)
                            for (Post post : postResponse.getPosts())
                                RealmController.getInstance().createPost(post);
                    }
                })
                .subscribe(new Action1<PostResponse>() {
                    @Override
                    public void call(PostResponse postResponse) {

                        RealmResults<SocialPost> socialPosts = RealmController.getInstance().getSocialPosts();

                        if (empty)
                            view.showNewPosts(socialPosts);
                        else {
                            //if (postResponse.getPosts().size() != socialPosts.size()) {
                            view.showNewPosts(null);
                            if (socialPosts.size() > 0)
                                view.showNewPosts(socialPosts);
                            //}
                            view.updateBadget(socialPosts.size());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d(PostPresenter.class.getCanonicalName(), "ERROR");
                    }
                });
    }

    /**
     * @param post
     */
    public void downloadVideo(final SocialPost post) {
        PostInteractor.downloadVideo(retrofit, post);
    }


    /**
     * Send twitter post
     *
     * @param post
     * @param current
     */
    public void sendTwitterPost(final SocialPost post, final Activity current) {
        if (twitterInteractor.isValidatedSession()) {
            String tweet = Util.textToPost(post);

            // if ((post.getYoutubeId() != null && post.getYoutubeId().length() > 0) ||
            //         ((post.getImgUrl() != null && post.getImgUrl().length() > 0)))
            twitterInteractor.uploadTwitter(post, User.getSharedUser().getToken(), tweet);
            // else
            //    twitterInteractor.sendTweet(post, tweet);


        } else
            loginWithTwitterRunnable(current, new Runnable() {
                @Override
                public void run() {
                    sendTwitterPost(post, current);
                }
            });

    }

    /**
     * ReTweet in twitter
     *
     * @param post
     * @param current
     */
    public void reTweet(final SocialPost post, final Activity current) {
        if (twitterInteractor.isValidatedSession())
            twitterInteractor.reTweet(post);
        else
            loginWithTwitterRunnable(current, new Runnable() {
                @Override
                public void run() {
                    reTweet(post, current);
                }
            });
    }

    /**
     * Like in twitter
     *
     * @param post
     * @param current
     */
    public void likeTweet(final SocialPost post, final Activity current) {
        if (twitterInteractor.isValidatedSession())
            twitterInteractor.like(post);
        else
            loginWithTwitterRunnable(current, new Runnable() {
                @Override
                public void run() {
                    likeTweet(post, current);
                }
            });
    }

    /**
     * Get action submit
     *
     * @param postId
     * @return
     */
    public String getMarkTweet(String postId) {
        return PostInteractor.getMarkTweet(sharedPreferences, postId);
    }

    public void showError(final String title, final String message) {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                view.hideLoader();
                view.showError(title, message);
            }
        });
    }

    public void handleTwitterResult(int requestCode, int resultCode, Intent data) {
        if (accountsPresenter != null)
            accountsPresenter.handleTwitterResult(requestCode, resultCode, data);
    }

    /**
     * Login witter
     *
     * @param current
     * @param runnable
     */
    public void loginWithTwitterRunnable(Activity current, final Runnable runnable) {
        accountsPresenter = new AccountsPresenter();//Inject crear un modulo de presenter
        accountsPresenter.onCreate();
        accountsPresenter.attachView(view);
        accountsPresenter.loginWithTwitter(current, runnable);
    }

    /**
     * Facebook
     */
    public void handleFacebookResult(int requestCode, int resultCode, Intent data) {
        if (accountsPresenter != null)
            accountsPresenter.handleFacebookResult(requestCode, resultCode, data);
    }

    public void handleFacebookPostResult(SocialPost socialPost, Action action) {
        facebookInteractor.onActivityResult(socialPost, action);
    }

    public void handleFacebookPostResult(int requestCode, int resultCode, Intent data) {
        facebookInteractor.onActivityResult(requestCode, resultCode, data);
    }

    /*public void getAlbumByPhotoId(final Activity current, final String albumId) {
        if (facebookInteractor.hasAccessToken())
            facebookInteractor.getAlbumIdByPhoto(albumId);
        else {
            loginWithFacebookRunnable(current, new Runnable() {
                @Override
                public void run() {
                    getAlbumByPhotoId(current, albumId);
                }
            });
        }
    }*/

    /**
     * Send post facebook
     *
     * @param post
     * @param current
     */
    public void sendPostFacebook(final SocialPost post, final Activity current) {
        view.showLoader();
        if (facebookInteractor.hasAccessToken()) {
            String albumId = sharedPreferences.getString(FacebookInteractor.ALBUM_ID, null);
            if (null == albumId)
                view.showFacebookAlbum(post);
            else {
                Util.setClipboard(application, Util.textToPost(post));
                facebookInteractor.sendPostFacebook(post, new Runnable() {
                    @Override
                    public void run() {
                        view.showFacebookAlbum(post);
                    }
                });
            }
        } else {
            loginWithFacebookRunnable(current, new Runnable() {
                @Override
                public void run() {
                    sendPostFacebook(post, current);
                }
            });
        }
    }

    public void createFacebookAlbum(final Activity activity,
                                    final SocialPost post,
                                    final Runnable runnable) {
        view.showLoader();
        if (facebookInteractor.hasAccessToken()) {
            facebookInteractor.createAlbum(post);
        } else {
            loginWithFacebookRunnable(activity, new Runnable() {
                @Override
                public void run() {
                    createFacebookAlbum(activity, post, runnable);
                }
            });
        }
    }

    /*@Override
    public void onAlbumId(String albumId, String photoId) {
        view.facebookAlbumId(albumId, photoId);
    }*/

    /**
     * Share in facebook
     *
     * @param post
     * @param current
     */
    /*public void sharePostFacebook(final SocialPost post, final Activity current) {
        if (facebookInteractor.hasAccessToken()) {
            Util.setClipboard(application, Util.textToPost(post));
            facebookInteractor.sharePostFacebook(post, current);
        } else {
            loginWithFacebookRunnable(current, new Runnable() {
                @Override
                public void run() {
                    sharePostFacebook(post, current);
                }
            });
        }
    }*/

    /**
     * Get facebook mark
     *
     * @param postId
     * @return
     */
    public String getMarkFacebook(String postId) {
        return PostInteractor.getMarkFacebook(sharedPreferences, postId);
    }

    /**
     * Instagram
     */
    public void sendPostInstagram(HomeNavActivity homeNavActivity, SocialPost post, final Runnable runnable) {
        Util.setClipboard(application, Util.textToPost(post));
        if (post.getVideoUrl() != null && post.getVideoUrl().length() > 0) {
            final String youTubeId = post.getYoutubeId();
            final String urlVideo = post.getVideoUrl();
            File path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MOVIES);
            File file = new File(path, youTubeId + urlVideo.substring(urlVideo.lastIndexOf('.')));
            if (file.exists() && file.length() > 0) {
                Uri uri = FileProvider.getUriForFile(homeNavActivity,
                        BuildConfig.APPLICATION_ID + ".provider", file);

                view.showInstagram(post, uri, "video/*", runnable);
            } else {
                file = null;
                path = null;
                view.showFileError();
            }

        } else {
            File path = Environment.getExternalStorageDirectory();
            File file = new File(path, post.getId() + post.getImgUrl().substring(
                    post.getImgUrl().lastIndexOf('.')));

            if (file.exists() && file.length() > 0) {
                Uri uri = FileProvider.getUriForFile(homeNavActivity,
                        BuildConfig.APPLICATION_ID + ".provider", file);

                view.showInstagram(post, uri, "image/jpeg", runnable);
            } else {
                file = null;
                path = null;
                view.showFileError();
            }
        }
    }

    /**
     * Get instagram mark
     *
     * @param postId
     * @return
     */
    public String getMarkInstagram(String postId) {
        return PostInteractor.getMarkInstagram(sharedPreferences, postId);
    }

    public void likeInstagram(Activity activity, SocialPost post) {
        instagramInteractor.setOnInstagramRequest(this);
        instagramInteractor.like(activity, post);
    }

    public void commentInstagram(Activity activity, SocialPost post, final Runnable runnable) {
        instagramInteractor.setOnInstagramRequest(this);
        instagramInteractor.comment(activity, post, runnable);
    }

    public boolean isDone(String postId) {
        //Twitter
        String twitter = PostInteractor.getMarkTweet(sharedPreferences, postId);
        String facebook = PostInteractor.getMarkFacebook(sharedPreferences, postId);
        String instagram = PostInteractor.getMarkInstagram(sharedPreferences, postId);
        return twitter != null || facebook != null || instagram != null;
    }

    public void startService(final Activity activity, final SocialPost post) {
        Intent intent = new Intent(activity, InstagramService.class);
        intent.putExtra("ID", post.getId());
        intent.putExtra("BRAND_ID", post.getBrandId());
        activity.startService(intent);
    }

    public boolean shouldShowInstragram(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    public boolean shoulsShowFacebookLike(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    public void verifyInstagramSession(Activity activity, final Brand brand, Runnable runnable,
                                       Runnable closeListener) {
        User sharedUser = User.getSharedUser();
        if (sharedUser != null && sharedUser.getBrands() != null && sharedUser.getBrands().size() > 0) {
            int index = sharedUser.getBrands().indexOf(brand);
            if (index >= 0)
                brand.setName(sharedUser.getBrands().get(index).getName());
        }
        instagramInteractor.verifyInstagramSession(activity, brand, runnable, closeListener);
    }

    @Override
    public void onSuccess() {
        // view.showSuccessPost();
    }

    /**
     * Success twitter
     *
     * @param post
     * @param action
     */
    @Override
    public void onSuccess(SocialPost post, Action action, final NetworkType networkType) {
        if (networkType.getId() == NetworkType.TWITTER.getId()) {
            RealmController.getInstance().setSocialPostTwitterSelected(post.getId(), true);
            PostInteractor.markTweet(sharedPreferences, post.getId(), action);
            view.showSuccessPost();
        }
        view.hideLoader();
    }

    /**
     * Success facebook
     *
     * @param socialPost
     * @param postBrand
     */
    @Override
    public void onSuccess(final SocialPost socialPost, final PostBrand postBrand, final NetworkType networkType) {
        view.hideLoader();
        PostInteractor.actionPost(retrofit, postBrand,
                User.getSharedUser().getToken(), new Runnable() {
                    @Override
                    public void run() {

                        PostInteractor.markFacebook(sharedPreferences,
                                socialPost.getId(), Action.get(postBrand.getAction()));
                        view.hideLoader();
                        RealmController.getInstance().
                                setSocialPostFacebookSelected(socialPost.getId(), true);
                        view.showSuccessPost();
                    }
                }, new PostInteractor.onFailure() {
                    @Override
                    public void onFailure(String message) {
                        PostPresenter.this.onFailure(message);
                    }
                });
    }

    /**
     * Success instagram
     *
     * @param post
     * @param postBrand
     */
    @Override
    public void onSuccess(final SocialPost post, PostBrand postBrand, final Action action,
                          final InstagramService.StopInstagramService stopInstagramService) {
        PostInteractor.actionPost(retrofit, postBrand, User.getSharedUser().getToken(),
                new Runnable() {
                    @Override
                    public void run() {
                        PostInteractor.markInstagram(sharedPreferences,
                                post.getId(), action);
                        if (action.getId() != Action.POST.getId())
                            view.hideLoader();
                        RealmController.getInstance().
                                setSocialPostInstagramSelected(post.getId(), true);
                        if (stopInstagramService != null)
                            stopInstagramService.onStop();
                        view.showSuccessPost();
                    }
                }, new PostInteractor.onFailure() {
                    @Override
                    public void onFailure(String message) {
                        PostPresenter.this.onFailure(message);
                    }
                });
    }

    /**
     * failure twitter and facebook
     *
     * @param message
     */
    @Override
    public void onFailure(String message) {
        view.hideLoader();
        if (message != null)
            view.showMessage(message);
    }

    /**
     * Login facebook
     *
     * @param current
     * @param runnable
     */
    public void loginWithFacebookRunnable(Activity current, final Runnable runnable) {
        LoginManager.getInstance().logOut();
        accountsPresenter = new AccountsPresenter();//Inject crear un modulo de presenter
        accountsPresenter.onCreate();
        accountsPresenter.attachView(view);
        accountsPresenter.loginWithFacebook(current, runnable);
    }

    @Override
    public void onLogin(Runnable runnable) {
        if (runnable != null)
            runnable.run();
    }

    public void refresh() {
        sLogOut = retrofit.create(RetrofitInterface.class).checkSession(
                User.getSharedUser().getToken(),
                BuildConfig.OS,
                BuildConfig.VERSION_NAME,
                BuildConfig.INVALIDATES,
                UIUtils.getPlayerID()).asObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(new Func1<Throwable, StatusRequest>() {
                    @Override
                    public StatusRequest call(Throwable throwable) {
                        return Functions.structure(throwable);
                    }
                })
                .subscribe(new Action1<StatusRequest>() {
                    @Override
                    public void call(StatusRequest statusRequest) {

                        User user = User.getSharedUser();
                        user.setBrands(statusRequest.getBrands());
                        user.setMultibrandAdvocate(statusRequest.isMultibrandAdvocate());
                        User.setUser(user);
                        UserInteractor.save(sharedPreferences, gson, user);

                        if (!statusRequest.isValid())
                            closeSession(statusRequest);
                        else {
                            if (!user.isMultibrandAdvocate())
                                view.updateToolBar(statusRequest);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        closeSession(null);
                    }
                });

    }

    /**
     * Log out action
     */
    public void logOut() {
        cancelDownloadVideo();
        application.stopService(new Intent(application, InstagramService.class));

        sLogOut = retrofit.create(RetrofitInterface.class).logout(User.getSharedUser().getToken()).asObservable()
                .onErrorReturn(new Func1<Throwable, ResponseBody>() {
                    @Override
                    public ResponseBody call(Throwable throwable) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe();
        User.setUser(null);
        UserInteractor.logOut(sharedPreferences);
        view.goToLogin(null);
    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void cancelDownloadVideo() {
        PostInteractor.clearDownlodVideo();
        dispatcher.cancelAll();
    }

    /**
     * Logout social media network
     */
    public void logOutsSocialMedia(final Context context) {
        facebookInteractor.logout();
        twitterInteractor.logout(context);
        instagramInteractor.logout(context);
    }

    /**
     * Pass post
     *
     * @param postId
     */
    public void declinePost(String postId, Runnable runnable) {
        deleteMedia(postId);
        PostInteractor.declinePost(retrofit, User.getSharedUser().getToken(), postId, runnable);
    }

    /**
     * Done action
     *
     * @param postId
     */
    public void donePost(String postId, Runnable runnable) {
        deleteMedia(postId);
        PostInteractor.donePost(retrofit, User.getSharedUser().getToken(), postId, runnable);
    }

    private void deleteMedia(String postId) {

        try {
            SocialPost socialPost = getPostById(postId);
            File file;
            if (socialPost.getYoutubeId() != null && socialPost.getYoutubeId().length() > 0) {

                File path = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_MOVIES);
                file = new File(path, socialPost.getYoutubeId() +
                        socialPost.getVideoUrl().substring(socialPost.getVideoUrl().lastIndexOf('.')));
            } else {
                File path = Environment.getExternalStorageDirectory();
                file = new File(path, socialPost.getId() +
                        socialPost.getImgUrl().substring(socialPost.getImgUrl().lastIndexOf('.')));
            }

            Uri imageUriLcl = FileProvider.getUriForFile(application,
                    application.getPackageName() + ".provider", file);
            ContentResolver contentResolver = application.getContentResolver();
            contentResolver.delete(imageUriLcl, null, null);
            file.delete();
        } catch (Exception e) {
            Log.d(PostPresenter.class.getCanonicalName(), e.getMessage());
        }
    }

    public SocialPost getPostById(String postId) {
        return PostInteractor.getPostById(postId);
    }

    private void closeSession(StatusRequest statusRequest) {
        view.showMessageSessionExpirated();
        User.setUser(null);
        UserInteractor.logOut(sharedPreferences);
        view.goToLogin(statusRequest);
    }
}
