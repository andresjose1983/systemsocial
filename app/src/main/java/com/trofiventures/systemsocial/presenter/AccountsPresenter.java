package com.trofiventures.systemsocial.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.InstagramService;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.Action;
import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.interactor.FacebookInteractor;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.interactor.PostInteractor;
import com.trofiventures.systemsocial.interactor.TwitterInteractor;
import com.trofiventures.systemsocial.interactor.UserInteractor;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.requests.PostBrand;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.accounts.AccountsView;
import com.trofiventures.systemsocial.ui.main.HomeView;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.List;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.Subscription;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by luis_ on 1/26/2017.
 */

public class AccountsPresenter implements Presenter<AccountsView>, TwitterInteractor.onTwitterLogin,
        FacebookInteractor.OnFacebookRequest, InstagramInteractor.OnInstagramRequest {

    AccountsView view;
    HomeView homeView;
    @Inject
    Retrofit retrofit;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Gson gson;
    @Inject
    TwitterInteractor twitterInteractor;
    @Inject
    FacebookInteractor facebookInteractor;
    @Inject
    InstagramInteractor instagramInteractor;

    private Subscription sLogOut;

    public Callback<TwitterSession> twitterCallbackManager;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
        twitterInteractor.setOnTwitterLogin(this);
        facebookInteractor.setOnFacebookRequest(this);
        instagramInteractor.setOnInstagramRequest(this);
        if (User.getSharedUser() != null) {
            if (User.getSharedUser().getHasLinks()) {
                if (view != null)
                    view.refreshView();
            }
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if (sLogOut != null && sLogOut.isUnsubscribed())
            sLogOut.unsubscribe();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(AccountsView view) {
        this.view = view;
    }

    public void attachView(HomeView homeView) {
        this.homeView = homeView;
    }

    /**
     * Twiiter
     *
     * @param context
     * @param successBlock
     */
    public void loginWithTwitter(final Activity context, final Runnable successBlock) {
        twitterInteractor.loginWithTwitter(context, successBlock);
    }

    public void loginWithTwitter(final Activity context) {
        loginWithTwitter(context, null);
    }

    public void handleTwitterResult(int requestCode, int resultCode, Intent data) {
        twitterInteractor.handleTwitterResult(requestCode, resultCode, data);
    }

    /**
     * Facebook
     *
     * @param context
     */
    public void loginWithFacebook(Activity context) {
        facebookInteractor.loginWithFacebook(context, null);
    }

    public void loginWithFacebook(final Activity context, final Runnable successBlock) {
        facebookInteractor.loginWithFacebook(context, successBlock);
    }

    public void handleFacebookResult(int requestCode, int resultCode, Intent data) {
        facebookInteractor.onActivityResult(requestCode, resultCode, data);
    }

    public void initFacebook(Context context) {
        facebookInteractor.initFacebook(context, null);
    }

    public void registerToFacebook() {
        if (User.getSharedUser() != null) {
            if (User.getSharedUser().getHasLinks()) {
                if (facebookInteractor.hasAccessToken())
                    view.facebookLogOut();
                else
                    view.registerWithFacebook();
            } else
                view.registerWithFacebook();
        }
    }

    public void registerWithTwitter() {
        if (User.getSharedUser() != null) {
            if (User.getSharedUser().getHasLinks()) {
                if (twitterInteractor.isValidatedSession()) {
                    view.twitterLogOut();
                } else
                    view.registerWithTwitter();
            } else
                view.registerWithTwitter();
        }
    }

    public void registerWithInstagram() {
        if (User.getSharedUser() != null) {
            if (User.getSharedUser().getUserType().equals("EMPLOYEE")) {
                List<Brand> brands = User.getSharedUser().getBrands();
                if (User.getSharedUser().getHasLinks()) {
                    if (brands != null && brands.size() > 0)
                        if (instagramInteractor.isLogged(view.getActivity(), brands.get(0).getId())) {
                            view.instagramLogOut();
                        } else {
                            if (brands != null && brands.size() > 0)
                                instagramInteractor.login(view.getActivity(), brands.get(0),
                                        null, new Runnable() {
                                            @Override
                                            public void run() {
                                                if (view != null)
                                                    view.hideLoader();
                                            }
                                        });
                        }
                } else {
                    if (brands != null && brands.size() > 0)
                        instagramInteractor.login(view.getActivity(), brands.get(0), null, new Runnable() {
                            @Override
                            public void run() {
                                if (view != null)
                                    view.hideLoader();
                            }
                        });
                }
            } else {
                view.gotoInstagramBrandGroup();
            }
        }
    }

    public void showError(final String message) {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                if (view != null)
                    view.showError(message);
                if (homeView != null) {
                    if (message == null)
                        homeView.hideLoader();
                    else
                        homeView.showMessage(message);
                }
            }
        });
    }

    public void dismiss() {
        User user = User.getSharedUser();
        if (user != null) {
            if (user.getHasLinks()) {
                view.close();
            } else {
                view.showMissingLink();
            }
        } else {
            view.showError(view.getActivity().getString(R.string.error_login_facebook));
            logOut();
        }
    }

    @Override
    public void logOutTwitter() {
        twitterInteractor.logout(view.getActivity());
    }

    @Override
    public void logOutFacebook() {
        facebookInteractor.logout();
    }

    @Override
    public void logOutInstagram() {
        instagramInteractor.logout(view.getActivity(), null);
    }

    public void logOut() {
        if (User.getSharedUser() != null && User.getSharedUser().getToken() != null)
            sLogOut = retrofit.create(RetrofitInterface.class).logout(User.getSharedUser()
                    .getToken()).asObservable()
                    .onErrorReturn(new Func1<Throwable, ResponseBody>() {
                        @Override
                        public ResponseBody call(Throwable throwable) {
                            return null;
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .subscribe();

        User.setUser(null);
        UserInteractor.logOut(sharedPreferences);
        view.goToLogin();
    }

    /**
     * Failure twitter and facebook
     *
     * @param message
     */
    @Override
    public void onFailure(String message) {
        if (view != null)
            view.hideLoader();
        showError(message);
    }

    @Override
    public void onSuccess(final SocialPost post, final PostBrand postBrand, final Action action,
                          final InstagramService.StopInstagramService stopInstagramService) {
        PostInteractor.actionPost(retrofit, postBrand, User.getSharedUser().getToken(),
                new Runnable() {
                    @Override
                    public void run() {
                        PostInteractor.markInstagram(sharedPreferences,
                                post.getId(), action);
                        if (action.getId() != Action.POST.getId())
                            view.hideLoader();
                        RealmController.getInstance().
                                setSocialPostInstagramSelected(post.getId(), true);
                        if (stopInstagramService != null)
                            stopInstagramService.onStop();
                    }
                }, new PostInteractor.onFailure() {
                    @Override
                    public void onFailure(String message) {
                        AccountsPresenter.this.onFailure(message);
                    }
                });
    }

    @Override
    public void onSuccess(SocialPost socialPost, PostBrand postBrand, NetworkType networkType) {

    }

    @Override
    public void onLogin(Runnable runnable) {
        UserInteractor.save(sharedPreferences, gson, User.getSharedUser());
        if (runnable == null)
            view.successWithFacebook();
        else
            runnable.run();
    }

    @Override
    public void onLogged() {
        UserInteractor.save(sharedPreferences, gson, User.getSharedUser());

        if (view != null) {
            view.hideLoader();
            view.successWithTwitter();
        }
    }

    /**
     * Instagram login success
     */
    @Override
    public void onSuccess() {
        UserInteractor.save(sharedPreferences, gson, User.getSharedUser());
        if (view != null) {
            view.hideLoader();
            view.successWithInstagram();
        }
    }

    /**
     * Validate links from settings
     */
    public void validateLinks() {
        if (User.getSharedUser() != null) {
            if (User.getSharedUser().getHasLinks()) {
                if (facebookInteractor.hasAccessToken())
                    view.successWithFacebook();
                if (twitterInteractor.isValidatedSession())
                    view.successWithTwitter();
                if (User.getSharedUser().getUserType().equals("EMPLOYEE") &&
                        User.getSharedUser().getBrands().size() > 0) {
                    if (instagramInteractor.isLogged(view.getActivity(), User.getSharedUser().getBrands().get(0).getId()))
                        view.successWithInstagram();
                }
                if (User.getSharedUser().getUserType().equals("MANAGER"))
                    view.viewRefreshInstagramView();
            }
        }
    }

    /*@Override
    public void onAlbumId(String albumId, String photoId) {

    }*/

    /**
     * Logout social media network
     */
    public void logOutsSocialMedia(final Context context) {
        facebookInteractor.logout();
        twitterInteractor.logout(context);
        instagramInteractor.logout(context);
        /*retrofit.create(RetrofitInterface.class)
                .resetSystemSocialAccount(User.getSharedUser()
                        .getToken()).asObservable()
                .onErrorReturn(new Func1<Throwable, ResponseBody>() {
                    @Override
                    public ResponseBody call(Throwable throwable) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .subscribe();*/
    }
}
