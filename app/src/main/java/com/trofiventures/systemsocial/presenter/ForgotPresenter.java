package com.trofiventures.systemsocial.presenter;

import android.os.Handler;
import android.os.Looper;

import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.requests.ForgotRequest;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.login.ForgotView;

import java.util.regex.Pattern;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by luis_ on 1/15/2017.
 */
public class ForgotPresenter implements Presenter<ForgotView> {

    private ForgotView loginView;

    @Inject
    Retrofit retrofit;

    private Subscription subscriptions;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
        User.setUser(null);
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void attachView(ForgotView view) {
        this.loginView = view;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void restorePassword(final ForgotRequest request) {

        loginView.showLoader();
        if (request.getEmail() == null || request.getEmail().isEmpty() ||
                !android.util.Patterns.EMAIL_ADDRESS.matcher(request.getEmail()).matches()) {
            loginView.hideLoader();
            loginView.showInvalidEmail();
            return;
        }
        if (request.getToken() == null || request.getToken().isEmpty()) {
            loginView.hideLoader();
            loginView.showInvalidPin();
            return;
        }

        Pattern lettersPatters = Pattern.compile("[a-zA-Z]{1,1}");
        Pattern numbersPatters = Pattern.compile("[0-9]{1,1}");

        if (request.getPassword() == null || request.getPassword().isEmpty()
                || request.getPassword().length() < 6) {
            loginView.hideLoader();
            loginView.showInvalidPassword();
            return;
        } else {
            if (!(lettersPatters.matcher(request.getPassword()).find() &&
                    numbersPatters.matcher(request.getPassword()).find())) {
                loginView.hideLoader();
                loginView.showInvalidPassword();
                return;
            }
        }

        if (request.getConfirm() == null || request.getConfirm().isEmpty()
                || request.getPassword().length() < 6) {
            loginView.hideLoader();
            loginView.showInvalidPassword();
            return;
        } else {
            if (!(lettersPatters.matcher(request.getConfirm()).find() &&
                    numbersPatters.matcher(request.getConfirm()).find())) {
                loginView.hideLoader();
                loginView.showInvalidPassword();
                return;
            }
        }

        if (request.getPassword() == null || !request.getPassword().equals(request.getConfirm())) {
            loginView.hideLoader();
            loginView.showInvalidPasswords();
            return;
        }
        subscriptions = retrofit.create(RetrofitInterface.class).restorePassword(request)
                .asObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        loginView.hideLoader();
                        loginView.showPasswordReset();
                        subscriptions.unsubscribe();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        loginView.hideLoader();
                        showError(Functions.structure(throwable).getMsg());
                        subscriptions.unsubscribe();
                    }
                });
    }

    public void sendForgotRequest(final ForgotRequest request) {
        loginView.showLoader();

        if (request.getEmail() != null && request.getEmail().isEmpty() ||
                !android.util.Patterns.EMAIL_ADDRESS.matcher(request.getEmail()).matches()) {
            loginView.showInvalidEmail();
            return;
        }

        Observable<ResponseBody> response = retrofit.create(RetrofitInterface.class).sendForgotRequest(request);
        response.subscribeOn(Schedulers.io())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody loginResponse) {
                        //got the data
                        loginView.hideLoader();
                        if (loginResponse != null) {
                            //show home screen later
                            Handler uiHandler = new Handler(Looper.getMainLooper());
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (request.getToken() != null) {
                                        loginView.showMessage("Your password has been changed.");
                                    } else {
                                        loginView.showMessage("A verification pin has been sent to your email. Please provide it on the next screen.");
                                    }
                                }
                            });
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        showError(Functions.structure(throwable).getMsg());
                    }
                });
    }

    public void showError(final String message) {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                loginView.showError(message);
            }
        });
    }

}
