package com.trofiventures.systemsocial.presenter;

import android.app.Activity;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.systemsocial.systemsocial.InstagramBrandGroupActivity;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.interactor.UserInteractor;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.accounts.InstagramBrandGroupView;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 1/29/2018.
 */

public class InstagramBrandGroupPresenter implements Presenter<InstagramBrandGroupView> {

    private InstagramBrandGroupView instagramBrandGroupView;

    @Inject
    InstagramInteractor instagramInteractor;
    @Inject
    Retrofit retrofit;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Gson gson;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
    }

    @Override
    public void onStart() {
        User user = User.getSharedUser();
        if (user != null)
            instagramBrandGroupView.brandGroups(user.getBrands());
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(InstagramBrandGroupView view) {
        instagramBrandGroupView = view;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void logOutByBrand(Activity context, Brand brand) {
        instagramInteractor.logOutByBrand(context, brand);
        User user = User.getSharedUser();
        int index = user.getBrands().indexOf(brand);
        Brand brand1 = user.getBrands().get(index);
        brand1.setInstagramUsername(null);
        user.getBrands().remove(index);
        user.getBrands().add(index, brand1);
        User.setUser(user);
        UserInteractor.save(sharedPreferences, gson, user);

        retrofit.create(RetrofitInterface.class).resetSystemSocialAccount(user.getToken(),
                String.valueOf(NetworkType.INSTAGRAM.getId()),
                brand.getId()).asObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe();
        onStart();
    }

    public void login(final InstagramBrandGroupActivity activity, Brand brand) {

        if (instagramInteractor.isLogged(activity, brand.getId()))
            instagramBrandGroupView.resetInstagramAccount(brand);
        else
            instagramInteractor.login(activity, brand, new Runnable() {
                @Override
                public void run() {
                    retrofit.create(RetrofitInterface.class).checkSession(
                            User.getSharedUser().getToken(),
                            BuildConfig.OS,
                            BuildConfig.VERSION_NAME,
                            BuildConfig.INVALIDATES,
                            UIUtils.getPlayerID()).asObservable()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .onErrorReturn(new Func1<Throwable, StatusRequest>() {
                                @Override
                                public StatusRequest call(Throwable throwable) {
                                    return Functions.structure(throwable);
                                }
                            })
                            .subscribe(new Action1<StatusRequest>() {
                                @Override
                                public void call(StatusRequest statusRequest) {

                                    User user = User.getSharedUser();
                                    user.setBrands(statusRequest.getBrands());
                                    User.setUser(user);
                                    UserInteractor.save(sharedPreferences, gson, user);
                                    onStart();

                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            });
                }
            }, null);
    }


}
