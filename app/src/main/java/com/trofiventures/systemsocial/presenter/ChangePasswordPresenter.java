package com.trofiventures.systemsocial.presenter;

import android.os.Handler;
import android.os.Looper;

import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.helper.Util;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.requests.ChangePassword;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.changePassword.ChangePasswordView;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 5/1/2017.
 */

public class ChangePasswordPresenter implements Presenter<ChangePasswordView> {

    private ChangePasswordView changePasswordView;

    @Inject
    Retrofit retrofit;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(ChangePasswordView view) {
        this.changePasswordView = view;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void changePassword(ChangePassword changePassword) {
        if (changePassword.getOldPassword() == null || changePassword.getOldPassword().isEmpty()) {
            changePasswordView.showPasswordEmpty();
            return;
        }

        if (changePassword.getPassword() == null || changePassword.getPassword().isEmpty()) {
            changePasswordView.showPasswordEmpty();
            return;
        }

        if (changePassword.getConfirmPassword() == null || changePassword.getConfirmPassword().isEmpty()) {
            changePasswordView.showPasswordEmpty();
            return;
        }

        if (!changePassword.getPassword().equals(changePassword.getConfirmPassword())) {
            changePasswordView.showErrorPassword();
            return;
        }

        if (!Util.isPasswordValidate(changePassword.getConfirmPassword())) {
            changePasswordView.showPasswordErrorPolitics();
            return;
        }

        changePasswordView.hideLoader();

        retrofit.create(RetrofitInterface.class).updatePassword(changePassword,
                String.valueOf(User.getSharedUser().getId()))
                .asObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        Handler uiHandler = new Handler(Looper.getMainLooper());
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                changePasswordView.showLoader();
                            }
                        });
                    }
                })
                .doAfterTerminate(new Action0() {
                    @Override
                    public void call() {
                        Handler uiHandler = new Handler(Looper.getMainLooper());
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                changePasswordView.hideLoader();
                            }
                        });
                    }
                })
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        changePasswordView.showPasswordChanged();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        changePasswordView.showError(Functions.structure(throwable).getMsg());
                    }
                });
    }
}
