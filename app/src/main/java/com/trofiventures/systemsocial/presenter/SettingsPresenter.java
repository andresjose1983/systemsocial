package com.trofiventures.systemsocial.presenter;

import android.os.Handler;
import android.os.Looper;

import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.interactor.FacebookInteractor;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.interactor.TwitterInteractor;
import com.trofiventures.systemsocial.ui.settings.SettingsView;

import javax.inject.Inject;

import retrofit2.Retrofit;

/**
 * Created by andre on 5/1/2017.
 */

public class SettingsPresenter implements Presenter<SettingsView> {

    private SettingsView settingsView;
    @Inject
    Retrofit retrofit;
    @Inject
    TwitterInteractor twitterInteractor;
    @Inject
    FacebookInteractor facebookInteractor;
    @Inject
    InstagramInteractor instagramInteractor;

    public SettingsPresenter() {
    }

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    public void attachView(SettingsView settingsView) {
        this.settingsView = settingsView;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void loadTermsAndConditions() {
        settingsView.showTermsAndConditions(null);
        /*settingsView.showLoader();
        retrofit2.Call<ResponseBody> response = retrofit.create(RetrofitInterface.class)
                .terms("text/html,application/json", "employee");
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    settingsView.hideLoader();
                    settingsView.showTermsAndConditions(response.body().string());
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                showError(t.getMessage());
            }
        });*/
    }

    public void showError(final String message) {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                settingsView.hideLoader();
                settingsView.showError(message);
            }
        });
    }
}
