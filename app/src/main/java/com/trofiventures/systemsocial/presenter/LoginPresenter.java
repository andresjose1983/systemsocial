package com.trofiventures.systemsocial.presenter;

import android.Manifest;
import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;

import com.google.gson.Gson;
import com.trofiventures.systemsocial.BuildConfig;
import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.interactor.FacebookInteractor;
import com.trofiventures.systemsocial.interactor.InstagramInteractor;
import com.trofiventures.systemsocial.interactor.TwitterInteractor;
import com.trofiventures.systemsocial.interactor.UserInteractor;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.StatusRequest;
import com.trofiventures.systemsocial.model.network.responses.FacebookAlbum;
import com.trofiventures.systemsocial.model.network.responses.LoginResponse;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.login.LoginView;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by luis_ on 1/15/2017.
 */
public class LoginPresenter implements Presenter<LoginView> {

    private Subscription loginSubscription;
    private LoginView loginView;
    public static final String USER = "user_email";

    @Inject
    Retrofit retrofit;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Gson gson;
    @Inject
    TwitterInteractor twitterInteractor;
    @Inject
    FacebookInteractor facebookInteractor;
    @Inject
    InstagramInteractor instagramInteractor;
    @Inject
    Application application;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
        //Verify if user is logged in
        User user = UserInteractor.getUser(sharedPreferences, gson);
        if (user != null) {
            User.setUser(user);
            showSuccess();
        } else {
            User.setUser(null);
        }
    }

    @Override
    public void onStart() {
        //if (Build.VERSION_CODES.LOLLIPOP_MR1 >= BuildConfig.VERSION_CODE)
        //First checking if the app is already having the permission
        //  if (!isStorageAllowed())
        loginView.requestCallPermission();
    }

    //We are calling this method to check the permission status
    private boolean isStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(loginView.getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    @Override
    public void onStop() {
        if (loginSubscription != null && !loginSubscription.isUnsubscribed()) {
            loginSubscription.unsubscribe();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(LoginView view) {
        this.loginView = view;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void makeLogin(final User user) {
        final String email = user.getEmail();
        /*final String userEmailLastLogged = getUserEmail();
        if (userEmailLastLogged != null && userEmailLastLogged.length() > 0) {
            if (!userEmailLastLogged.equalsIgnoreCase(email)) {
                loginView.showLoader();
                login(user)
                        .subscribe(new Action1<LoginResponse>() {
                            @Override
                            public void call(final LoginResponse loginResponse) {
                                loginView.hideLoader();
                                facebookInteractor.logout();
                                twitterInteractor.logout(application);
                                instagramInteractor.logout(loginView.getActivity(), new Runnable() {
                                    @Override
                                    public void run() {
                                        ResetUserAccount resetUserAccount = new ResetUserAccount();
                                        resetUserAccount.setEmail(userEmailLastLogged);
                                        retrofit.create(RetrofitInterface.class)
                                                .resetSystemSocialAccount(loginResponse.getToken(),
                                                        resetUserAccount).asObservable()
                                                .onErrorReturn(new Func1<Throwable, ResponseBody>() {
                                                    @Override
                                                    public ResponseBody call(Throwable throwable) {
                                                        return null;
                                                    }
                                                })
                                                .subscribeOn(Schedulers.io())
                                                .subscribe(new Action1<ResponseBody>() {
                                                    @Override
                                                    public void call(ResponseBody responseBody) {

                                                        loginSuccess(loginResponse, email);
                                                    }
                                                }, new Action1<Throwable>() {
                                                    @Override
                                                    public void call(Throwable throwable) {

                                                    }
                                                });
                                    }
                                });
                            }
                        });
            } else
                executeLogin(user, email);
        } else*/
        executeLogin(user, email);
    }

    private void executeLogin(final User user, final String email) {
        login(user)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        loginView.showLoader();
                    }
                })
                .subscribe(new Action1<LoginResponse>() {
                    @Override
                    public void call(LoginResponse loginResponse) {
                        loginSuccess(loginResponse, email);
                    }
                });
    }

    private void loginSuccess(LoginResponse loginResponse, String email) {
        //got the data
        loginView.hideLoader();
        if (loginResponse != null) {
            //show home screen later
            User user = new User();
            user.setId(loginResponse.getId());
            user.setToken(loginResponse.getToken());
            user.setName(loginResponse.getName());
            user.setHasLinks(loginResponse.getHasLinks());
            user.setShouldAcceptTerms(loginResponse.getShouldAcceptTerms());
            user.setBrandColor(loginResponse.getBrandColor());
            user.setBrandLogo(loginResponse.getBrandLogo());
            user.setUserType(loginResponse.getUserType());
            user.setBrands(loginResponse.getBrands());
            user.setMultibrandAdvocate(loginResponse.isMultibrandAdvocate());
            User.setUser(user);
            UserInteractor.save(sharedPreferences, gson, user);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString(USER, email).apply();
            edit.commit();
            showSuccess();
        }
    }

    public void validateStatusRequest(final StatusRequest structure) {
        showError(structure.getMsg());
        if (structure.isInvalidates()) {
            facebookInteractor.logout();
            twitterInteractor.logout(application);
            instagramInteractor.logout(loginView.getActivity(), new Runnable() {
                @Override
                public void run() {
                    if (structure.getCode().equals(BuildConfig.UPDATE_VERSION_CODE))
                        loginView.showUpdateVersion(structure.isInvalidates());
                }
            });
        } else {
            if (structure.getCode() != null && structure.getCode().equals(BuildConfig.UPDATE_VERSION_CODE))
                loginView.showUpdateVersion(structure.isInvalidates());

        }
    }

    private Observable<LoginResponse> login(final User user) {
        return retrofit.create(RetrofitInterface.class)
                .login(UIUtils.getPlayerID(), BuildConfig.OS, BuildConfig.VERSION_NAME,
                        BuildConfig.INVALIDATES, user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .onErrorReturn(new Func1<Throwable, LoginResponse>() {
                    @Override
                    public LoginResponse call(Throwable throwable) {
                        loginView.hideLoader();
                        final StatusRequest structure = Functions.structure(throwable);
                        if (structure != null)
                            validateStatusRequest(structure);
                        return null;
                    }
                })
                .filter(new Func1<LoginResponse, Boolean>() {
                    @Override
                    public Boolean call(LoginResponse loginResponse) {
                        return loginResponse != null;
                    }
                })
                .flatMap(new Func1<LoginResponse, Observable<LoginResponse>>() {
                    @Override
                    public Observable<LoginResponse> call(LoginResponse loginResponse) {
                        retrofit.create(RetrofitInterface.class)
                                .getFacebookAlbumId(loginResponse.getToken())
                                .asObservable()
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Action1<FacebookAlbum>() {
                                    @Override
                                    public void call(FacebookAlbum facebookAlbum) {
                                        SharedPreferences.Editor edit = sharedPreferences.edit();
                                        edit.putString(FacebookInteractor.ALBUM_ID, facebookAlbum.getAlbumId()).apply();
                                        edit.commit();
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {

                                    }
                                });

                        return Observable.just(loginResponse);
                    }
                });
    }

    private void showSuccess() {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                loginView.loginSuccessfull();
            }
        });
    }

    /*public void makeLoginWithCallback(User user, final RegisterView view, final Runnable runnable) {
        BaseApplication.getNetComponent().inject(this);
        Observable<LoginResponse> response = retrofit.create(RetrofitInterface.class)
                .login(UIUtils.getPlayerID(), BuildConfig.OS, BuildConfig.VERSION_NAME,
                        BuildConfig.INVALIDATES, user);
        response.subscribeOn(Schedulers.io())
                .onErrorReturn(new Func1<Throwable, LoginResponse>() {
                    @Override
                    public LoginResponse call(Throwable throwable) {
                        final StatusRequest structure = Functions.structure(throwable);
                        if (structure != null) {
                            if (structure.isInvalidates()) {
                                facebookInteractor.logout();
                                twitterInteractor.logout(application);
                                instagramInteractor.logout(view.getActivity(), new Runnable() {
                                    @Override
                                    public void run() {
                                        if (structure.getCode().equals(BuildConfig.UPDATE_VERSION_CODE))
                                            view.showUpdateVersion(structure.isInvalidates());
                                    }
                                });
                            } else {
                                if (structure.getCode().equals(BuildConfig.UPDATE_VERSION_CODE))
                                    view.showUpdateVersion(structure.isInvalidates());

                            }
                        }

                        return null;
                    }
                }).subscribe(new Action1<LoginResponse>() {
            @Override
            public void call(LoginResponse loginResponse) {
                if (loginResponse != null) {
                    User user = new User();
                    user.setToken(loginResponse.getToken());
                    user.setHasLinks(loginResponse.getHasLinks());
                    user.setName(loginResponse.getName());
                    User.setUser(user);
                    UserInteractor.save(sharedPreferences, gson, user);
                    runnable.run();
                }
            }
        });
    }*/

    public void showError(final String message) {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                loginView.showLoginError(message);
            }
        });
    }

    public void forgotPassword() {
        loginView.showForgot();
    }

    public void showRegisterView() {
        loginView.showRegister();
    }

    public void reset(User user) {
        loginView.showLoader();

        loginSubscription = retrofit.create(RetrofitInterface.class)
                .login(UIUtils.getPlayerID(), BuildConfig.OS, BuildConfig.VERSION_NAME,
                        BuildConfig.INVALIDATES, user)
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<LoginResponse, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(LoginResponse loginResponse) {
                        return retrofit.create(RetrofitInterface.class).reset(loginResponse.getToken()).asObservable();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        RealmController.getInstance().clearAll();
                        sharedPreferences.edit().clear().commit();
                        showReset();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        loginView.hideLoader();
                        showError(Functions.structure(throwable).getMsg());
                    }
                });
    }

    private void showReset() {
        loginView.hideLoader();
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                loginView.showReset();
            }
        });
    }

    public String getUserEmail() {
        return sharedPreferences.getString(USER, null);
    }
}