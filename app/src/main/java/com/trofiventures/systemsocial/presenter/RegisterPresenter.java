package com.trofiventures.systemsocial.presenter;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.helper.Util;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.requests.RegisterRequest;
import com.trofiventures.systemsocial.model.network.responses.register.RegisterResponse;
import com.trofiventures.systemsocial.ui.register.RegisterView;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by luis_ on 1/21/2017.
 */

public class RegisterPresenter implements Presenter<RegisterView> {
    RegisterView view;
    @Inject
    Retrofit retrofit;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(RegisterView view) {
        this.view = view;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void registerUser(final RegisterRequest request) {
        if (!request.isTermsAgreed()) {
            showTermsError();
            return;
        }

        if (request.getEmail() == null ||
                request.getEmail().length() == 0 ||
                !isValidEmail(request.getEmail())) {
            view.showEmailError();
            return;
        }

        if (request.getConfirm() == null ||
                request.getPassword() == null ||
                (!request.getConfirm().equals(request.getPassword())) ||
                request.getConfirm().length() == 0 ||
                request.getPassword().length() == 0) {
            view.showErrorPassword();
            return;
        }

        if (!Util.isPasswordValidate(request.getConfirm())) {
            view.showPasswordErrorPolitics();
            return;
        }

        view.showLoader();
        Observable<RegisterResponse> response = retrofit.create(RetrofitInterface.class)
                .register(request);
        response.subscribeOn(Schedulers.io())
                .onErrorReturn(new Func1<Throwable, RegisterResponse>() {
                    @Override
                    public RegisterResponse call(Throwable throwable) {
                        showError(Functions.structure(throwable).getMsg());
                        return null;
                    }
                }).subscribe(new Action1<RegisterResponse>() {
            @Override
            public void call(RegisterResponse response) {
                //got the data
                view.hideLoader();
                if (response != null) {
                    System.out.println(response);

                    Handler uiHandler = new Handler(Looper.getMainLooper());
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            view.showRegisterSuccess(request);
                        }
                    });
                }
            }
        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        return target == null || !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void showError(final String message) {
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                view.hideLoader();
                view.showError(message);
            }
        });
    }

    public void showTerms() {
        retrofit2.Call<ResponseBody> response = retrofit.create(RetrofitInterface.class)
                .terms("text/html,application/json", "employee");
        response.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    view.showTermsAndConditions(response.body().string());
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                showError(t.getMessage());
            }
        });
    }

    public void showPrivacyPolicy() {
        view.showPrivacyPolicy();
    }

    public void showTermsError() {
        view.showErrorTerms();
    }
}
