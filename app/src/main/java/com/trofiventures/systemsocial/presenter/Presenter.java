package com.trofiventures.systemsocial.presenter;


import com.trofiventures.systemsocial.common.View;

/**
 * Created by luis_ on 1/15/2017.
 */
public interface Presenter<T extends View> {
    void onCreate();

    void onStart();

    void onStop();

    void onPause();

    void attachView(T view);

    void logOut();

    void logOutFacebook();

    void logOutInstagram();

    void logOutTwitter();
}

