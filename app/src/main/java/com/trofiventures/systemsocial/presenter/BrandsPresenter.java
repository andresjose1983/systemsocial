package com.trofiventures.systemsocial.presenter;

import com.trofiventures.systemsocial.common.BaseApplication;
import com.trofiventures.systemsocial.model.network.RetrofitInterface;
import com.trofiventures.systemsocial.model.network.responses.BrandResponse;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.brand.BrandView;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by andre on 5/1/2017.
 */

public class BrandsPresenter implements Presenter<BrandView> {

    @Inject
    Retrofit retrofit;
    private BrandView brandView;

    @Override
    public void onCreate() {
        BaseApplication.getNetComponent().inject(this);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void attachView(BrandView view) {
        this.brandView = view;
    }

    @Override
    public void logOut() {

    }

    @Override
    public void logOutFacebook() {

    }

    @Override
    public void logOutInstagram() {

    }

    @Override
    public void logOutTwitter() {

    }

    public void getBrands() {
        retrofit.create(RetrofitInterface.class).getBrands(User.getSharedUser().getToken())
                .asObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        brandView.showLoader();
                    }
                })
                .doAfterTerminate(new Action0() {
                    @Override
                    public void call() {
                        brandView.hideLoader();
                    }
                })
                .subscribe(new Action1<BrandResponse>() {
                    @Override
                    public void call(BrandResponse brands) {
                        if (brands != null && brands.getBrands().size() > 0)
                            brandView.showBrands(brands.getBrands());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }
}
