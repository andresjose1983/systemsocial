package com.trofiventures.systemsocial.helper;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.Action;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.ui.posts.PostInteraction;

import java.util.List;

/**
 * Created by andre on 4/7/2017.
 */

public final class TwitterHelper extends SystemSocialHelper {

    @Override
    public void onAction(final SocialPost current, final PostInteraction listener,
                         final View v, final View twitterActionButtons) {
        twitterActionButtons.setVisibility(View.VISIBLE);
        final TextView likePoints = (TextView) v.findViewById(R.id.likePoints);
        final TextView retweetPoints = (TextView) v.findViewById(R.id.retweetPoints);
        final View retweet = v.findViewById(R.id.retweet);
        final View like = v.findViewById(R.id.twitterLike);

        validateMultiBrandAdvocate(current, v);

        List<Action> actions = current.getTypeData().getActionsRealm();
        if (actions != null) {
            if (actions.size() == 0) {
                retweet.setVisibility(View.GONE);
                like.setVisibility(View.GONE);
            } else {
                for (Action action : actions) {
                    if (action.getAction() != com.trofiventures.systemsocial.common.Action.COMMENT.getId())
                        verifyAction(action, likePoints, retweetPoints, true);

                    if (action.getAction() == com.trofiventures.systemsocial.common.Action.LIKE.getId())
                        like.setVisibility(View.VISIBLE);
                    else if (action.getAction() == com.trofiventures.systemsocial.common.Action.SHARE.getId())
                        retweet.setVisibility(View.VISIBLE);

                }
            }
        }

        final ImageView imageViewRetweet = (ImageView) v.findViewById(R.id.imageViewRetweet);
        retweet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.reTweet(current);
                changeColorDone(retweet.getContext(), retweet, retweetPoints, imageViewRetweet);
            }
        });
        final ImageView imageViewLikeTweet = (ImageView) v.findViewById(R.id.imageViewLikeTweet);

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.likeTweet(current);
                changeColorDone(like.getContext(), like, likePoints, imageViewLikeTweet);
            }
        });
        // refresh action view
        String markTweet = listener.getMarkTweet(current.getId());
        if (markTweet != null) {
            String[] split = markTweet.split(",");
            for (String action : split) {
                if (Integer.valueOf(action) == com.trofiventures.systemsocial.common.Action.LIKE.getId()) {
                    changeColorDone(like.getContext(), like, likePoints, imageViewLikeTweet);
                } else {
                    changeColorDone(retweet.getContext(), retweet, retweetPoints, imageViewRetweet);
                }
            }
        }

    }

    @Override
    public void onPost(final View v, final SocialPost current, final PostInteraction listener) {
        //Post tweet
        final View twitterButton = v.findViewById(R.id.twitter);
        final TextView twitterText = (TextView) v.findViewById(R.id.twitter_text);
        final ImageView twitterImage = (ImageView) v.findViewById(R.id.twitter_image);

        validateMultiBrandAdvocate(current, v);

        //v.findViewById(R.id.post_text).setVisibility(View.VISIBLE);
        TextView postUrl = (TextView) v.findViewById(R.id.post_url);
        if (current.getPostUrl() != null && current.getPostUrl().length() > 0) {
            postUrl.setVisibility(View.VISIBLE);
            postUrl.setText(current.getPostUrl());
            postUrl.setMovementMethod(LinkMovementMethod.getInstance());
        } else
            postUrl.setVisibility(View.GONE);

        ((TextView) v.findViewById(R.id.tap_editable)).setText(R.string.tap_to_edit);
        ((TextView) v.findViewById(R.id.post_text)).setText(current.getPostText());

        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeColorDone(v.getContext(), twitterButton, twitterText, twitterImage);
                RealmController.getInstance().setSocialPostTwitterSelected(current.getId(), true);
                listener.sendPostTwitter(current);
            }
        });
        twitterButton.setClickable(true);

        // refresh action view
        String markTweet = listener.getMarkTweet(current.getId());
        if (markTweet != null) {
            String[] split = markTweet.split(",");
            for (String action : split) {
                if (Integer.valueOf(action) == com.trofiventures.systemsocial.common.Action.POST.getId())
                    changeColorDone(v.getContext(), twitterButton, twitterText, twitterImage);

            }
        }
    }
}
