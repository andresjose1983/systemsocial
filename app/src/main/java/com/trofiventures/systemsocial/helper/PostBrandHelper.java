package com.trofiventures.systemsocial.helper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.Functions;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.common.PostType;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.PostId;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;
import com.trofiventures.systemsocial.ui.posts.PostInteraction;

import java.util.List;

/**
 * Created by andre on 4/24/2017.
 */

public class PostBrandHelper {

    public void showItem(final SocialPost current, final HomeNavActivity context,
                         final PostInteraction listener, final View v) {
        final ImageView imageContainer = (ImageView) v.findViewById(R.id.post_image);

        View videoPost = v.findViewById(R.id.video_post);
        String image;
        if (current.getYoutubeId() != null && current.getYoutubeId().length() > 0) {
            listener.downloadVideo(current);
            videoPost.setVisibility(View.VISIBLE);
            image = current.getThumbnail();
        } else {
            image = current.getImgUrl();
            videoPost.setVisibility(View.GONE);
        }
        videoPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.watchYoutubeVideo(view.getContext(), current.getYoutubeId());
            }
        });

        if ((current.getYoutubeId() == null || current.getYoutubeId().isEmpty()) &&
                (current.getImgUrl() == null || current.getImgUrl().isEmpty()))
            v.findViewById(R.id.gallery_view).setVisibility(View.GONE);
        else
            v.findViewById(R.id.gallery_view).setVisibility(View.VISIBLE);

        if (image != null && !image.isEmpty()) {
            final String imageUrl = image;// = image.contains("http://") ? image : "http://" + image;

            Picasso.with(context)
                    .load(image)
                    .noFade()
                    .tag(context)
                    .into(imageContainer, new Callback() {
                        @Override
                        public void onSuccess() {
                            Util.saveImage(context, current, imageUrl);
                        }

                        @Override
                        public void onError() {

                        }
                    });

            imageContainer.setBackgroundColor(Color.TRANSPARENT);
            //Expandable image view
            //final ImageView expandedImage = (ImageView) context.findViewById(R.id.expanded_image);
            //final View viewItem = v;

            /*imageContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Functions.zoomImageFromThumb(viewItem, imageContainer, expandedImage,
                            imageUrl, 100);
                }
            });*/
        }

        //TextView title = (TextView) v.findViewById(R.id.post_title);
        //title.setText(Html.fromHtml(current.getTitle()));

        TextView postUrl = (TextView) v.findViewById(R.id.post_url);
        if (current.getPostUrl() != null && current.getPostUrl().length() > 0) {
            postUrl.setVisibility(View.VISIBLE);
            postUrl.setText(current.getPostUrl());
            postUrl.setMovementMethod(LinkMovementMethod.getInstance());
        } else
            postUrl.setVisibility(View.GONE);

        View tapEditable = v.findViewById(R.id.tap_editable);
        final EditText postTextEdit = (EditText) v.findViewById(R.id.post_text);
        postTextEdit.setText(current.getPostText());

        tapEditable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.setClipboard(postTextEdit.getContext(), Util.textToPost(current));

                postTextEdit.setEnabled(true);//enables the edit text which we disabled in layout file
                postTextEdit.setCursorVisible(true);
                postTextEdit.setFocusableInTouchMode(true);
                postTextEdit.setFocusable(true);
                postTextEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);
                postTextEdit.setRawInputType(InputType.TYPE_CLASS_TEXT);
                postTextEdit.setHorizontallyScrolling(true);
                postTextEdit.setLines(1);
                postTextEdit.requestFocus();
                postTextEdit.getBackground().setColorFilter(ContextCompat.getColor(view.getContext(), R.color.primary), PorterDuff.Mode.SRC_IN);
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(postTextEdit, InputMethodManager.SHOW_IMPLICIT);//shows   keyboard
            }
        });
        postTextEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    RealmController.getInstance().updatePost(current.getId(), postTextEdit.getText().toString());
                    postTextEdit.setEnabled(false);
                    postTextEdit.setCursorVisible(false);
                    postTextEdit.setFocusableInTouchMode(false);
                    postTextEdit.setFocusable(false);
                    postTextEdit.setHorizontallyScrolling(false);
                    postTextEdit.setBackground(ContextCompat.getDrawable(textView.getContext(), android.R.color.transparent));
                    return true;
                }
                return false;
            }
        });

        final View twitterButton = v.findViewById(R.id.twitter);
        final View facebookButton = v.findViewById(R.id.facebook);
        final View instagramButton = v.findViewById(R.id.instagram);

        View socialButtonsContainer = v.findViewById(R.id.social_buttons_container);
        View twitterActionButtons = v.findViewById(R.id.twitter_action_buttons);
        View facebookActionButtons = v.findViewById(R.id.facebook_action_buttons);
        View instagramActionButtons = v.findViewById(R.id.instagram_action_buttons);

        TwitterHelper twitterHelper = new TwitterHelper();
        FacebookHelper facebookHelper = new FacebookHelper();
        InstagramHelper instagramHelper = new InstagramHelper();

        if (current.getType() == PostType.ADVOCATE.getId()) {
            socialButtonsContainer.setVisibility(View.VISIBLE);
            twitterActionButtons.setVisibility(View.GONE);
            facebookActionButtons.setVisibility(View.GONE);

            showOrHideButton(twitterButton, canShow(current.getTypeData().getNetworks(),
                    NetworkType.TWITTER.getId()));

            showOrHideButton(facebookButton, canShow(current.getTypeData().getNetworks(),
                    NetworkType.FACEBOOK.getId()));

            showOrHideButton(instagramButton, canShow(current.getTypeData().getNetworks(),
                    NetworkType.INSTAGRAM.getId()));

            int changeTapText = 0;

            //Twitter post
            if (current.getTypeData().getNetworks().where().equalTo("id", NetworkType.TWITTER.getId()).findFirst() != null) {
                ++changeTapText;
                twitterHelper.onPost(v, current, listener);
            }
            //Facebook post
            if (current.getTypeData().getNetworks().where().equalTo("id", NetworkType.FACEBOOK.getId()).findFirst() != null)
                facebookHelper.onPost(v, current, listener);
            //Instagram post
            if (current.getTypeData().getNetworks().where().equalTo("id", NetworkType.INSTAGRAM.getId()).findFirst() != null) {
                ++changeTapText;
                instagramHelper.onPost(v, current, listener);
            }

            tapEditable.setVisibility(View.VISIBLE);
            if (changeTapText == 2)
                ((TextView) tapEditable).setText(R.string.tap_to_edit_twitter);

        } else if (current.getType() == PostType.BRAND.getId()) {
            socialButtonsContainer.setVisibility(View.GONE);
            //if twitter
            if (canShow(current.getTypeData().getNetworks(),
                    NetworkType.TWITTER.getId()))
                //Action
                twitterHelper.onAction(current, listener, v, twitterActionButtons);
            if (canShow(current.getTypeData().getNetworks(),
                    NetworkType.FACEBOOK.getId()))
                //Action
                facebookHelper.onAction(current, listener, v, facebookActionButtons);

            if (canShow(current.getTypeData().getNetworks(),
                    NetworkType.INSTAGRAM.getId()))
                //Action
                instagramHelper.onAction(current, listener, v, instagramActionButtons);

            tapEditable.setVisibility(View.GONE);
        }

        Button passButton = (Button) v.findViewById(R.id.pass_button);
        Button doneButton = (Button) v.findViewById(R.id.post_button);

        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.pass(current);
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.done(current);
            }
        });
    }

    private void showOrHideButton(View button, boolean canShow) {
        if (canShow) {
            button.setVisibility(View.VISIBLE);
        } else
            button.setVisibility(View.GONE);
    }

    private boolean canShow(List<PostId> postIds, long id) {
        for (PostId postId : postIds) {
            if (postId.getId() == id)
                return true;
        }
        return false;
    }

}
