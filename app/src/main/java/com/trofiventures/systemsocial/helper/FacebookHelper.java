package com.trofiventures.systemsocial.helper;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.Action;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.ui.posts.PostInteraction;

import java.util.List;

/**
 * Created by andre on 4/13/2017.
 */

public class FacebookHelper extends SystemSocialHelper {

    @Override
    public void onAction(final SocialPost current, final PostInteraction listener,
                         final View v, final View facebookActionButtons) {
        final TextView likePoints = (TextView) v.findViewById(R.id.like_points);
        final TextView pointShareFacebook = (TextView) v.findViewById(R.id.point_share_facebook);
        final TextView pointCommentFacebook = (TextView) v.findViewById(R.id.point_comment_facebook);

        validateMultiBrandAdvocate(current, v);

        final View shareFacebook = v.findViewById(R.id.share_facebook);
        final View facebookLike = v.findViewById(R.id.facebook_like);
        final View facebookComment = v.findViewById(R.id.comment_facebook);
        //String shareLabel = pointShareFacebook.getContext().getString(R.string.share);
        //facebookActionButtons.setClickable(true);
        facebookActionButtons.setVisibility(View.VISIBLE);
        facebookActionButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openFacebookBrowser(current);
            }
        });

        List<Action> actions = current.getTypeData().getActionsRealm();
        if (actions != null) {
            if (actions.size() == 0) {
                shareFacebook.setVisibility(View.GONE);
                facebookLike.setVisibility(View.GONE);
            } else {
                for (Action action : actions) {
                    if (action.getAction() == com.trofiventures.systemsocial.common.Action.LIKE.getId())
                        super.showInfo(likePoints, action.getPoints() == 0 ? String.format(likePoints.getContext()
                                .getString(R.string.points_win), action.getPoints()) : "", action.getPoints());
                    if (action.getAction() == com.trofiventures.systemsocial.common.Action.SHARE.getId())
                        super.showInfo(pointShareFacebook, "", 0);
                    if (action.getAction() == com.trofiventures.systemsocial.common.Action.COMMENT.getId())
                        super.showInfo(pointCommentFacebook, action.getPoints() == 0 ? String.format(pointCommentFacebook.getContext()
                                .getString(R.string.points_win), action.getPoints()) : "", action.getPoints());

                    facebookLike.setVisibility(View.VISIBLE);
                    shareFacebook.setVisibility(View.VISIBLE);
                    facebookComment.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onPost(final View v, final SocialPost current, final PostInteraction listener) {
        //Post tweet
        final View facebookButton = v.findViewById(R.id.facebook);
        final TextView facebookText = (TextView) v.findViewById(R.id.facebook_text);
        final ImageView facebookImage = (ImageView) v.findViewById(R.id.facebook_image);

        validateMultiBrandAdvocate(current, v);

        ((TextView) v.findViewById(R.id.post_text)).setText(current.getPostText());

        //v.findViewById(R.id.post_text).setVisibility(View.GONE);
        //v.findViewById(R.id.post_url).setVisibility(View.INVISIBLE);
        ((TextView) v.findViewById(R.id.tap_editable)).setText(R.string.add_your_comment);
        //add_your_comment

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeColorDone(v.getContext(), facebookButton, facebookText, facebookImage);
                RealmController.getInstance().setSocialPostFacebookSelected(current.getId(), true);
                listener.sendPostFacebook(current);
            }
        });
        facebookButton.setClickable(true);

        // refresh action view
        String markTweet = listener.getMarkFacebook(current.getId());
        if (markTweet != null) {
            String[] split = markTweet.split(",");
            for (String action : split) {
                if (Integer.valueOf(action) == com.trofiventures.systemsocial.common.Action.POST.getId())
                    changeColorDone(v.getContext(), facebookButton, facebookText, facebookImage);

            }
        }
    }
}
