package com.trofiventures.systemsocial.helper;

import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.ui.posts.PostInteraction;

/**
 * Created by andre on 4/25/2017.
 */

public class RelinkHelper {

    public void action(final View view, final SocialPost post, final PostInteraction listener) {
        TextView titleMessageText = (TextView) view.findViewById(R.id.title_message_text);
        TextView contentMessageText = (TextView) view.findViewById(R.id.content_message_text);
        int network = post.getTypeData().getNetworks().get(0).getId();
        if (network == NetworkType.FACEBOOK.getId()) {
            showIconSocial(view, R.id.facebook_image, R.id.twitter_image,
                    R.id.instagram_image);
            showButton(view, R.id.login_facebook_button, R.id.login_twitter_button,
                    R.id.button_instagram);

            titleMessageText.setText(R.string.relink_title_facebook);
            contentMessageText.setText(R.string.relink_content_facebook);

            view.findViewById(R.id.login_facebook_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.loginWithFacebook(post);
                }
            });
        } else if (network == NetworkType.TWITTER.getId()) {
            showIconSocial(view, R.id.twitter_image, R.id.facebook_image,
                    R.id.instagram_image);
            showButton(view, R.id.login_twitter_button, R.id.login_facebook_button,
                    R.id.button_instagram);

            titleMessageText.setText(R.string.relink_title_twitter);
            contentMessageText.setText(R.string.relink_content_twitter);

            Button twitterButton = (Button) view.findViewById(R.id.login_twitter_button);

            twitterButton.setCompoundDrawablesWithIntrinsicBounds(
                    view.getContext().getResources().getDrawable(com.twitter.sdk.android.core.R.drawable.tw__ic_logo_default), null, null, null);
            twitterButton.setCompoundDrawablePadding(
                    view.getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_drawable_padding));
            twitterButton.setText(com.twitter.sdk.android.core.R.string.tw__login_btn_txt);
            twitterButton.setTextColor(view.getContext().getResources().getColor(com.twitter.sdk.android.core.R.color.tw__solid_white));
            twitterButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    view.getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_text_size));
            twitterButton.setTypeface(Typeface.DEFAULT_BOLD);
            twitterButton.setPadding(view.getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_left_padding), 0,
                    view.getContext().getResources().getDimensionPixelSize(com.twitter.sdk.android.core.R.dimen.tw__login_btn_right_padding), 0);
            twitterButton.setBackgroundResource(com.twitter.sdk.android.core.R.drawable.tw__login_btn);
            twitterButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.loginWithTwitter(post);
                }
            });


        } else {
            showIconSocial(view, R.id.instagram_image,
                    R.id.twitter_image, R.id.facebook_image);
            showButton(view, R.id.button_instagram, R.id.login_twitter_button,
                    R.id.login_facebook_button);

            titleMessageText.setText(R.string.relink_title_instagram);
            contentMessageText.setText(R.string.relink_content_instagram);

            view.findViewById(R.id.button_instagram).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.loginWithInstagram(post);
                }
            });
        }
    }

    private void showButton(View view, int... buttons) {
        view.findViewById(buttons[0]).setVisibility(View.VISIBLE);
        view.findViewById(buttons[1]).setVisibility(View.GONE);
        view.findViewById(buttons[2]).setVisibility(View.GONE);
    }

    private void showIconSocial(final View view, int... social) {
        view.findViewById(social[0]).setVisibility(View.VISIBLE);
        view.findViewById(social[1]).setVisibility(View.GONE);
        view.findViewById(social[2]).setVisibility(View.GONE);
    }
}
