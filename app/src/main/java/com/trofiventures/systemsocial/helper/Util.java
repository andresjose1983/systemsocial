package com.trofiventures.systemsocial.helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.common.NetworkType;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.ui.main.HomeNavActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andre on 4/20/2017.
 */

public class Util {

    private static final String TAG = "SystemSocial";
    private static final int NOTIFICATION_ID = 101;
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public static void setClipboard(Context context, String text) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager)
                context.getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
    }

    /**
     * Save image to gallery
     *
     * @param context
     * @param current
     * @param imageUrl
     */
    public static void saveImage(final Context context, final SocialPost current, final String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                try {
                    File path = Environment.getExternalStorageDirectory();
                    File file = new File(path, current.getId() +
                            imageUrl.substring(imageUrl.lastIndexOf('.')));

                    file.createNewFile();
                    FileOutputStream ostream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                    ostream.close();
                    //Util.addImageToGallery(imageContainer.getContext(), file.getPath());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        };
        Picasso.with(context)
                .load(imageUrl)
                .into(target);
    }

    public static boolean isPasswordValidate(String password) {
        Pattern pLetters = Pattern.compile("[a-zA-Z]");
        Matcher mLetters = pLetters.matcher(password);
        Pattern pNumbers = Pattern.compile("[0-9]");
        Matcher mNumbers = pNumbers.matcher(password);
        return mLetters.find() && mNumbers.find() && password.length() >= 6 && password.length() <= 24;
    }

    public static void showNotification(final Context context, final String title,
                                        final String message) {

        NotificationManager notificationmanager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(context, HomeNavActivity.class);
        PendingIntent pendIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendIntent).build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationmanager.notify(TAG, NOTIFICATION_ID, notification);

        Intent intentNotification = new Intent();
        intentNotification.setAction(HomeNavActivity.HIDE_NOTIFICATIONS);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intentNotification);
    }

    public static void removeNotifications(final Context context) {
        NotificationManager notificationmanager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationmanager.cancel(TAG, NOTIFICATION_ID);
    }

    /**
     * Text to system social
     *
     * @param post
     * @return String
     */
    public static String textToPost(SocialPost post) {
        StringBuilder tweet = new StringBuilder();
        //Text
        if (post.getPostText() != null) {
            if (post.getPostText().length() == 0) {
                if (post.getTypeData().getNetworks().where().equalTo("id", NetworkType.FACEBOOK.getId()).findFirst() != null)
                    tweet.append(post.getFacebookText());
                else
                    tweet.append(" ");
            } else
                tweet.append(post.getPostText());
        } else {
            tweet.append(post.getFacebookText());
        }

        tweet.append(" ");

        if (post.getPostUrl() != null && post.getPostUrl().length() > 0) {
            tweet.append(post.getPostUrl());
        }
        return tweet.toString();
    }

    public static Date toDate(String dateString) {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = DATE_FORMAT.parse(dateString);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return date;
    }
}
