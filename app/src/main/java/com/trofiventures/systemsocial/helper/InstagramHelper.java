package com.trofiventures.systemsocial.helper;

import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.entities.RealmController;
import com.trofiventures.systemsocial.model.entities.post.Action;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.posts.PostInteraction;

import java.util.List;

/**
 * Created by andre on 4/13/2017.
 */

public class InstagramHelper extends SystemSocialHelper {

    @Override
    public void onAction(final SocialPost current, final PostInteraction listener,
                         final View v, final View instagramActionButtons) {

        instagramActionButtons.setVisibility(View.VISIBLE);
        final TextView likePoints = (TextView) v.findViewById(R.id.likeInstagramPoints);
        final TextView commentPoints = (TextView) v.findViewById(R.id.commentInstagramPoints);
        instagramActionButtons.findViewById(R.id.instagram).setClickable(false);

        validateMultiBrandAdvocate(current, v);

        final View like = v.findViewById(R.id.instagramLike);
        final View comment = v.findViewById(R.id.instagramComment);

        List<Action> actions = current.getTypeData().getActionsRealm();
        if (actions != null) {
            if (actions.size() == 0) {
                comment.setVisibility(View.GONE);
                like.setVisibility(View.GONE);
            } else {
                for (Action action : actions) {
                    verifyAction(action, likePoints, commentPoints, false);
                    if (action.getAction() == com.trofiventures.systemsocial.common.Action.LIKE.getId())
                        like.setVisibility(View.VISIBLE);
                    else if (action.getAction() == com.trofiventures.systemsocial.common.Action.COMMENT.getId())
                        comment.setVisibility(View.VISIBLE);
                }
            }
        }

        final ImageView imageViewLikeInstagram = (ImageView) v.findViewById(R.id.imageViewLikeInstagram);
        final ImageView imageViewCommentInstagram = (ImageView) v.findViewById(R.id.imageViewCommentInstagram);

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.likeInstagram(current);
                changeColorDone(like.getContext(), like, likePoints, imageViewLikeInstagram);
            }
        });
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.commentInstagram(current, new Runnable() {
                    @Override
                    public void run() {
                        changeColorDone(like.getContext(), comment, commentPoints, imageViewCommentInstagram);
                    }
                });
            }
        });
        // refresh action view
        String markInstagram = listener.getMarkInstagram(current.getId());
        if (markInstagram != null) {
            String[] split = markInstagram.split(",");
            for (String action : split) {
                if (Integer.valueOf(action) == com.trofiventures.systemsocial.common.Action.LIKE.getId())
                    changeColorDone(like.getContext(), like, likePoints, imageViewLikeInstagram);
                else
                    changeColorDone(comment.getContext(), comment, commentPoints, imageViewCommentInstagram);

            }
        }
    }

    @Override
    public void onPost(final View v, final SocialPost current, final PostInteraction listener) {
        //Post tweet
        final View instagramButton = v.findViewById(R.id.instagram);
        final TextView instagramText = (TextView) v.findViewById(R.id.instagram_text);
        final ImageView instagramImage = (ImageView) v.findViewById(R.id.instagram_image);

        TextView nameBrand = (TextView) v.findViewById(R.id.name_brands_text);
        User sharedUser = User.getSharedUser();
        if (sharedUser != null && sharedUser.getUserType().equals("MANAGER")) {
            nameBrand.setVisibility(View.VISIBLE);
            int index = sharedUser.getBrands().indexOf(new Brand(current.getBrandId()));
            if (index >= 0) {
                Brand brand = sharedUser.getBrands().get(index);
                if (brand != null) {
                    String brandName;
                    brandName = v.getContext().getString(R.string.brand).concat("<br/>");
                    brandName = brandName + "<b>".concat(brand.getName()).concat("</b");
                    nameBrand.setText(Html.fromHtml(brandName));
                } else nameBrand.setVisibility(View.INVISIBLE);
            } else nameBrand.setVisibility(View.INVISIBLE);
        } else
            validateMultiBrandAdvocate(current, v);
        //nameBrand.setVisibility(View.INVISIBLE);

        //v.findViewById(R.id.post_text).setVisibility(View.VISIBLE);
        //v.findViewById(R.id.post_url).setVisibility(View.VISIBLE);
        ((TextView) v.findViewById(R.id.tap_editable)).setText(R.string.tap_to_edit);
        ((TextView) v.findViewById(R.id.post_text)).setText(current.getPostText());

        instagramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (RealmController.getInstance().getSocialPost(current.getId()).isDispatched()) {
                    listener.showHasPosted();
                    changeColorDone(v.getContext(), instagramButton, instagramText, instagramImage);
                } else {
                    RealmController.getInstance().setSocialPostInstagramSelected(current.getId(), true);
                    listener.loadInstagramMedia(current, new Runnable() {
                        @Override
                        public void run() {
                            changeColorDone(v.getContext(), instagramButton, instagramText, instagramImage);
                        }
                    });
                }
            }
        });

        // refresh action view
        String markTweet = listener.getMarkInstagram(current.getId());
        if (markTweet != null) {
            String[] split = markTweet.split(",");
            for (String action : split) {
                if (Integer.valueOf(action) == com.trofiventures.systemsocial.common.Action.POST.getId())
                    changeColorDone(v.getContext(), instagramButton, instagramText, instagramImage);

            }
        }
    }
}
