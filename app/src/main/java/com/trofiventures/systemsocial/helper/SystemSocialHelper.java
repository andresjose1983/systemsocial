package com.trofiventures.systemsocial.helper;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.model.entities.post.Action;
import com.trofiventures.systemsocial.model.entities.post.SocialPost;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.model.network.responses.User;
import com.trofiventures.systemsocial.ui.posts.PostInteraction;

/**
 * Created by andre on 4/13/2017.
 */

public abstract class SystemSocialHelper {

    protected void validateMultiBrandAdvocate(SocialPost post, View view) {
        User user = User.getSharedUser();
        if (user != null && user.isMultibrandAdvocate()) {
            TextView textView = (TextView) view.findViewById(R.id.name_brands_text);
            textView.setVisibility(View.VISIBLE);
            int index = user.getBrands().indexOf(new Brand(post.getBrandId()));
            if (index >= 0) {
                Brand brand = user.getBrands().get(index);
                if (brand != null) {
                    String brandName;
                    brandName = view.getContext().getString(R.string.brand).concat("<br/>");
                    brandName = brandName + "<b>".concat(brand.getName()).concat("</b");
                    textView.setText(Html.fromHtml(brandName));

                    if ((post.getYoutubeId() == null || post.getYoutubeId().isEmpty()) &&
                            (post.getImgUrl() == null || post.getImgUrl().isEmpty())) {
                        TextView textView1 = (TextView) view.findViewById(R.id.name_brands_with_out_gallery);
                        textView1.setVisibility(View.VISIBLE);
                        textView1.setText(Html.fromHtml(brandName));
                        view.findViewById(R.id.view_separator).setVisibility(View.VISIBLE);
                    } else {
                        view.findViewById(R.id.name_brands_with_out_gallery).setVisibility(View.GONE);
                        view.findViewById(R.id.view_separator).setVisibility(View.GONE);
                    }

                }
            }
        }
    }

    protected void changeColorDone(final Context context, final View view, final TextView text,
                                   final ImageView imageView) {
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        text.setTextColor(ContextCompat.getColor(context, R.color.tw__composer_white));
        imageView.setColorFilter(ContextCompat.getColor(context, R.color.tw__composer_white));
        view.setClickable(false);
        text.setClickable(false);
        imageView.setClickable(false);
    }

    public abstract void onAction(final SocialPost current, final PostInteraction listener,
                                  final View v, final View twitterActionButtons);

    public abstract void onPost(final View v, final SocialPost current, final PostInteraction listener);

    public void showInfo(TextView textview, String text, long points) {
        if (points > 0)
            textview.setText(/*text + "\n" +*/ String.format(textview.getContext()
                    .getString(R.string.points_win), points));
        else
            textview.setText("");
    }

    protected void verifyAction(Action action, TextView label1, TextView label2, boolean isTwitter) {
        if (action.getAction() == com.trofiventures.systemsocial.common.Action.LIKE.getId()) {
            showInfo(label1, label1.getContext().getString(R.string.like), action.getPoints());
        } else if (action.getAction() == com.trofiventures.systemsocial.common.Action.COMMENT.getId()) {
            showInfo(label2, label1.getContext().getString(R.string.comment), action.getPoints());
        } else if (action.getAction() == com.trofiventures.systemsocial.common.Action.SHARE.getId()) {
            if (isTwitter)
                showInfo(label2, label1.getContext().getString(R.string.retweet), action.getPoints());
            else
                showInfo(label2, label1.getContext().getString(R.string.share), action.getPoints());
        }
    }
}
