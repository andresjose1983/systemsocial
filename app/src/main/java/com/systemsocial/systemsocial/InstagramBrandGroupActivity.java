package com.systemsocial.systemsocial;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.trofiventures.systemsocial.R;
import com.trofiventures.systemsocial.adapter.InstagramBrandGroupAdapter;
import com.trofiventures.systemsocial.common.BaseActivity;
import com.trofiventures.systemsocial.common.UIUtils;
import com.trofiventures.systemsocial.databinding.ActivityInstagramBrandGroupBinding;
import com.trofiventures.systemsocial.model.network.responses.Brand;
import com.trofiventures.systemsocial.presenter.InstagramBrandGroupPresenter;
import com.trofiventures.systemsocial.ui.accounts.InstagramBrandGroupView;

import java.util.List;

public class InstagramBrandGroupActivity extends BaseActivity implements InstagramBrandGroupView {

    private InstagramBrandGroupPresenter presenter;
    private ActivityInstagramBrandGroupBinding viewDataBinding;
    private RecyclerView brandsRecycler;
    private InstagramBrandGroupAdapter brandsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_instagram_brand_group);
        setCustomToolbar(null, false);

        presenter = new InstagramBrandGroupPresenter();
        viewDataBinding.setPresenter(presenter);

        presenter.attachView(this);
        presenter.onCreate();

        brandsRecycler = (RecyclerView) findViewById(R.id.instagrams_recycler);
        brandsAdapter = new InstagramBrandGroupAdapter();
        brandsAdapter.setAction(new IIBrandAction() {
            @Override
            public void onClick(Brand brand) {
                presenter.login(InstagramBrandGroupActivity.this, brand);
            }
        });
        brandsRecycler.setHasFixedSize(true);
        brandsRecycler.setAdapter(brandsAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        brandsRecycler.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(brandsRecycler.getContext(),
                linearLayoutManager.getOrientation());
        brandsRecycler.addItemDecoration(dividerItemDecoration);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void brandGroups(List<Brand> instagramBrandGroups) {
        brandsAdapter.setBrands(instagramBrandGroups);
        brandsAdapter.notifyDataSetChanged();
    }

    @Override
    public void resetInstagramAccount(final Brand brand) {
        UIUtils.showAlertView(this, getString(R.string.reset_account),
                String.format(getString(R.string.reset_instagram_account), brand.getName()),
                getString(R.string.reset_account),
                getString(R.string.cancel),
                new Runnable() {
                    @Override
                    public void run() {
                        presenter.logOutByBrand(InstagramBrandGroupActivity.this, brand);
                    }
                }, new Runnable() {
                    @Override
                    public void run() {
                    }
                });
    }

    public interface IIBrandAction {

        void onClick(Brand brand);

    }
}
